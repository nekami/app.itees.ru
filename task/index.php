<!DOCTYPE html>
<html>
	<head>
		<title>Приоритет задач</title>		
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<link href="css/style.css?<?=rand();?>" rel="stylesheet" />	
		<script src="js/jquery-3.1.0.min.js?<?=rand();?>"></script>	
	</head>
	<body id="app">	
	
		<div class="container-fluid">				
		
			<div class="row">
				<div class="col-md-12">				
					<div class="col-md-12">
						<div onClick="return Task.OpenModal();" class="settings_clear settings_go">Настройки</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div id="task"></div>					
				</div>
			</div>
		</div>
		
		<div id="modal_form" class="ModalForm">
			<form>
					<div class="col-md-12 DivParams"></div>
					
					<div class="row">
						<div class="col-md-12">					
							<div class="AddParam">
								<span class="AddParam__Btn">Добавить параметр</span>								
								
								<div class="PopupMenu PopupMenu_Paddings">
									<div class="PopupMenu__Content">
										<div class="PopupMenu__Body">
																				
											<span class="PopupMenu__Item">										
												<span class="PopupMenu__Text" onClick="return Task.AddSimpleParam('DEADLINE');">Дедлайн</span>
											</span>									
													
											<span class="PopupMenu__Delimiter"></span>									
													
											<span class="PopupMenu__Item">										
												<span class="PopupMenu__Text" onClick="return Task.AddSimpleParam('PRIORITY');">Важные задачи</span>
											</span>										
													
											<span class="PopupMenu__Delimiter"></span>									
													
											<span class="PopupMenu__Item">										
												<span class="PopupMenu__Text" onClick="return Task.AddSimpleParam('GROUP_ID');">Группа</span>
											</span>	
																					
											<span class="PopupMenu__Delimiter"></span>									
													
											<span class="PopupMenu__Item">										
												<span class="PopupMenu__Text" onClick="return Task.AddSimpleParam('DATE_START');">Начатые задачи</span>
											</span>	
													
										</div>
									</div>
								</div>								
							</div>								
						</div>
					</div>
							
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<input type="submit" onClick="return Task.Sorting();" value="Сортировать" id="sort" class="settings_submit" />	
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<div onClick="return Task.CloseModal();" class="settings_clear">Отмена</div>			
						</div>
					</div>
			</form>   
		</div>
		<div id="overlay" class="ModalFormOverlay"></div>
					
		<script src="js/underscore-min.js?<?=rand();?>"></script>	
		<script src="js/b24_rest_api.js?<?=rand();?>"></script>		
		<script src="js/drag-arrange.min.js?<?=rand();?>"></script>	
		<script src="js/script.js?<?=rand();?>"></script>		
	</body>	
</html>