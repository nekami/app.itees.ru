$(function(){
	
//инициализация
	// 1 - id div для задач // 2 - id select для групп // 3 - id кнопки "сортировать" // 4 - класс div настроек	
		
	Task = new Tabletask("task", "GROUP_ID", "sort", "DivParams");	

// вывод задач при загрузке страницы
	BX24.init(function(){			
		Task.getTask(); // вывод списка задач	
	});	
		
//конструктор		
	function Tabletask(id_tbody, id_select, id_submit, id_settings)
	{			
		this.id_tbody = id_tbody; // id div для задач	
		this.id_select = id_select; // id селекта с группами в модальном окне
		this.id_submit = id_submit; // id кнопки "Сортировать"	
		this.id_settings = id_settings; // id окна насроек для вывода опций
		this.task = new Object; // задачи
		this.group = new Object; // группы
	};
		
//список задач

	Tabletask.prototype.getTask = function(filter = false)
	{
		let me = this;

// промис текущего пользователя		
return new Promise(function(resolve, reject) 
{
	Promise.all([]).then(function(){
		return new Promise(function(resolve, reject) 
		{
			let IdOnlineUser;
			
			BX24.callMethod('user.current', {}, function(res){
				IdOnlineUser = res.data().ID;
				resolve(IdOnlineUser);
			});			

		});
	}).then(function(IdOnlineUser)
	{	
		// промис задач
			return new Promise(function(resolve, reject) 
			{
				Promise.all([]).then(function(){
					return new Promise(function(resolve, reject) 
					{						
						params = {
							'order': {'ID': 'DESC'},
							'filter': {'RESPONSIBLE_ID': IdOnlineUser, 'CLOSED_DATE': false},
							'params': {},
							'select': {/*'ID', 'TITLE', 'RESPONSIBLE_ID', 'CREATED_BY', 'CREATED_BY_LAST_NAME', 'CREATED_BY_NAME', 'DURATION_FACT', 'DEADLINE', 'PRIORITY', 'DATE_START', 'GROUP_ID', 'TIME_ESTIMATE', 'RESPONSIBLE_NAME', 'RESPONSIBLE_LAST_NAME'*/}
						};									
								
						BX24.callMethod(
							'task.item.list',
							params,
							function(result)
							{	
								resolve(result.total());
							}
						);
					});
				}).then(function(total)
				{	
					let count = 0, b_count = 0, limit = 40, items_per_page = 50, batchs = {};
					let pages = Math.ceil(total/items_per_page);
								
					for(let index = 0; index < pages; index ++)
					{
						if (count >= limit) {
							count = 0;
							b_count++;
						}

						if(!batchs[b_count]) {
							batchs[b_count] = {};
						}
									
						batchs[b_count][index] = {
						'method': 'task.item.list', 
							'params': Object.assign({'start': items_per_page * index}, params)
						};

						count++;
					}								

					if(Object.keys(batchs).length){
						let el_progress = $('#get_companies_progress').find('.progress-bar');
						let batchsCount = Object.keys(batchs).length;
						let progressStep = 100;
									
						if(batchsCount) {
							progressStep = 100/batchsCount;
							progressStep = +progressStep.toFixed(2);
						}
																	
						me.bacthHandler(
							batchs,
							function(id, error) {
								console.error("ERROR: " + error);
							},
								function(id, data) {},										
								el_progress,
								progressStep
						);
					}		
				});
			});	

	});
});	
			
		}
		
//
		
	Tabletask.prototype.bacthHandler = function(batchs, funcError, funcSuccess, progress_bar = false, progress_step = false) 
	{
		let me = this;
		let ob, promises;
		ob = this; promises = [];
		
		promises = me.getBatchPromises(promises, batchs, progress_bar, progress_step);
		
		Promise.all(promises).then(result => {
			let resBatch, data;
			resBatch = {}; data = {}; 
			result.forEach(function(value) {
				$.extend(resBatch, value);
			});

			for(let id in resBatch) {
				let res;
				res = resBatch[id];
				
				if(res.error()) {
					funcError(id, res.error(), res);
				}
				else {
					data[id] = res.data();
					funcSuccess(id, data[id], res);
				}
			}
			
			// преобразование объекта данных	
			
			let newData = new Object;

			$.each(data, function(id, mass) {
			$.each(mass, function(code, elem) 
			{					
				elem.order = 0; 
				
				newData[elem.ID] = elem;
				
			});
			});
			
			me.task = newData;
			
			let Configuration = BX24.userOption.get("PARAMS");	

			//console.log(Configuration);
			
			if (Configuration != undefined)
			{	
				Configuration = JSON.parse(Configuration);
		
				// текущая дата для подсчета деделайна
				let DateOnline = new Date();			
				DateOnline = DateOnline.getUnixTimestamp(); 			
				
				for(let ProperttyCode in Configuration) 
				{
					// сортировка задач
					me.SetOrder(DateOnline, Configuration[ProperttyCode].name, Configuration[ProperttyCode].value, Configuration[ProperttyCode].order);
				}
				
				me.SetOrdersNumberTasks(); // порядковый номер элемента в завимости от поля order
			}
			
			me.printTask();
		});
	}
	
//
	Tabletask.prototype.getBatchPromises = function(promises, batchs, progress_bar, progress_step)
	{
		let ob;
		ob = this;

		function delayExec(num, resolve) {
			setTimeout(
				function() {
					BX24.callBatch(batchs[num], function (result) {
						if(progress_bar && progress_step) {
							let prev_progress = parseFloat(progress_bar.attr('aria-valuenow'));
							let cur_progress = prev_progress + parseFloat(progress_step);
							cur_progress = +cur_progress.toFixed(2);
							progress_bar.css('width', cur_progress + '%').attr('aria-valuenow', cur_progress).find(".js-pers_val").html(cur_progress + "%");
						}						
						BX24.refreshAuth();
						resolve(result);
					});
				},
				(Math.floor(parseInt(num) / 1)) * 7500
			);
		}
		
		if(Object.keys(batchs).length) {
			for (let n in batchs) {
				let pr = new Promise((resolve, reject) => {
					delayExec(n, resolve);
				});
				promises.push(pr);
			}
		} else {
			if(progress_bar && progress_step) {
				let prev_progress = parseFloat(progress_bar.attr('aria-valuenow'));
				let cur_progress = prev_progress + parseFloat(progress_step);
				cur_progress = +cur_progress.toFixed(2);
				progress_bar.css('width', cur_progress + '%').attr('aria-valuenow', cur_progress).html(cur_progress + "%");
			}
		}
		
		return promises;
	}		



	// печать массива
		Tabletask.prototype.printTask = function()
		{
			let me = this;
			
			$('#'+me.id_tbody).empty();			
			
			return new Promise(function(resolve, reject) 
			{
				Promise.all([]).then(function(){
					return new Promise(function(resolve, reject) 
					{						
						me.getGroupsTask(resolve);					
					});
				}).then(function(gpoups){

					let str = '';			
				
					$.each(me.task, function(code, elem) {
						
						//console.log(elem.ID);
						
						if ((elem.PRIORITY != 0) && (elem.PRIORITY == 2)) class_priority = "TaskBlock_priority";
						else class_priority = "";
						
						detail_link = 'https://itees.bitrix24.ru/company/personal/user/'+elem.RESPONSIBLE_ID+'/tasks/task/view/'+elem.ID+'/';					
						created_by_link = 'https://itees.bitrix24.ru/company/personal/user/'+elem.CREATED_BY+'/';
						LenghtTime = Math.floor(elem.DURATION_FACT/60);													
						if (elem.DATE_START != "") StartTime = me.FormatDate(elem.DATE_START); else StartTime = "Не начата";													
						if (elem.DEADLINE != "") { Deadline = me.FormatDate(elem.DEADLINE); } else { Deadline = "Не установлен" }													
						if (elem.GROUP_ID != 0) { group = gpoups[elem.GROUP_ID]; } else { group = 'Без группы'; }
										
						str += '<div class="col-md-12"> <div class="TaskBlock TaskBlock_size"> <div class="TaskBlock__content TaskBlock__body_skin"> <div class="TaskBlock__head"><span class="'+class_priority+'"></span><span class="TaskBlock__TitleContainer"><div class="block__title__open block__title__open_close"></div><span class="TaskBlock__TitleContainer__Inner"><a href="'+detail_link+'" target="_blank"><span class="TaskBlock__TitleContainer__Title">'+elem.ID+' - '+elem.TITLE+'</span></a> </span> </span> </div> <div class="TaskBlock__body"> <div class="TaskBlock__body__style"> <div class="row"> <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><span class="TaskBlock__body__img"></span></div> <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <div class="TaskBlock__body__ItemBlock"> <div class="TaskBlock__body__item"> <div class="TaskBlock__body__item__name">КРАЙНИЙ СРОК</div> <div class="TaskBlock__body__item__value">'+Deadline+'</div> </div> </div> <div class="row TaskBlock__body__ItemSubBlock" style="top: 6px; position: relative;"><span class="col-lg-6 col-md-6 col-sm-12 col-xs-12 TaskBlock__body__LeftSubItem"><span class="TaskBlock__body__TitleSubItem">ДАТА НАЧАЛА ЗАДАЧИ</span><div class="TaskBlock__body__ValueSubItem">'+StartTime+'</div></span><span class="col-lg-6 col-md-6 col-sm-12 col-xs-12 TaskBlock__body__LeftSubItem"><span class="TaskBlock__body__TitleSubItem">ЗАТРАЧЕННОЕ ВРЕМЯ</span><div class="TaskBlock__body__ValueSubItem">'+LenghtTime+'</div></span> </div> </div> <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <div class="TaskBlock__body__ItemBlock"> <div class="TaskBlock__body__item"> <div class="TaskBlock__body__item__name">ПОСТАНОВЩИК</div> <div class="TaskBlock__body__item__value">'+elem.CREATED_BY_LAST_NAME+' '+elem.CREATED_BY_NAME+'</div> </div> </div> <div class="row TaskBlock__body__ItemSubBlock" style="top: 6px; position: relative;"><span class="col-lg-6 col-md-6 col-sm-12 col-xs-12 TaskBlock__body__LeftSubItem"><span class="TaskBlock__body__TitleSubItem">РАБОЧАЯ ГРУППА</span><div class="TaskBlock__body__ValueSubItem">'+group+'</div></span><span class="col-lg-6 col-md-6 col-sm-12 col-xs-12 TaskBlock__body__LeftSubItem"><span class="TaskBlock__body__TitleSubItem">ОТВЕТСТВЕННЫЙ</span><div class="TaskBlock__body__ValueSubItem">'+elem.RESPONSIBLE_LAST_NAME+' '+elem.RESPONSIBLE_NAME+'</div> </span> </div> </div> </div> </div> </div> </div> </div></div>';
					
					});					
					
					$('#'+me.id_tbody).append(str);			
					
					// детальный просмотр карточки задачи
					$(".block__title__open").click(function() 
					{	
						if ($(this).hasClass("block__title__open_close")) 
						{
							$(this).parent().parent().parent().find(".TaskBlock__body").css({'display' : 'block'});				
							$(this).removeClass("block__title__open_close");
						}
						else 
						{
							$(this).parent().parent().parent().find(".TaskBlock__body").css({'display' : 'none'});								
							$(this).addClass("block__title__open_close");	
						}
					});	
				});
			});	
		}
		
		
//добавление свойства order к объекту задач
		
			Tabletask.prototype.SetOrder = function(DateOnline, PropertyCode, PropertyValue, Mark)
			{
				me = this;			
				
				$.each(me.task, function(id, elem) 
				{	
					switch (PropertyCode) 
					{
						case 'DEADLINE':	
							if ((PropertyValue != 0) && (elem.DEADLINE != ""))
							{							
								let DedlineDate = new Date(elem.DEADLINE);
								DedlineDate = DedlineDate.getUnixTimestamp();
								let CountHours = DedlineDate - DateOnline;
								
								Hour = me.TimeConverter(CountHours);								
								if ((CountHours < 0) && (Hour != 0)) Hour = -Hour;								
								
								if (Number(Hour) <= Number(PropertyValue))
								{
									if ((elem.order == "") || (elem.order == undefined)) 
									{ 
										elem.order = Mark; 
									}
									else
									{
										if (Mark > elem.order) elem.order = Mark;										
									}
								}
								
								//console.log(['DEADLINE', Mark, elem.ID, elem.DEADLINE]);
							}
						break;
						
						case 'PRIORITY':							
							if ((PropertyValue != 0) && (elem.PRIORITY === PropertyValue))
							{
								if ((elem.order == "") || (elem.order == undefined)) 
								{ 
									elem.order = Mark; 
								}
								else { if (Mark > elem.order) elem.order = Mark; }
								
								//console.log(['PRIORITY', elem.ID, elem.PRIORITY]);
							}
						break;
						
						case 'GROUP_ID':
							if ((PropertyValue != 0) && (elem.GROUP_ID === PropertyValue))
							{
								if ((elem.order == "") || (elem.order == undefined)) 
								{ 
									elem.order = Mark; 
								}
								else { if (Mark > elem.order) elem.order = Mark; }
								
								//console.log(['GROUP_ID', elem.ID, elem.GROUP_ID]);
							}
						break;
						
						case 'DATE_START':							
							if ((PropertyValue != 0) && (elem.DATE_START != ""))
							{
								if ((elem.order == "") || (elem.order == undefined)) 
								{ 
									elem.order = Mark; 
								}
								else { if (Mark > elem.order) elem.order = Mark; }
								
								//console.log(['DATE_START', elem.ID, elem.DATE_START]);
							}
						break;
						default:
							elem.order = 0; 
						break;
					}			
					
				});
				
				/*$.each(me.task, function(id, elem) 
				{			
					console.log(elem.ID, elem.order);
				});	
				
				console.log("конец");*/
				
			}
			
//назначение порядка вывода элементам объекта в зависимости от значения поля order
			
		Tabletask.prototype.SetOrdersNumberTasks = function()
		{				
			me = this;
			
			let NewSortTask = new Object;
			
			let i = 0;
			
			$.each(me.task, function(id, elem) 
			{
				if (elem.order === 40) 
				{	
					i++;
					NewSortTask[i] = elem;
				}
			});
			
			$.each(me.task, function(id, elem) 
			{		
				if (elem.order === 30) 
				{
					i++;
					NewSortTask[i] = elem;
				}					
			});	
			
			$.each(me.task, function(id, elem) 
			{			
				if (elem.order === 20) 
				{							
					i++;
					NewSortTask[i] = elem;
				}					
			});
			
			$.each(me.task, function(id, elem) 
			{			
				if (elem.order === 10) 
				{	
					i++;
					NewSortTask[i] = elem;
				}	
			});	
			
			$.each(me.task, function(id, elem) 
			{
				if (elem.order === 0) 
				{	
					i++;
					NewSortTask[i] = elem;
				}	
			});
			
			
			
			
			/*$.each(NewSortTask, function(id, elem) 
			{			
				console.log(elem.ID, elem.order);
			});	
			
			console.log("конец");*/
			
			me.task = NewSortTask;
		};		
		
//нажатие на кнопку "Сортировать"
		
		Tabletask.prototype.FilterTask = function()
		{
			me = this;		
			
			let object_form = $('#'+me.id_submit).parent().parent().parent();
			
			// объект со всеми элементами с заданным параметром (поля с результатами)
			let obValue = object_form.find('[data-filter="filter"]');	
			
			//console.log(obValue);
			
			let selector = 0; // селектор для подсчета количества оборотов цикла
		
		// формирование и сохраненние конфигурации
			let Configuration = new Object;
			
			$.each(obValue, function(code, value) 
			{
				// текущая конфигурация				
				let template_settings = new Object;				
				template_settings.name = $(value).attr('id');				
				template_settings.value = $(value).val();							
				
				switch (selector) // назначение очков в зависимости от количества оборотов цикла
				{
					case 0: mark = 40; break;
					case 1: mark = 30; break;
					case 2: mark = 20; break;
					case 3: mark = 10; break;
				}

				template_settings.order = mark;					
				
				// сохранение значений конфигурации
				Configuration[selector] = template_settings;
				
				selector++;						
			});
			
			Configuration = JSON.stringify(Configuration);  // преобразорвание значений настроек в строку
				
			BX24.userOption.set("PARAMS", Configuration);   // сохранение настроек
			
			// перевывод задач
			
			let Config = BX24.userOption.get("PARAMS");
			
			// перед новой сортировкой удаляем старую, чтоб не мешалась в условиях
			$.each(me.task, function(id, elem) 
			{
				if (elem.order != undefined) delete me.task[id].order;
			});
			
			if (Config != undefined)
			{	
				Config = JSON.parse(Config);
		
				// текущая дата для подсчета деделайна
				let DateOnline = new Date();			
				DateOnline = DateOnline.getUnixTimestamp(); 			
				
				for(let ProperttyCode in Config) 
				{
					// сортировка задач
					me.SetOrder(DateOnline, Config[ProperttyCode].name, Config[ProperttyCode].value, Config[ProperttyCode].order);
				}
				
				me.SetOrdersNumberTasks(); // порядковый номер элемента в завимости от поля order
			}
			
			me.printTask();		
		}
		
//////////////// Группы/////////////////////////////////////////////////////
					 
		// список все групп
		Tabletask.prototype.getGroupsTask = function(resolve)
		{									
			let gpoups = new Object();		
					
			BX24.callMethod('sonet_group.get', {
				'ORDER': 
				{
					'ID': 'ASC'
				},
				'FILTER': {},
				'PARAMS': {},
				'SELECT': {},
				},
				function(result_gpoups)
				{					
					let gpoups_res = result_gpoups.data();
								
					for(let id in gpoups_res) 
					{
						gpoups[gpoups_res[id].ID] = gpoups_res[id].NAME;	
					}

					resolve(gpoups);
				}		
			);					
		};
		
		
		
// заполнение селектового поля	
	Tabletask.prototype.GetSelectGroup = function(value = false)
	{
		let me = this;
		
		return new Promise(function(resolve, reject) 
		{
			Promise.all([]).then(function(){
				return new Promise(function(resolve, reject) 
				{						
					me.getGroupsTask(resolve);					
				});
			}).then(function(gpoups){
				
				let data = "<option>--Не выбрано--</option>";
				
				for(let id in gpoups) 
				{	
					if (value == id) selected = "selected"; else selected = "";
			
					data += "<option "+selected+" value="+id+" id ='"+id+"'>"+gpoups[id]+"</option>"
				}
				
				me.group = gpoups;
				
				$('#'+me.id_select).append(data);
			});
		});
	}

// вывод div окна настроек

	Tabletask.prototype.PrintFilter = function()
	{
		let me = this;		
		let StringSettings = '';
		
		let Configuration = BX24.userOption.get("PARAMS");

		if (Configuration != undefined)	
		{
			Configuration = JSON.parse(Configuration);			
			
			for(let ProperttyCode in Configuration) 
			{				
				if (Configuration[ProperttyCode].name == "DEADLINE") 
				{
					StringSettings += '<div class="row DivParams__Item"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><span class="DivParams__Hamburger_Light DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title_Dark DivParams__Title"><span class="DivParams__Title__Inner"><span class="DivParams__Title__Name">Дедлайн</span></span></span></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><input class="settings_input" placeholder="Количество часов" type="text" name="count_days" value="'+Configuration[ProperttyCode].value+'" id="DEADLINE" data-filter="filter"/></div></div>';
				}
				if (Configuration[ProperttyCode].name == "PRIORITY") 
				{
					StringSettings += '<div class="row DivParams__Item"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner"><span class="DivParams__Title__Name">Важность</span></span></span></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><div class="inputGroup"><input id="PRIORITY" name="PRIORITY" type="checkbox" data-filter="filter" value="'+Configuration[ProperttyCode].value+'"/><label for="PRIORITY"></label></div></div></div>';
				}
				if (Configuration[ProperttyCode].name == "GROUP_ID") 
				{
					StringSettings += '<div class="row DivParams__Item"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner"><span class="DivParams__Title__Name">Группа</span></span></span></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> <select id="GROUP_ID" class="settings_select" data-filter="filter"></select></div></div>';
					
					BX24.init(function(){
						Task.GetSelectGroup(Configuration[ProperttyCode].value); // вывод групп в селект модального окна			
					});	
				}
				if (Configuration[ProperttyCode].name == "DATE_START") 
				{
					StringSettings += '<div class="row DivParams__Item"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner"><span class="DivParams__Title__Name">Дата начала</span></span></span></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><div class="inputGroup"><input id="DATE_START" name="DATE_START" type="checkbox" data-filter="filter" value="'+Configuration[ProperttyCode].value+'"/><label for="DATE_START"></label></div></div></div>';
				}
			}
		
		} 
		else 
		{		
			StringSettings += '<div class="row DivParams__Item"><div class="col-md-6"><span class="DivParams__Hamburger_Light DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title_Dark DivParams__Title"><span class="DivParams__Title__Inner"><span class="DivParams__Title__Name">Дедлайн</span></span></span></div><div class="col-md-6"><input class="settings_input" placeholder="Количество часов" type="text" name="count_days" value="" id="DEADLINE" data-filter="filter"/></div></div><div class="row DivParams__Item"><div class="col-md-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner"><span class="DivParams__Title__Name">Важность</span></span></span></div><div class="col-md-6"><div class="inputGroup"><input id="PRIORITY" name="PRIORITY" type="checkbox" data-filter="filter" value="0"/><label for="PRIORITY"></label></div></div></div><div class="row DivParams__Item"><div class="col-md-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner"><span class="DivParams__Title__Name">Группа</span></span></span></div><div class="col-md-6"> <select id="GROUP_ID" class="settings_select" data-filter="filter"></select></div></div><div class="row DivParams__Item"><div class="col-md-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner"><span class="DivParams__Title__Name">Дата начала</span></span></span></div><div class="col-md-6"><div class="inputGroup"><input id="DATE_START" name="DATE_START" type="checkbox" data-filter="filter" value="0"/><label for="DATE_START"></label></div></div></div>';
		
			BX24.init(function(){
				Task.GetSelectGroup(); // вывод групп в селект модального окна			
			});	
		}
		
		$('.'+me.id_settings).append(StringSettings);
		
		// заменители онкликов

		$("#PRIORITY").click(function() 
		{	
			if ($(this).prop('checked') == true) $(this).val("2");
			else $(this).val("0");
		});
		if ($("#PRIORITY").val() == 2) $('#PRIORITY').prop('checked',true);
		
		$("#DATE_START").click(function() 
		{	
			if ($(this).prop('checked') == true) $(this).val("1");
			else $(this).val("0");
		});
		if ($("#DATE_START").val() == 1) $('#DATE_START').prop('checked',true);
		
		// перетаскивание				
        $('.DivParams__Item').arrangeable({dragSelector: '.DivParams__Hamburger'});
	}
		
/////////////////////////////вспомогательные функции///////////////////////////		
		
		
// получение часов из даты в формате timestamp
	Tabletask.prototype.TimeConverter = function(UNIX_timestamp) {
	  let a = new Date(UNIX_timestamp * 1000);  
	  let hour = a.getHours();  
	  return hour;
	}	

//	формат даты для ее вывода в карточку задачи	
	Tabletask.prototype.FormatDate = function(ObjectDate)
	{	
		let Time = new Date(ObjectDate);
		Time = Time.toLocaleString();
		return Time;
	}

// дополнительный метод для объекта Date (перевод даты в формат timestamp)
	Date.prototype.getUnixTimestamp = function() {

		return Math.round(this.getTime() / 1000);

	}

// Закрытие модального окна	
	Tabletask.prototype.CloseModal = function() 
	{
		$('#modal_form').animate({opacity: 0, top: '20%'}, 200,
			function(){
				$(this).css('display', 'none');				
			}
		);
		return false;
	}	

// Открытие модального окна
	Tabletask.prototype.OpenModal = function()
	{
		$('#modal_form').css('display', 'block').animate({opacity: 1, top: '28%'}, 200);		
		
		$('.'+this.id_settings).empty(); // очистка окна настроек
		
		this.PrintFilter();	// вывод настроек	
		return false;
	};		


// Сортировка
	Tabletask.prototype.Sorting = function()
	{				
		this.CloseModal();
		this.FilterTask(); 
		return false;
	}				
			
});