$(function(){

//<------------------- Вынести обработчики DOM-событий наружу

	$( '#app' ).on( 'click', function( event ) {
				
		let online = event.target;
				
		// открытие/закрытие детальной карточки задачи
		if ($(online).attr("class") == "block__title__open") 
		{					
			$(online).parent().find(".TaskBlock__body").css({'display' : 'block'});	
			$(online).addClass("block__title__open_close");	
		}
		else if ($(online).attr("class") == "block__title__open block__title__open_close")
		{
			$(online).parent().find(".TaskBlock__body").css({'display' : 'none'});				
			$(online).removeClass("block__title__open_close");
		}
				
	});

//инициализация
	// 1 - id div для задач // 2 - id select для групп // 3 - id кнопки "сортировать" // 4 - класс div настроек	
		
	Task = new Tabletask("task", "sort", "DivParams");	

// вывод задач при загрузке страницы
	BX24.init(function()
	{		
		Task.getGroupsTask(); // получение и вывод групп
		Task.getTask(); // вывод списка задач		
	});	
		
//конструктор		
	function Tabletask(id_tbody, id_submit, id_settings)
	{			
		this.id_tbody = id_tbody; // id div для задач
		this.id_submit = id_submit; // id кнопки "Сортировать"	
		this.id_settings = id_settings; // id окна насроек для вывода опций
		this.task = {}; // задачи
		this.group = {}; // группы		
		this.IdFirstGroup = 1; // индекс первой группы (для стартовых настроек)
	};	
		
//список задач
	Tabletask.prototype.getTask = function(filter = false)
	{
		let me = this;

		// промис текущего пользователя	
		return new Promise(function(resolve, reject) 
		{
			let IdOnlineUser;
				
			BX24.callMethod('user.current', {}, function(res){
				IdOnlineUser = res.data().ID;
				resolve(IdOnlineUser);
			});
			
		}).then(function(IdOnlineUser)
		{	
			// промис задач
			return new Promise(function(resolve, reject) 
			{						
				params = {
					'order': {'ID': 'DESC'},
					'filter': {'RESPONSIBLE_ID': IdOnlineUser, 'CLOSED_DATE': false},
					'params': {},
					'select': {}
				};									
								
				BX24.callMethod(
					'task.item.list',
					params,
					function(result)
					{	
						resolve(result.total());
					}
				);
					
			}).then(function(total)
			{
				me.getAllPage(params, total, "task", "task.item.list");
			});
		});			
	}


// получение объектов со всех страниц сразу
	Tabletask.prototype.getAllPage = function(params, total, place, method)
	{
		let me = this;
		
		let count = 0, b_count = 0, limit = 40, items_per_page = 50, batchs = {};
		let pages = Math.ceil(total/items_per_page);
								
		for(let index = 0; index < pages; index ++)
		{			
			if (count >= limit) {
				count = 0;
				b_count++;
			}

			if(!batchs[b_count]) {
				batchs[b_count] = {};
			}
									
			batchs[b_count][index] = {
				'method': method, 
				'params': Object.assign({'start': items_per_page * index}, params)
			};

			count++;
		}	

		if(Object.keys(batchs).length) 
		{														
			me.bacthHandler(
				batchs,
				function(id, error) {
					console.error("ERROR: " + error);
				},
				function(id, data) {},		
				place
			);
		}
	}	
		
	Tabletask.prototype.bacthHandler = function(batchs, funcError, funcSuccess, place) 
	{
		let me = this;
		let ob, promises;
		ob = this; promises = [];
		
		promises = me.getBatchPromises(promises, batchs);
		
		Promise.all(promises).then(result => {
			let resBatch, data;
			resBatch = {}; data = {}; 
			result.forEach(function(value) {
				$.extend(resBatch, value);
			});
			
			for(var id in resBatch) {
				var res;
				res = resBatch[id];
				
				if(res.error()) {
					funcError(id, res.error(), res);
				}
				else {
					data[id] = res.data();
					funcSuccess(id, data[id], res);
				}
			}
					
			let newData = {};

				$.each(data, function(id, mass) {
				$.each(mass, function(code, elem) 
				{				
					newData[elem.ID] = elem;
				});
			});					
			
			if (place == "task")
			{						
				me.task = newData;				
				me.ConfigurationTask();	
			}			
			else if (place == "group") 
			{
				me.IdFirstGroup = Object.keys(newData)[0];				
				me.group = newData;					
				me.GetSelectGroup();
			}
			
		});
	}

//
	Tabletask.prototype.getBatchPromises = function(promises, batchs)
	{
		let ob;
		ob = this;

		function delayExec(num, resolve) {
			setTimeout(
				function() {
					BX24.callBatch(batchs[num], function (result) {						
						BX24.refreshAuth();
						resolve(result);
					});
				},
				(Math.floor(parseInt(num) / 1)) * 7500
			);
		}
		
		if(Object.keys(batchs).length) {
			for (let n in batchs) {
				let pr = new Promise((resolve, reject) => {
					delayExec(n, resolve);
				});
				promises.push(pr);
			}
		} 
		
		return promises;
	}		



	// печать массива
		Tabletask.prototype.printTask = function()
		{
						   // получение домена	
			let me = this, auth = BX24.getAuth(), domain = auth["domain"], str = '';			
			
			$('#'+me.id_tbody).empty();	
				
			$.each(me.task, function(code, elem) {
						
				if ((elem.PRIORITY != 0) && (elem.PRIORITY == 2)) class_priority = "TaskBlock_priority";
				else class_priority = "";
						
				detail_link = 'https://'+domain+'/company/personal/user/'+elem.RESPONSIBLE_ID+'/tasks/task/view/'+elem.ID+'/';					
				created_by_link = 'https://'+domain+'/company/personal/user/'+elem.CREATED_BY+'/';
				LenghtTime = Math.floor(elem.DURATION_FACT/60);	
				let AllTime = '';
				if (elem.TIME_ESTIMATE != "0") AllTime = '/'+Math.floor(elem.TIME_ESTIMATE/3600);				
				if (elem.DATE_START != "") StartTime = me.FormatDate(elem.DATE_START); else StartTime = "Не начата";													
				if (elem.DEADLINE != "") { Deadline = me.FormatDate(elem.DEADLINE); } else { Deadline = "Не установлен" }													
				
				if ((elem.GROUP_ID != 0) && (elem.GROUP_ID != undefined) && (me.group[elem.GROUP_ID] != undefined)) 
				{ 
					group = me.group[elem.GROUP_ID].NAME; 
				} 
				else { group = 'Без группы'; }
								
				str += '<div class="col-md-12">'
					+'<div class="TaskBlock TaskBlock_size">'
						+'<div class="block__title__open"></div>'
						+'<div class="TaskBlock__content TaskBlock__body_skin">'
							+'<div class="TaskBlock__head">'
								+'<span class="'+class_priority+'"></span>'								
								+'<span class="TaskBlock__TitleContainer">'						
									+'<span class="TaskBlock__TitleContainer__Inner"><a href="'+detail_link+'" target="_blank"><span class="TaskBlock__TitleContainer__Title">'+elem.ID+' - '+elem.TITLE+'</span></a>'
									+'</span>'
								+'</span>'
							+'</div>'
							+'<div class="TaskBlock__body">'
								+'<div class="TaskBlock__body__style">'
									+'<div class="row">'
										+'<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><span class="TaskBlock__body__img"></span></div>'
										+'<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">'
											+'<div class="TaskBlock__body__ItemBlock">'
												+'<div class="TaskBlock__body__item">'
													+'<div class="TaskBlock__body__item__name">КРАЙНИЙ СРОК</div>'
													+'<div class="TaskBlock__body__item__value">'+Deadline+'</div>'
												+'</div>'
											+'</div>'											
											+'<div class="row TaskBlock__body__ItemSubBlock" style="top: 6px; position: relative;">'											
												+'<span class="col-lg-6 col-md-6 col-sm-12 col-xs-12 TaskBlock__body__LeftSubItem">'
													+'<span class="TaskBlock__body__TitleSubItem">ДАТА НАЧАЛА ЗАДАЧИ</span>'
													+'<div class="TaskBlock__body__ValueSubItem">'+StartTime+'</div>'
												+'</span>'											
												+'<span class="col-lg-6 col-md-6 col-sm-12 col-xs-12 TaskBlock__body__LeftSubItem">'
													+'<span class="TaskBlock__body__TitleSubItem">ЗАТРАЧЕННОЕ ВРЕМЯ</span>'
													+'<div class="TaskBlock__body__ValueSubItem">'+LenghtTime+AllTime+'</div>'
												+'</span>'
											+'</div>'
										+'</div>'
										+'<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">'
											+'<div class="TaskBlock__body__ItemBlock">'
												+'<div class="TaskBlock__body__item">'
													+'<div class="TaskBlock__body__item__name">ПОСТАНОВЩИК</div>'
													+'<div class="TaskBlock__body__item__value">'+elem.CREATED_BY_LAST_NAME+' '+elem.CREATED_BY_NAME+'</div>'
												+'</div>'
											+'</div>'										
											+'<div class="row TaskBlock__body__ItemSubBlock" style="top: 6px; position: relative;">'										
												+'<span class="col-lg-6 col-md-6 col-sm-12 col-xs-12 TaskBlock__body__LeftSubItem">'												
													+'<span class="TaskBlock__body__TitleSubItem">РАБОЧАЯ ГРУППА</span>'													
													+'<div class="TaskBlock__body__ValueSubItem">'+group+'</div>'
												+'</span>'												
												+'<span class="col-lg-6 col-md-6 col-sm-12 col-xs-12 TaskBlock__body__LeftSubItem">'
													+'<span class="TaskBlock__body__TitleSubItem">ОТВЕТСТВЕННЫЙ</span>'
													+'<div class="TaskBlock__body__ValueSubItem">'+elem.RESPONSIBLE_LAST_NAME+' '+elem.RESPONSIBLE_NAME+'</div>'
												+'</span>'
											+'</div>'									
										+'</div>'										
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
				+'</div>';					
			});				
					
			$('#'+me.id_tbody).append(str);	
			
			BX24.resizeWindow($(document).width(), ($(document).height()+(me.task.length*146)));
		}

// упорядочивание и вывод задач
		Tabletask.prototype.ConfigurationTask = function()
		{	
			let me = this;
			
			let Config = me.VerifyСonfiguration();
			
			Config = JSON.parse(Config);
		
			// текущая дата для подсчета деделайна
			let DateOnline = new Date();			
			DateOnline = DateOnline.getUnixTimestamp(); 			
			
			for (var value of Config) 
			{	
				// сортировка задач				
				me.SetOrder(DateOnline, value);
			}
				
			me.task = _.sortBy(me.task, function(object) 
			{
				return -object.order;			
			});				
						
			me.printTask();
			
		}
		


		
//добавление свойства order к объекту задач
		
			Tabletask.prototype.SetOrder = function(DateOnline, Config)
			{			
				let me = this;					
				
				if (Config != null)
				{
					$.each(me.task, function(id, elem) 
					{
						switch ( Config.name.replace(/[0-9]/g, '') ) 
						{						
							case 'DEADLINE':

								if ( Config.hide == 1 ) break; // деактивация настройки
							
								if ((Config.value != 0) && (elem.DEADLINE != "") && (elem.order != -1))
								{							
									let DedlineDate = new Date(elem.DEADLINE);
									DedlineDate = DedlineDate.getUnixTimestamp();
									let CountHours = DedlineDate - DateOnline;
									
									Hour = me.TimeConverter(CountHours);								
									if ((CountHours < 0) && (Hour != 0)) Hour = -Hour;								
									
									if (Number(Hour) <= Number(Config.value))
									{
										if (elem.order == undefined)
										{
											elem.order = Config.order;										
										}
										else 
										{
											elem.order += Config.order;										
										}
									}
								}
							break;
							
							case 'PRIORITY':
							
								if ( Config.hide == 1 ) break; // деактивация настройки
							
								if ((Config.value != 0) && (elem.PRIORITY === Config.value) && (elem.order != -1))
								{
									if (elem.order == undefined) 
									{
										elem.order = Config.order;									
									}
									else 
									{
										elem.order += Config.order;								
									}
								}
							break;
							
							case 'GROUP_ID':						
							
								if ( Config.hide == 1 ) break; // деактивация настройки
								
								if ((Config.value != 0) && (elem.GROUP_ID === Config.value) && (elem.order != -1))
								{
									if ( Config.notgroup != 1 )	
									{									
										if (elem.order == undefined) elem.order = Config.order;
										else elem.order += Config.order;
									} 
									else 
									{
										elem.order = -1;				
									}
								}
							break;
							
							case 'DATE_START':	

								if ( Config.hide == 1 ) break; // деактивация настройки
							
								if ((Config.value != 0) && (elem.DATE_START != "") && (elem.order != -1))
								{
									if (elem.order == undefined) 
									{
										elem.order = Config.order;									
									}
									else 
									{
										elem.order += Config.order;									
									}
								}
							break;
						}	

						if (elem.order == undefined) elem.order = 0;
						
					});
				}
			}
			
			
// проверка конфигурации	
		Tabletask.prototype.VerifyСonfiguration = function()
		{
			let me = this;
			
			let Configuration = BX24.userOption.get("PARAMS");			
			
			if ((Configuration == undefined) || (Configuration == "{}"))			
			{				
				Configuration = {"0":{"name":"DEADLINE","value":"1","order":100000, "hide": "0"},"1":{"name":"PRIORITY","value":"2","order":10000, "hide": "0"},"2":{"name":"GROUP_ID","value":me.IdFirstGroup,"order":1000, "notgroup":1, "hide": "0"},"3":{"name":"DATE_START","value":"1","order":10, "hide": "0"}};				
					
				Configuration = JSON.stringify(Configuration);
			}
			return Configuration;
		}
		
		
//////////////// Группы/////////////////////////////////////////////////////
					 
		// список все групп
		Tabletask.prototype.getGroupsTask = function()
		{				
			let me = this;
				
			return new Promise(function(resolve, reject) 
			{
				params = {
					'order': {'NAME': 'ASC'},
					'filter': {},
					'params': {},
					'select': {}
				};		

					
				BX24.callMethod('sonet_group.get', 
					params,
					function(group)
					{	
						resolve(group.total());
					}		
				);
					
			}).then(function(total)
			{	
				me.getAllPage(params, total, "group", "sonet_group.get");
			});			
		};
		
		
		
// заполнение селектового поля	
	Tabletask.prototype.GetSelectGroup = function(value = false)
	{
		let me = this, data = "<option>--Не выбрано--</option>";
		
		$.each(me.group, function(id, group) 
		{			
			if (value == group.ID) selected = "selected"; else selected = "";
			
			data += "<option "+selected+" value="+group.ID+">"+group.NAME+"</option>"		
		});		

		return data;
	}
	
//////////////////////////////////////////////////////////////////////////
			
		
//нажатие на кнопку "Сортировать"
		
		Tabletask.prototype.FilterTask = function()
		{
			me = this;		
			
			let object_form = $('#'+me.id_submit).parent().parent().parent();
			
			// объект со всеми элементами с заданным параметром (поля с результатами)
			let obValue = object_form.find('[data-filter="filter"]');				
			
			let selector = 0; // селектор для подсчета количества оборотов цикла
			
			let Configuration = [];	// конфигурация			
						
			// формирование конфигурации
			$.each(obValue, function(code, value) 
			{
				// текущая конфигурация				
				let template_settings = {};				
				template_settings.name = $(value).attr('id');				
				template_settings.value = $(value).val();			

				if ((template_settings.name.replace(/[0-9]/g, '') == "GROUP_ID") && (object_form.find('#NOT_GROUP_ID'+template_settings.name.replace(/[_A-Z]/g, '')).val() == 1))
				{ template_settings.notgroup = 1; }	else template_settings.notgroup = 0;			

				template_settings.hide = object_form.find('[data-hide="'+template_settings.name+'"]').val();	
				
				// сохранение значений конфигурации
				Configuration[selector] = template_settings; delete template_settings;
				
				selector++;	
			});
			
			// названчение баллов
			
			let mark = 100; 
			
			Configuration = Configuration.reverse();			
			
			$.each(Configuration, function(code, value) 
			{
				value.order = mark;	
				mark *= 10;
			});

			// сохраненние конфигурации			
			
			Configuration = JSON.stringify(Configuration);  // преобразорвание значений настроек в строку
				
			BX24.userOption.set("PARAMS", Configuration);   // сохранение настроек			
			
			// перед новой сортировкой удаляем старую, чтоб не мешалась в условиях
			$.each(me.task, function(id, elem) 
			{
				if (elem.order != undefined) delete me.task[id].order;
			});
			
			// перевывод задач			
			me.ConfigurationTask();
		}
		
		


// вывод div окна настроек

	Tabletask.prototype.PrintFilter = function()
	{
		let me = this;		
		let StringSettings = '';		
		
		let Configuration = me.VerifyСonfiguration();
		
		Configuration = JSON.parse(Configuration);
		
		Configuration = Configuration.reverse();

		//let Random = Math.floor(Date.now());		
			
		for(let value of Configuration) 
		{
			if (value != null)
			{
				Random = value.name.replace(/[_A-Z]/g, '');
								
				if (value.name.replace(/[0-9]/g, '') == "DEADLINE") 
				{
					StringSettings += '<div class="row DivParams__Item"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><span class="DivParams__Hamburger_Light DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title_Dark DivParams__Title"><span class="DivParams__Title__Inner">';
					
					StringSettings += '<span class="DivParams__Select DivParams__Select_Light"><select name="params" onchange="return Task.GetSample(this);">';
					StringSettings += '<option value="GROUP_ID" >Группа</option>';
					StringSettings += '<option value="PRIORITY">Важные задачи</option>';
					StringSettings += '<option value="DEADLINE" selected>До дедлайна осталось менее</option>';
					StringSettings += '<option value="DATE_START">Начатые задачи</option>';
					StringSettings += '</select></span>';				
					
					StringSettings += '</span></span></div><div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 simple"><input class="settings_input" type="text" name="count_days" value="'+value.value+'" id="DEADLINE'+Random+'" data-filter="filter"/> часов</div><div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><input onclick="return Task.ClickCheckbox(this, 1)" id="hide_DEADLINE'+Random+'" data-hide="DEADLINE'+Random+'" type="checkbox" name="hide'+Random+'" value="'+value.hide+'" /></div>';
					
					StringSettings += '<div onClick="return Task.DeleteParams(this, \'DEADLINE'+Random+'\');" class="DivParams__IconDelete"></div>';					
					
					StringSettings += '</div>';
				}
				if (value.name.replace(/[0-9]/g, '') == "PRIORITY") 
				{
					StringSettings += '<div class="row DivParams__Item"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner">';
					
					StringSettings += '<span class="DivParams__Select DivParams__Select_Light"><select name="params" onchange="return Task.GetSample(this);">';
					StringSettings += '<option value="GROUP_ID" >Группа</option>';
					StringSettings += '<option value="PRIORITY" selected>Важные задачи</option>';
					StringSettings += '<option value="DEADLINE">До дедлайна осталось менее</option>';
					StringSettings += '<option value="DATE_START">Начатые задачи</option>';
					StringSettings += '</select></span>';				
					
					StringSettings += '</span></span></div><div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 simple"><div class="inputGroup"><input id="PRIORITY'+Random+'" name="PRIORITY" onclick="return Task.ClickCheckbox(this, 2)" type="checkbox" data-filter="filter" value="'+value.value+'"/><label for="PRIORITY'+Random+'"></label></div></div><div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><input onclick="return Task.ClickCheckbox(this, 1)" data-hide="PRIORITY'+Random+'" id="hide_PRIORITY'+Random+'" type="checkbox" name="hide'+Random+'" value="'+value.hide+'" /></div>';
					
					StringSettings += '<div onClick="return Task.DeleteParams(this, \'PRIORITY'+Random+'\');" class="DivParams__IconDelete"></div>';					
					
					StringSettings += '</div>';
				}
				if (value.name.replace(/[0-9]/g, '') == "GROUP_ID") 
				{
										
					StringSettings += '<div class="row DivParams__Item"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner">';
									
					StringSettings += '<span class="DivParams__Select DivParams__Select_Light"><select name="params" onchange="return Task.GetSample(this);">';
					StringSettings += '<option value="GROUP_ID" selected>Группа</option>';
					StringSettings += '<option value="PRIORITY">Важные задачи</option>';
					StringSettings += '<option value="DEADLINE">До дедлайна осталось менее</option>';
					StringSettings += '<option value="DATE_START">Начатые задачи</option>';
					StringSettings += '</select></span>';	
					
					StringSettings += '</span></span></div><div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 simple"><select id="GROUP_ID'+Random+'" class="settings_select" data-filter="filter">'+me.GetSelectGroup(value.value)+'</select><div class="inputGroup"><input id="NOT_GROUP_ID'+Random+'" name="NOT_GROUP_ID" type="checkbox" onclick="return Task.ClickCheckbox(this, 1)" data-filter="filter_notgroup" value="'+value.notgroup+'"><label for="NOT_GROUP_ID'+Random+'">Все, кроме</label></div></div><div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><input type="checkbox" onclick="return Task.ClickCheckbox(this, 1)" data-hide="GROUP_ID'+Random+'" id="hide_GROUP_ID'+Random+'" name="hide'+Random+'" value="'+value.hide+'" /></div>';
					
					StringSettings += '<div onClick="return Task.DeleteParams(this, \'GROUP_ID'+Random+'\');" class="DivParams__IconDelete"></div>';					
					
					StringSettings += '</div>';
				}
				if (value.name.replace(/[0-9]/g, '') == "DATE_START") 
				{
					StringSettings += '<div class="row DivParams__Item"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner">'
									
					+ '<span class="DivParams__Select DivParams__Select_Light"><select name="params" onchange="return Task.GetSample(this);">'
					+ '<option value="GROUP_ID">Группа</option>'
					+ '<option value="PRIORITY">Важные задачи</option>'
					+ '<option value="DEADLINE">До дедлайна осталось менее</option>'
					+ '<option value="DATE_START" selected>Начатые задачи</option>'
					+ '</select></span>';	
									
					+ '</span></span></div>'
					
					+ '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 simple">'
						+ '<div class="inputGroup">'
							+ '<input id="DATE_START'+Random+'" name="DATE_START" onclick="return Task.ClickCheckbox(this, 1)" type="checkbox" data-filter="filter" value="'+value.value+'"/>'
							+ '<label for="DATE_START'+Random+'"></label>'
						+ '</div>'
					+ '</div>'
					
					+ '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">'
						+ '<input onclick="return Task.ClickCheckbox(this, 1)" data-hide="DATE_START'+Random+'" id="hide_DATE_START'+Random+'" type="checkbox" name="hide'+Random+'" value="'+value.hide+'" />'
					+ '</div>';
					
					+ '<div onClick="return Task.DeleteParams(this, \'DATE_START'+Random+'\');" class="DivParams__IconDelete"></div>'					
					+ '</div>';
				}
				
				
			}	
			
			
		}
		
		$('.'+me.id_settings).append(StringSettings);
		
		// отражение нажатых чекбоксов

		$.each($("input[type='checkbox']"), function(code, value) 
		{
			if (($(this).val() != 0) && ($(this).val() != "undefined"))
			{
				$(this).prop('checked',true);
			}
			
			if ($(this).val() == "undefined") $(this).val() == 0;			
		});		
		
		// перетаскивание				
        $('.DivParams__Item').arrangeable({dragSelector: '.DivParams__Hamburger'});
	}
		
/////////////////////////////вспомогательные функции///////////////////////////

	
	// обработка онкликов на чекбоксы
	Tabletask.prototype.ClickCheckbox = function(e, num) 
	{		
		let id = $(e).attr('id');	
		
		if ($('#'+id).prop('checked') == true) $('#'+id).val(num);
		else $('#'+id).val("0");
	}
		
		
// получение часов из даты в формате timestamp
	Tabletask.prototype.TimeConverter = function(UNIX_timestamp) {
	  let a = new Date(UNIX_timestamp * 1000);  
	  let hour = a.getHours();  
	  return hour;
	}	

//	формат даты для ее вывода в карточку задачи	
	Tabletask.prototype.FormatDate = function(ObjectDate)
	{	
		let Time = new Date(ObjectDate);
		Time = Time.toLocaleString();
		return Time;
	}

// дополнительный метод для объекта Date (перевод даты в формат timestamp)
	Date.prototype.getUnixTimestamp = function() {

		return Math.round(this.getTime() / 1000);

	}

// Закрытие модального окна	
	Tabletask.prototype.CloseModal = function() 
	{
		$('#modal_form')
			.animate({opacity: 0, top: '30px'}, 200,
				function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
		);
		
		return false;
	}	

// Открытие модального окна
	Tabletask.prototype.OpenModal = function()
	{
		$('#overlay').fadeIn(400,
		 	function(){
				$('#modal_form') 
					.css('display', 'block')
					.animate({opacity: 1, top: '10px'}, 200);
		});		
		
		$('.'+this.id_settings).empty(); // очистка окна настроек
		
		this.PrintFilter();	// вывод настроек	
		return false;
	};		


// Сортировка
	Tabletask.prototype.Sorting = function()
	{				
		this.CloseModal();
		this.FilterTask(); 
		return false;
	}	


/////////////////////////////вспомогательные функции///////////////////////////		
		
		
// получение часов из даты в формате timestamp
	Tabletask.prototype.TimeConverter = function(UNIX_timestamp) {
	  let a = new Date(UNIX_timestamp * 1000);  
	  let hour = a.getHours();  
	  return hour;
	}	

//	формат даты для ее вывода в карточку задачи	
	Tabletask.prototype.FormatDate = function(ObjectDate)
	{	
		let Time = new Date(ObjectDate);
		Time = Time.toLocaleString();
		return Time;
	}

// дополнительный метод для объекта Date (перевод даты в формат timestamp)
	Date.prototype.getUnixTimestamp = function() {

		return Math.round(this.getTime() / 1000);

	}

// Закрытие модального окна	
	Tabletask.prototype.CloseModal = function() 
	{
		$('#modal_form')
			.animate({opacity: 0, top: '30px'}, 200,
				function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
		);
		
		return false;
	}	

// Открытие модального окна
	Tabletask.prototype.OpenModal = function()
	{
		$('#overlay').fadeIn(400,
		 	function(){
				$('#modal_form') 
					.css('display', 'block')
					.animate({opacity: 1, top: '10px'}, 200);
		});		
		
		$('.'+this.id_settings).empty(); // очистка окна настроек
		
		this.PrintFilter();	// вывод настроек	
		return false;
	};		


// Сортировка
	Tabletask.prototype.Sorting = function()
	{				
		this.CloseModal();
		this.FilterTask(); 
		return false;
	}	

/**********************КОНСТРУКТОР ПАРАМЕТРОВ**********************/


	Tabletask.prototype.AddSimpleParam = function(Param)
	{	
		let me = this;	
		
		// выбранное поле
		let Selected = me.SetSelected(Param);
		
		let Random = Math.floor(Date.now());
		
		// правая часть
		let Sample = me.SetSample(Param, Random);	
		
		// левая часть
		let str = '<div class="row DivParams__Item"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><span class="DivParams__Hamburger_Dark DivParams__Hamburger DivParams__Hamburger_Move"><span class="DivParams__Hamburger__Icon"></span></span><span class="DivParams__Title"><span class="DivParams__Title__Inner">';
		
		str += '<span class="DivParams__Select DivParams__Select_Light"><select name="params" onchange="return Task.GetSample(this);"><option value="GROUP_ID" '+Selected["GROUP_ID"]+'>Группа</option><option value="PRIORITY" '+Selected["PRIORITY"]+'>Важные задачи</option><option value="DEADLINE" '+Selected["DEADLINE"]+'>До дедлайна осталось менее</option><option value="DATE_START" '+Selected["DATE_START"]+'>Начатые задачи</option></select></span>';		
		
		str += '</span></span></div><div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 simple">'+Sample+'</div><div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><input data-hide="DATE_START" type="checkbox" name="hide[]" value="" /></div>';	
		
		str += '<div class="DivParams__IconDelete" onClick="return Task.DeleteParams(this, \'DEADLINE'+Random+'\');"></div>';	
		
		str += '</div>'; 

		$('.DivParams').append(str);		
		
		// перетаскивание				
        $('.DivParams__Item').arrangeable({dragSelector: '.DivParams__Hamburger'});
		
		return false;
	}
	
	
	// при нажатии на селект фильстра с напором параметров
	Tabletask.prototype.GetSample = function(e)
	{
		let me = this;

		let Random = Math.floor(Date.now());
		
		// правая часть
		let Sample = me.SetSample(e.value, Random);
		
		// смена html правой части
//<------------------- too many parents. Use closest	
		
		$(e).closest('.DivParams__Item').find('.simple').html(Sample);
		
		return false;
	}


// подгружаемая индивидуальная часть параметра	
	Tabletask.prototype.SetSample = function(Param, Random)
	{	
		let Sample = ''; let me = this;
		
		switch (Param) 
		{						
			case 'DEADLINE':
				Sample = '<input class="settings_input" type="text" name="count_days" value="" id="DEADLINE'+Random+'" data-filter="filter"/> часов'; 
			break;
		
			case 'PRIORITY': 
				Sample = '<div class="inputGroup"><input id="PRIORITY'+Random+'" name="PRIORITY" onclick="return Task.ClickCheckbox(this, 2)" type="checkbox" data-filter="filter" value="" /><label for="PRIORITY'+Random+'"></label></div>'; 
			break;
		
			case 'GROUP_ID':
				Sample = '<select id="GROUP_ID'+Random+'" class="settings_select" data-filter="filter">'+me.GetSelectGroup()+'</select><div class="inputGroup"><input id="NOT_GROUP_ID'+Random+'" name="NOT_GROUP_ID" onclick="return Task.ClickCheckbox(this, 1)" type="checkbox" data-filter="filter_notgroup" value=""><label for="NOT_GROUP_ID'+Random+'">Все, кроме</label></div>'; 				
			break;
		
			case 'DATE_START':
				Sample = '<div class="inputGroup"><input id="DATE_START'+Random+'" name="DATE_START" onclick="return Task.ClickCheckbox(this, 1)" type="checkbox" data-filter="filter" value="" /><label for="DATE_START'+Random+'"></label></div>'; 
			break;
		}
		
		return Sample;		
	}
		
	
// определение активного параметра в селекте	
	Tabletask.prototype.SetSelected = function(Param)
	{
		let Selected = {};
		
		Selected['DEADLINE'] = Selected['PRIORITY'] = Selected['GROUP_ID'] = Selected['DATE_START'] = '';
		
		switch (Param) 
		{						
			case 'DEADLINE':   Selected['DEADLINE']   = "Selected"; break;		
			case 'PRIORITY':   Selected['PRIORITY']   = "Selected"; break;		
			case 'GROUP_ID':   Selected['GROUP_ID']   = "Selected"; break;		
			case 'DATE_START': Selected['DATE_START'] = "Selected"; break;
		}
		
		return Selected;	
	}
	

// удаление критерия сортировки 
	Tabletask.prototype.DeleteParams = function(e, id)
	{
		let me = this;
		
		// удаление слоя
		$(e).parent().remove();
				
		/***удаление критерия из конфигурации***/
		
		// получение конфигурации			
		
		let Configuration = me.VerifyСonfiguration();
		
		Configuration = JSON.parse(Configuration);		
			
		for(let value of Configuration) 
		{
			if ((value != null) && (value.name == id))
			delete value;
		}	
		
		Configuration = JSON.stringify(Configuration);  // преобразорвание значений настроек в строку
				
		BX24.userOption.set("PARAMS", Configuration);   // сохранение настроек			
	}
	
			
});