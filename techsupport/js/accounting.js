/**
 * Функция получения имени польователя по битриксовому объекту.
 * @param obUser - битриксовый объект пользователя
 * @param full - возвращать полное имя?
 * @returns {*}
 */
function getUserName(obUser, full) {
    let arName = [];

    if(obUser["LAST_NAME"] && obUser["LAST_NAME"].length) {
        arName.push(obUser["LAST_NAME"]);
    }

    if(obUser["NAME"] && obUser["NAME"].length) {
        arName.push(obUser["NAME"]);
    }

    if(full) {
        if(obUser["SECOND_NAME"] && obUser["SECOND_NAME"].length) {
            arName.push(obUser["SECOND_NAME"]);
        }
    }

    if(arName.length) {
        return arName.join(' ');
    } else {
        return obUser["EMAIL"] ? obUser["EMAIL"] : obUser["ID"];
    }
}

/**
 * Функция выполнения batch-запросов.
 * @param batchs - batch-запросы.
 * @param funcError - callback неуспешного выполнения запроса.
 * @param funcSuccess - callback успешного выполнения запроса.
 * @param funcEnd - callback после выполнения всех запросов.
 * @param progress_bar - jQuery-объект с прогресс-баром.
 * @param progress_step - шаг прогресс-бара.
 */
function bacthHandler(batchs, funcError, funcSuccess, funcEnd, progress_bar = false, progress_step = false)
{
    var ob, promises, additionalSecs;
    ob = this; promises = [], additionalSecs = 0;

    if(Object.keys(batchs).length == 1) {
        additionalSecs = 1;
    }

    promises = getBatchPromises(promises, batchs, additionalSecs, progress_bar, progress_step);

    Promise.all(promises).then(result => {
        var resBatch, data;
        resBatch = {}; data = {};

        for(var i in result) {
            var value = result[i];

            var b_id = value.index;
            var b_res = value.result;

            if(b_res.status == 500)
            {
                funcError(0, 500, false);
                continue;
            }

            data[b_id] = {};

            for(var id in b_res) {
                var res;
                res = b_res[id];

                if(res.error()) {
                    funcError(id, res.error(), res);
                }
                else {
                    funcSuccess(id, res.data(), res);
                }

                data[b_id][id] = res;
            }
        }

        funcEnd(data);
    });
}

function getBatchPromises(promises, batchs, secs, progress_bar, progress_step) {
    var ob;
    ob = this;

    function delayExec(num, resolve) {
        setTimeout(
            function() {
                try{
                    BX24.callBatch(batchs[num], function (result) {
                        if(result.status !== 500 && progress_bar && progress_step) {
                            var prev_progress = parseFloat(progress_bar.attr('aria-valuenow'));
                            var cur_progress = prev_progress + parseFloat(progress_step);
                            cur_progress = (Math.floor(cur_progress * 100)/100);
                            progress_bar.css('width', cur_progress + '%').attr('aria-valuenow', cur_progress).find(".js-pers_val").html(cur_progress + "%");
                        }

                        resolve({'index': num, 'result': result});
                    });
                } catch(err) {
                    resolve(false);
                }
            },
            (parseInt(num) + secs) * 1000
        );
    }

    if(Object.keys(batchs).length) {
        for (var n in batchs) {
            var pr = new Promise((resolve, reject) => {
                delayExec(n, resolve);
        });
            promises.push(pr);
        }
    } else {
        if(progress_bar && progress_step) {
            var prev_progress = parseFloat(progress_bar.attr('aria-valuenow'));
            var cur_progress = prev_progress + parseFloat(progress_step);
            cur_progress = +cur_progress.toFixed(2);
            progress_bar.css('width', cur_progress + '%').attr('aria-valuenow', cur_progress).html(cur_progress + "%");
        }
    }

    return promises;
}

$(document).ready(function() {
    var  accountingPeariodEl = $('#accounting_peariod');
    var  usersInfo = {};
    var  elapsedInfo = {};

    var itemsPerPage = 50;
    var presumablyUsersCount = 50;
    var presumablyElapsedCount = 50;
    var presumablyUsersPages = Math.ceil(presumablyUsersCount/itemsPerPage);
    var presumablyElapsedPages = Math.ceil(presumablyElapsedCount/itemsPerPage);

    var today = new Date();
    var beginIntDate = new Date(today.getFullYear(), today.getMonth(), 1, 0, 0, 0, 0);
    var endIntDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 23, 59, 59, 999);

    accountingPeariodEl.dateRangePicker({
        autoClose: true,
        separator: ' - ',
        startOfWeek: 'monday',
        format: 'DD.MM.YYYY',
        endDate:  "today",
        stickyMonths: false,
        autoClose: true,
        showTopbar: false,
        container:'#accounting_peariod_daterange_wrap',
        showShortcuts: false,
        setValue: function(s) {
            if(s != $(this).val()) {
                $(this).val(s);
            }
        },
    }).data('dateRangePicker').setDateRange(beginIntDate, endIntDate);

    BX24.init(function() {
        $('#form_report').click(function() {
            $(this).addClass('disabled');
            $('.loader-mark').css('display','inline-block');

            /**
             * Функция получения пользователей.
             * @param df - deferer
             */
            function getUsers(df) {
                Promise.all([]).then(function () {
                    return new Promise(function (resolve, reject) {
                        var batch = {};

                        for (var index = 0; index < presumablyUsersPages; index++) {
                            batch['USERS_GET-' + index] = {
                                'method': 'user.get',
                                'params': {'start': index * itemsPerPage, "FILTER": {}}
                            };
                        }

                        var batchs = {0: batch};

                        getCallCardDataBase(batchs);

                        function getCallCardDataBase(batchs) {
                            bacthHandler(
                                batchs,
                                function (id, error) {
                                    console.error("ERROR: " + error);
                                },
                                function (id, data) {
                                },
                                function (items) {
                                    var totals = {};
                                    var callCardData = items[0];

                                    if (callCardData) {
                                        $.each(callCardData, function (code, value) {
                                            if (value.error()) {
                                                alert('Ошибка запроса: ' + value.error());
                                                return;
                                            } else {
                                                var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                                switch (batchCode[1]) {
                                                    case 'USERS_GET':
                                                        var users = value.data();
                                                        var total = value.answer.total;

                                                        for (var user in users) {
                                                            usersInfo[users[user]["ID"]] = users[user];
                                                        }

                                                        if (total > presumablyUsersCount) {
                                                            totals['USERS_GET'] = total;
                                                        }

                                                        break;
                                                }
                                            }
                                        });

                                        resolve(totals);
                                    } else {
                                        getCallCardDataBase(batchs);
                                    }
                                }
                            );
                        }
                    });
                }).then(function (totals) {
                    return new Promise(function (resolve, reject) {
                        if (Object.keys(totals).length) {
                            var count = 0, b_count = 0, limit = 40, batchs = {};

                            for (var code in totals) {
                                var pages = Math.ceil(totals[code] / itemsPerPage);

                                switch (code) {
                                    case 'USERS_GET':
                                        for (var index = presumablyUsersPages; index < pages; index++) {
                                            if (count >= limit) {
                                                count = 0;
                                                b_count++;
                                            }

                                            if (!batchs[b_count]) {
                                                batchs[b_count] = {};
                                            }

                                            batchs[b_count]['USERS_GET-' + index] = {
                                                'method': 'user.get',
                                                'params': {'start': index * itemsPerPage, "FILTER": {}}
                                            };

                                            count++;
                                        }
                                        break;
                                }
                            }

                            if (Object.keys(batchs).length) {
                                getCallCardDataTails(batchs);

                                function getCallCardDataTails(batchs) {
                                    bacthHandler(
                                        batchs,
                                        function (id, error) {
                                            console.error("ERROR: " + error);
                                        },
                                        function (id, data) {
                                        },
                                        function (items) {
                                            $.each(items, function (i, callCardData) {
                                                $.each(callCardData, function (code, value) {
                                                    if (value.error()) {
                                                        alert('Ошибка запроса: ' + value.error());
                                                        return
                                                    } else {
                                                        var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                                        switch (batchCode[1]) {
                                                            case 'USERS_GET':
                                                                var users = value.data();

                                                                for (var user in users) {
                                                                    usersInfo[users[user]["ID"]] = users[user];
                                                                }

                                                                break;
                                                        }
                                                    }
                                                });

                                                delete batchs[i];
                                            });

                                            if (Object.keys(batchs).length) {
                                                getCallCardDataTails(batchs);
                                            } else {
                                                resolve();
                                            }
                                        }
                                    );
                                }
                            }
                        } else {
                            resolve();
                        }
                    })
                }).then(function () {
                    df.resolve();
                });
            }

            /**
             * Функция получения записей о затраченном времени.
             * @param df - deferer
             */
            function getElapsedData(df) {
                Promise.all([]).then(function () {
                    return new Promise(function (resolve, reject) {
                        var statisticGetCounter = 0;
                        var tasksElapsed = [];
                        var accountingРeariod = $('#accounting_peariod').val();
                        accountingРeariod = accountingРeariod.split('-');
                        var accountingDateFrom = accountingРeariod[0].trim();
                        var accountingDateTo = accountingРeariod[1].trim();
                        var endReached = false;

                        elapsedInfo = {};

                        getElapsedDataRecursive(1);

                        function getElapsedDataRecursive(part) {
                            var batch = {}, limit = 40;

                            for (var index = limit * (part - 1); index < limit * part; index++) {
                                batch['ELAPSED_GET-' + index] = {
                                    'method': 'task.elapseditem.getlist',
                                    'params': [
                                        {'ID': 'asc'},
                                        {
                                            '>=CREATED_DATE': accountingDateFrom + ' 00:00:00',
                                            '<=CREATED_DATE': accountingDateTo + ' 23:59:59'
                                        },
                                        ["*"],
                                        {
                                            'NAV_PARAMS': {
                                                "nPageSize": itemsPerPage,
                                                'iNumPage': index + 1
                                            }
                                        }
                                    ]
                                };
                            }

                            var batchs = {0: batch};

                            getElapsedDataTails(batchs);

                            function getElapsedDataTails(batchs) {
                                bacthHandler(
                                    batchs,
                                    function (id, error) {
                                        console.error("Ошибка запроса: " + error);
                                    },
                                    function (id, data) {
                                    },
                                    function (items) {
                                        $.each(items, function (i, elapsedData) {
                                            $.each(elapsedData, function (code, value) {
                                                if (value.error()) {
                                                    endReached = true;
                                                    return;
                                                } else {
                                                    var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                                    switch (batchCode[1]) {
                                                        case 'ELAPSED_GET':
                                                            var elItems = value.data();

                                                            if(elItems.length) {
                                                                tasksElapsed = tasksElapsed.concat(elItems);

                                                                elItems.forEach(function(elapsedItem) {
                                                                    if(typeof elapsedInfo[elapsedItem['USER_ID']] == "undefined") {
                                                                        elapsedInfo[elapsedItem['USER_ID']] = {};
                                                                    }

                                                                    if(typeof elapsedInfo[elapsedItem['USER_ID']][elapsedItem['TASK_ID']] == "undefined") {
                                                                        elapsedInfo[elapsedItem['USER_ID']][elapsedItem['TASK_ID']] = 0;
                                                                    }

                                                                    elapsedInfo[elapsedItem['USER_ID']][elapsedItem['TASK_ID']] += parseInt(elapsedItem['SECONDS']);
                                                                });
                                                            } else {
                                                                endReached = true;
                                                            }

                                                            break;
                                                    }
                                                }
                                            });

                                            delete batchs[i];
                                        });

                                        if (Object.keys(batchs).length && !endReached) {
                                            getElapsedDataTails(batchs);
                                        } else if(endReached) {
                                            resolve();
                                        } else {
                                            getElapsedDataRecursive(part + 1);
                                        }
                                    }
                                );
                            }
                        }


                        /*BX24.callMethod('task.elapseditem.getlist', [
                            {
                                'ID': 'desc'
                            }, {
                                '>=CREATED_DATE': accountingDateFrom + ' 00:00:00',
                                '<=CREATED_DATE': accountingDateTo + ' 23:59:59'
                            }
                        ], function(result) {
                            if(result.error())
                            {
                                alert('Ошибка запроса: ' + res.error());
                            }
                            else
                            {
                                if(result.more())
                                {
                                    statisticGetCounter ++;

                                    if(result.data())
                                    {
                                        tasksElapsed = tasksElapsed.concat(result.data());
                                    }

                                    if(statisticGetCounter % 100 == 0)
                                    {
                                        var next = function()
                                        {
                                            result.next();
                                        }

                                        setTimeout(next, 30000);
                                    }
                                    else
                                    {
                                        result.next();
                                    }
                                }
                                else
                                {
                                    tasksElapsed = tasksElapsed.concat(result.data());

                                    tasksElapsed.forEach(function(elapsedItem) {
                                        if(typeof elapsedInfo[elapsedItem['USER_ID']] == "undefined") {
                                            elapsedInfo[elapsedItem['USER_ID']] = {};
                                        }

                                        if(typeof elapsedInfo[elapsedItem['USER_ID']][elapsedItem['TASK_ID']] == "undefined") {
                                            elapsedInfo[elapsedItem['USER_ID']][elapsedItem['TASK_ID']] = 0;
                                        }

                                        elapsedInfo[elapsedItem['USER_ID']][elapsedItem['TASK_ID']] += parseInt(elapsedItem['SECONDS']);
                                    });

                                    resolve();
                                }
                            }
                        });*/
                    })
                }).then(function () {
                    df.resolve();
                });
            }

            var dFlow1 = $.Deferred();
            var dFlow2 = $.Deferred();

            $.when(dFlow1, dFlow2).done(function() {
				if(Object.keys(elapsedInfo).length) {
					var reportTable = $('<table class="accounting-table">\
						<tr>\
							<th>Сотрудник</th>\
							<th>Количество задач</th>\
							<th>Затрачено времени</th>\
						</tr>\
					</table>');

					var confUserNameID = {};
					var arUserCodes = [];

					for (var userID in elapsedInfo) {
						var userName = usersInfo[userID] ? getUserName(usersInfo[userID]): userID;

						confUserNameID[userName + ' ' + userID] = userID;
						arUserCodes.push(userName + ' ' + userID);
					}

					arUserCodes.sort(function(a, b) {
						if (a > b) {
							return 1;
						}

						return -1;
					});

					//for (var userID in elapsedInfo) {
					for (var userCode in arUserCodes) {
						var userID = confUserNameID[arUserCodes[userCode]];
						var userElapsedData = elapsedInfo[userID];
						var userName = usersInfo[userID] ? getUserName(usersInfo[userID]): userID;
						var userElapsed = Object.values(userElapsedData).reduce(function(sum, current) {
							return sum + current
						});
						userElapsed = fromSecToTime(userElapsed);


						reportTable.append('<tr>\
							<td>' + userName + '</td>\
							<td>' + Object.keys(userElapsedData).length + '</td>\
							<td>' + userElapsed + '</td>\
						</tr>');
					}
					
					$('#accounting-content').html(reportTable);
				} else {
					$('#accounting-content').html('<div class="no_entries">Нет ни одной записи!</div>');
				}

                $('.loader-mark').fadeOut();
                $('#accounting-content').fadeIn();

                BX24.fitWindow();

            });

            getUsers(dFlow1);
            getElapsedData(dFlow2);
        });
    });
});