$(document).ready(function() {
	
	var frameWidth = $('body').width();

BX24.init(function() {
	
	BX24.resizeWindow(frameWidth,600);
	
	window.reportCalendar();
	window.departamentList();
	window.createReport();
	window.useDepartamentList();
	return false;
});

window.moveIniciation = function(w,w2) {
	$('.content-wrap').on('click','.table-move',function() {
		var moveValue = $(this).data('move');
		var positionX = $('.days-table').position().left;
		switch(moveValue) {
			case 'left':	
							if (positionX <= 0)
								if (positionX > -50) positionX = -50;
								$('.days-table').stop().animate({'left': (positionX + 50)+'px'},150);
							break;
			case 'right':	
							if (positionX >= (-(w - w2)))
								$('.days-table').stop().animate({'left': (positionX - 50)+'px'},150);
							break;
		}
	});
	return false;
}

window.scrollTableMove = function(w,w2) {
	$('.content-wrap').on('mouseenter','.table-window',function() {
		
		window.onwheel = function(){ 
			return false;
		};
		$('.content-wrap').off('mousewheel');
		$('.content-wrap').on('mousewheel','.table-window',function(event) {
			
			var positionX;
			var e = event || window.event;
			var scrollDelta = e.deltaY || e.detail || e.wheelDelta;
			
			if (scrollDelta > 0) moveValue = 'left';
			else moveValue = 'right';
			positionX = $('.days-table').position().left;
			switch(moveValue) {
				case 'left':	
								if (positionX < 0) {
									var newPosition = positionX + 50;
									if (newPosition > 0)
										newPosition = 0;
									$('.days-table').css('left', newPosition+'px');
								}
								break;
				case 'right':	
								if (positionX > (-(w - w2))) {
									var newPosition = positionX - 50;
									if (newPosition < (-(w - w2)))
										newPosition = (-(w - w2));
									$('.days-table').css('left', newPosition+'px');
								}
								break;
			}
				
			return false;
		});
	});
	
	$('.content-wrap').on('mouseleave','.table-window',function() {
		window.onwheel = function(){ 
			return true;
		};
	});
	return false;
}

window.dragTableMovie = function(w,w2) {
	
	var startPointX;
	var mDownMark = false;
	var nowPointX;
	var lastPoint;
	$('.content-wrap').off('mousedown');
	$('.content-wrap').on('mousedown','.table-window',function(event) {
		
		event = event || window.event;
		if (event.which == 1) {
			mDownMark = true;
			startPointX = event.pageX;
			lastPoint = startPointX;
		}
		
		$('.content-wrap').on('mousemove','.table-window',function(event) {
			event = event || window.event;
			
			if (mDownMark) {
				nowPointX = event.pageX;

				var deltaMove =  nowPointX - lastPoint;
				var positionX = $('.days-table').position().left;
				
				var newPosition = positionX + deltaMove;
				if (newPosition > 0)
					newPosition = positionX;
				if (newPosition < (-(w - w2)))
					newPosition = positionX;

				$('.days-table').css('left', newPosition+'px');

				lastPoint = nowPointX;
			}
			
			return false;
			
		});
		
		return false;
	});

	$(document).mouseup(function() {
		mDownMark = false;
		return false;
	});
	
	return false;
}

var monthList = [
		'Январь',
		'Февраль',
		'Март',
		'Апрель',
		'Май',
		'Июнь',
		'Июль',
		'Август',
		'Сентябрь',
		'Октябрь',
		'Ноябрь',
		'Декабрь',
	];
	
var dayList = [31,28,31,30,31,30,31,31,30,31,30,31];

window.reportCalendar = function() {

	var now = new Date();
	var thisYear = parseInt(now.getFullYear());
	var thisMonth = parseInt(now.getMonth());
	var finishYear = thisYear - 10;
	
	//$('.month').append('<option value="'+thisMonth+'">'+monthList[thisMonth]+'</option>');
	
	for (var i=0; i<monthList.length; i++) {
		//if (i != thisMonth) {
			$('.month').append('<option value="'+i+'">'+monthList[i]+'</option>');
		//}
	}
	for (var k = thisYear; k > finishYear; k--) {
		$('.years').append('<option value="'+k+'">'+k+'</option>');
	}
	return false;
}

window.departamentList = function() {
	var preResult = new Array;
	BX24.callMethod('department.get', {}, function(result) {
		if (!result.error()) {
			if(result.more()) {
				if (result.data()) {
					preResult = preResult.concat(result.data());
				}
				result.next();
			}
			else {
				preResult = preResult.concat(result.data());
				var myRow = preResult;
				var myString = '';
				$('.departament').ready(function() {
					for (var i in myRow) {
						myString += '<option value="'+myRow[i].ID+'">..'+myRow[i].NAME+'</option>';
					}
					$('.departament').append(myString);
				});
			}
		}
	});
	return false;
}

window.useDepartamentList = function() {
	$('.departament').change(function() {
		window.createReportHandler(true);
		$('.date-location').children().remove();
	});
	return false;
}

window.getDate = function() {
	var selectedMonth = $('.month').val();
	var selectedYear = $('.years').val();
	var selectedDate = {
		MONTH: selectedMonth,
		YEAR: selectedYear
	};
	return selectedDate;
}

window.getDateToString = function() {
	var selectedMonth = parseInt($('.month').val());
	var selectedYear = parseInt($('.years').val());

	if (selectedMonth == 1) {
		if ((selectedYear%4 == 0 && selectedYear%100 != 0) || (selectedYear%400 == 0)) dayList[1] = 29;
		else dayList[1] = 28;
	}
	
	var realMonth = selectedMonth + 1;
	
	var selectedFiltrDate = {
		filterDateBegin: '01.'+realMonth+'.'+selectedYear+' 00:00:00',
		filterDateEnd: dayList[selectedMonth]+'.'+realMonth+'.'+selectedYear+' 23:59:59'
	}
	
	return selectedFiltrDate;
}

var countMark;
var countMark2;
var countTask;
var preResultUser;
var reloadReportMark;
var innerStopMark = false;

window.createReport = function() {
	$('.go-report').click(function() {
		reloadReportMark = false;
		innerStopMark = false;
		countMark = 0;
		countMark2 = 0;
		countTask = 0;
		preResultUser = new Array;
		$('.date-location').children().remove();
		BX24.resizeWindow(frameWidth,6000);
		window.inLoadStatus();
		window.createReportHandler(false);
		
	});
	return false;
}

window.stopDownload = function() {
	
	$('select').change(function() {
		reloadReportMark = true;
		if ($('.stop-report').css('display') != 'none') {
			$('button').css('background','#FF5D40');
			setTimeout(function() {
				$('button').css('background','transparent');
			},300);
		}
	});
	$('.stop-report').click(function() {
		reloadReportMark = true;
		$('button').css('background','#FF5D40');
		$('.stop-report').text('Остановка загрузки');
		setTimeout(function() {
			$('button').css('background','transparent');
		},300);
	});
	if (reloadReportMark) {
		window.outLoadStatus();
		$('.date-location').children().remove();
	}
	return reloadReportMark;
}

window.stopDownloadInner = function() {
	
	$('select').change(function() {
		innerStopMark = true;
		if ($('.stop-report').css('display') != 'none') {
			$('button').css('background','#FF5D40');
			$('.stop-report').text('Остановка загрузки');
			setTimeout(function() {
				$('button').css('background','transparent');
			},300);
		}
	});
	$('.stop-report').click(function() {
		innerStopMark = true;
		$('button').css('background','#FF5D40');
		setTimeout(function() {
			$('button').css('background','transparent');
		},300);
	});
	if (innerStopMark) {
		window.outLoadStatus();
		$('.date-location').children().remove();
	}
	return innerStopMark;
}

window.inLoadStatus = function() {
	$('.loader-mark').css('display','inline-block');
	$('.go-report').css('display','none');
	$('.stop-report').css('display','inline-block');
	return false;
}

window.outLoadStatus = function() {
	$('.loader-mark').css('display','none');
	$('.stop-report').css('display','none');
	$('.go-report').css('display','inline-block');
	$('.stop-report').text('Остановить загрузку');
	return false;
}

window.createReportHandler = function(e) {
	var selectedDep = $('.departament').val();
	var dateReport = window.getDate();
	var dateString = '<p>'+monthList[dateReport.MONTH]+' '+dateReport.YEAR+'</p>';
	$('.date-location').children().remove();
	$('.content-wrap').children().remove();
	$('.date-location').append(dateString);
	
	if (!window.stopDownload() && !reloadReportMark) {
		window.getGroupUsersFilter(dateReport.MONTH,dateReport.YEAR,selectedDep,e);
	}
	else {
		window.outLoadStatus();
		$('.date-location').children().remove();
		return false;
	}
	
	return false;
}

window.getGroupUsersFilter = function(m,y,u,e) {

	var arUsers = new Array;
	if (u != '') {
		var preResultUser = new Array;
		BX24.callMethod('user.get', {UF_DEPARTMENT : [u],ACTIVE : true}, function(result) {
			
			if (result.error()) {
				$('.content-wrap').children().remove();
				$('.content-wrap').append('<p class="no-info">Произошла ошибка</p>');
				$('.date-location').children().remove();
				window.outLoadStatus();
				return false;
			}
			else if(result.data() == '') {
				$('.content-wrap').children().remove();
				$('.content-wrap').append('<p class="no-info">В выбранном департаменте нет пользователей</p>');
				$('.date-location').children().remove();
				window.outLoadStatus();
				return false;
			}
			else {
				if(result.more()) {
					if (result.data()) {
						preResultUser = preResultUser.concat(result.data());
					}
					if (window.stopDownloadInner()) {
						innerStopMark = false;
						return false;
					}
					result.next();
				}
				else {
					preResultUser = preResultUser.concat(result.data());
					var myRow = preResultUser;
					
					var i = 0;
					for (user in myRow) {
						arUsers[i] = myRow[user].ID;
						i++;
					}
					if (!window.stopDownload() && !reloadReportMark) {
						window.getTaskValueFilter(m,y,u,arUsers,e);
					}
					else {
						window.outLoadStatus();
						$('.date-location').children().remove();
						return false;
					}
				}
			}
			return false;
		});
	}
	else {
		if (!window.stopDownload() && !reloadReportMark) {
			window.stopDownload();
			window.getTaskValueFilter(m,y,u,false,e);
		}
		else {
			window.outLoadStatus();
			$('.date-location').children().remove();
			return false;
		}
	}
	return false;
}

window.getTaskValueFilter = function(m,y,u,f,e) {
	var filterDate = window.getDateToString();
	var preResult = new Array;
	var arGroups = new Array;
	var arUsers = new Array;
	var methodFilter;
	
	if (f) {
		methodFilter = {
			'>=CREATED_DATE': filterDate.filterDateBegin,
			'<=CREATED_DATE': filterDate.filterDateEnd,
			'CREATED_BY':f
		};
	}
	else {
		methodFilter = {
			'>=CREATED_DATE': filterDate.filterDateBegin,
			'<=CREATED_DATE': filterDate.filterDateEnd
		};
	}
	
	BX24.callMethod(
			'task.item.list',
			[
				{ID : 'asc'},
				methodFilter
			],
			function(result) {
				
				if (result.error()) {
					$('.content-wrap').children().remove();
					$('.date-location').children().remove();
					$('.content-wrap').append('<p class="no-info">Произошла ошибка</p>');
					window.outLoadStatus();
					return false;
				}
				else {
					if(result.more()) {
						if (result.data()) {
							preResult = preResult.concat(result.data());
						}
						if (window.stopDownloadInner()) {
							innerStopMark = false;
							return false;
						}
						result.next();
					}
					else {
						preResult = preResult.concat(result.data());
						var myRow = preResult;
						
						var i = 0
						for (gropups in myRow) {
							
							if (arGroups.length > 0) {
								
								var cFlag = false;
								for (var k = 0; k < arGroups.length; k++) {
									if (arGroups[k] == myRow[gropups].GROUP_ID) {
										cFlag = true;
										break;
									}
								}
								if (!cFlag) {
									arGroups[i] = myRow[gropups].GROUP_ID;
								}
								else continue;
								
							}
							else {
								arGroups[i] = myRow[gropups].GROUP_ID;
							}
							
							i++;
						}
						
						
						var j = 0
						for (users in myRow) {
							
							if (arUsers.length > 0) {
								
								var uFlag = false;
								for (var c = 0; c < arUsers.length; c++) {
									if (arUsers[c] == myRow[users].CREATED_BY) {
										uFlag = true;
										break;
									}
								}
								if (!uFlag) {
									arUsers[j] = myRow[users].CREATED_BY;
								}
								else continue;
								
							}
							else {
								arUsers[j] = myRow[users].CREATED_BY;
							}
							
							j++;
						}

						if (!window.stopDownload() && !reloadReportMark) {
							window.stopDownload();
							window.createReportTable(m,y,u,arGroups,e,arUsers);
						}
						else {
							window.outLoadStatus();
							$('.date-location').children().remove();
							return false;
						}
					}
				}
				return false;
			}
	);
	
	return false;
}

window.createReportTable = function(m,y,u,t,e,us) {
	var preResult = new Array;
	var nullGroup = false;
	var calendarMarker = false;
	if (t.length > 0) {
		for (var i = 0; i < t.length; i++) {
			if (t[i] == '0') {
				nullGroup = true;
				t.splice(i,1);
				break;
			}
		}
		if (e) {
			$('.go-report').css('background','#FF5D40');
			setTimeout(function() {
				$('.go-report').css('background','transparent');
			},300);
		}
	
		if (countMark == 0) {
			countMark++;
			BX24.callMethod('sonet_group.get', {'FILTER': {"ACTIVE":"Y","CLOSED":"N","ID":t}}, function(result){
				$('.content-wrap').append('<div class="report-table" style="display:none;"></div>');
				if (result.error()) {
					$('.content-wrap').children().remove();
					$('.date-location').children().remove();
					$('.content-wrap').append('<p class="no-info">Произошла ошибка</p>');
					window.outLoadStatus();
					return false;
				}
				else {
					if(result.more()) {
						if (result.data()) {
							preResult = preResult.concat(result.data());
						}
						if (window.stopDownloadInner()) {
							innerStopMark = false;
							return false;
						}
						result.next();
					}
					else {
						preResult = preResult.concat(result.data());
						var myRow = preResult;
						
						var myString = '';
						$('.report-table').ready(function() {
							var marker = 0;
							for (var i in myRow) {
								if (myRow[i] !== undefined) {
									myString += '<div class="table-wrap"><table class="groups-table" id="groupID-'+myRow[i].ID+'">';
									if (marker == 0) {
										myString += '<tr class="head-line"><td>Сотрудники</td>';
										myString += '</tr><tr class="under-head"><td><div class="under-head-block-wrap"><div class="under-head-block">'+myRow[i].NAME+'</div></div></td></tr></table><div class="table-window"><div class="table-move to-left" data-move="left"></div><table class="days-table first-days-table" id="'+myRow[i].ID+'"><tr class="head-line">'+window.dayCell(m,y)+'</tr><tr class="under-head"><td></td></tr></table><div class="table-move to-right" data-move="right"></div></div>';
										calendarMarker = true;
									}
									else {
										myString += '<tr class="under-head"><td><div class="under-head-block-wrap"><div class="under-head-block">'+myRow[i].NAME+'</div></div></td></tr></table><div class="table-window"><table class="days-table" id="'+myRow[i].ID+'"><tr class="under-head"><td></td></tr></table></div>';
									}
									marker++;
								}
							}
							
							if (nullGroup) {
								if (calendarMarker) {
									myString += '<div class="table-wrap"><table class="groups-table" id="groupID-0">';
									myString += '<tr class="under-head"><td><div class="under-head-block-wrap"><div class="under-head-block">Вне группы</div></div></td></tr></table><div class="table-window"><table class="days-table" id="0"><tr class="under-head"><td></td></tr></table></div>';
								}
								else {
									myString += '<div class="table-wrap"><table class="groups-table" id="groupID-0"><tr class="head-line"><td>Сотрудники</td></tr><tr class="under-head"><td><div class="under-head-block-wrap"><div class="under-head-block">Вне группы</div></div></td></tr></table><div class="table-window"><div class="table-move to-left" data-move="left"></div><table class="days-table first-days-table" id="0"><tr class="head-line">'+window.dayCell(m,y)+'</tr><tr class="under-head"><td></td></tr></table><div class="table-move to-right" data-move="right"></div></div>'
								}
								
							}
							
							if (!window.stopDownload() && !reloadReportMark) {
								$('.report-table').append(myString);
								window.resultHandler(m,y,u,us);
							}
							else {
								window.outLoadStatus();
								$('.date-location').children().remove();
								return false;
							}
							
						});
					}
				}
			});
		}
	}
	else {
		$('.content-wrap').children().remove();
		$('.date-location').children().remove();
		$('.content-wrap').append('<p class="no-info">На выбранный период информация отсутствует. Пожалуйста, выберите другой временной период.</p>');
		window.outLoadStatus();
		return false;
	}
	return false;
}

window.resultHandler = function(m,y,u,us) {
	var preResultUser = new Array;
	var userFilter;
	
	if (countMark2 == 0) {
		
			if (u == '') {
				userFilter = {ACTIVE : true, ID : us};
			}
			else {
				userFilter = {UF_DEPARTMENT : [u],ACTIVE : true, ID : us}
			}
			
			BX24.callMethod('user.get', userFilter, function(result2) {	
				var preDomain = window.location.search.split('&');
				var myDomain = preDomain[0].split('=');
				if (result2.error()) {
					$('.content-wrap').children().remove();
					$('.content-wrap').append('<p class="no-info">Произошла ошибка</p>');
					$('.date-location').children().remove();
					window.outLoadStatus();
				}
				else if(result2.data() == '') {
					$('.content-wrap').children().remove();
					$('.content-wrap').append('<p class="no-info">Информация отсутствует</p>');
					$('.date-location').children().remove();
					window.outLoadStatus();
				}
				else {
					if(result2.more()) {
						if (result2.data()) {
							preResultUser = preResultUser.concat(result2.data());
						}
						if (window.stopDownloadInner()) {
							innerStopMark = false;
							return false;
						}
						result2.next();
					}
					else {
						preResultUser = preResultUser.concat(result2.data());
						var myRow2 = preResultUser;
						var userString = '';
						
							for (var k in myRow2) {
								if (myRow2[k] !== undefined) {
									
									if (myRow2[k].NAME == '' && myRow2[k].LAST_NAME == '') {
										myRow2[k].NAME = 'Зарегистрированный';
										myRow2[k].LAST_NAME = ' пользователь';
									}
									
									userString += '<tr class="user-'+myRow2[k].ID+' empty"><td class="user-cell"><a href="https://'+myDomain[1]+'/company/personal/user/'+myRow2[k].ID+'/" target="_blank" title="'+myRow2[k].NAME+' '+myRow2[k].LAST_NAME+'">'+myRow2[k].NAME+' '+myRow2[k].LAST_NAME+'</a></td>';
									userString += '</tr>';
									
									if ($('.under-head').length > 0) {
										$('.groups-table').find('.under-head').after(userString);
										userString = '';
										$('.days-table').find('.under-head').after('<tr class="user-'+myRow2[k].ID+' empty">'+window.dayCellToInfo(m,y)+'</tr>');
									}
									else {
										$('.content-wrap').children().remove();
										$('.content-wrap').append('<p class="no-info">Произошла ошибка. Отсутствуют права доступа к данной информации.</p>');
										$('.loader-mark').css('display','none');
										$('.date-location').children().remove();
									}
								}
							}
							if (!window.stopDownload() && !reloadReportMark) {
								window.tasksValueWrite();
							}
							else {
								window.outLoadStatus();
								$('.date-location').children().remove();
								return false;
							}
					}
				}
			});
		countMark2++;
	}
	
	return false;
}

window.dayCell = function(m,y) {
	if (m == 1) {
		if ((y%4 == 0 && y%100 != 0) || (y%400 == 0)) dayList[1] = 29;
		else dayList[1] = 28;
	}
	var dayString = '';
	var dayWeek;
	for (var i=1; i<=dayList[m]; i++) {
		dayWeek = new Date(y,m,i);
		dayString += '<td class="'+window.getDayWeekClass(dayWeek)+'2" data-day="'+i+'">'+i+'<br /><span class="dayweek">'+window.getDayWeek(dayWeek)+'</span></td>';
	}
	return dayString;
}

window.getDayWeek = function(d) {
	var arDayParam = String(d).split(' ');
	var ruDayweek = {
		Mon: 'Пн',
		Tue: 'Вт',
		Wed: 'Ср',
		Thu: 'Чт',
		Fri: 'Пт',
		Sat: 'Сб',
		Sun: 'Вс'
	};
	return ruDayweek[arDayParam[0]];
}

window.getDayWeekClass = function(d) {
	var arDayParam = String(d).split(' ');
	var ruDayweek = {
		Mon: '',
		Tue: '',
		Wed: '',
		Thu: '',
		Fri: '',
		Sat: 'yasumi',
		Sun: 'yasumi'
	};
	return ruDayweek[arDayParam[0]];
}

window.dayCellToInfo = function(m,y) {
	if (m == 1) {
		if ((y%4 == 0 && y%100 != 0) || (y%400 == 0)) dayList[1] = 29;
		else dayList[1] = 28;
	}
	var dayString = '';
	var m1 = m;
	m1++;
	if (m1 <10) m1 = '0'+m1;
	var i1;
	var dayWeek;
	for (var i=1; i<=dayList[m]; i++) {
		dayWeek = new Date(y,m,i);
		i1 = i;
		if (i1 <10) i1 = '0'+i1;
		dayString += '<td class="year'+y+'-month'+m1+'-day'+i1+' '+window.getDayWeekClass(dayWeek)+'" data-value="0" title="'+i1+'.'+m1+'.'+y+'"><span class="zero">0</span></td>';
	}
	return dayString;
}

window.tasksValueWrite = function() {
	if (!window.stopDownload() && !reloadReportMark) {
		var preResult = new Array;
		if (countTask == 0) {
			countTask++;
			var filterDate = window.getDateToString();
			
			BX24.callMethod(
				'task.item.list',
				[
					{ID : 'asc'},
					{
						'>=CREATED_DATE': filterDate.filterDateBegin,
						'<=CREATED_DATE': filterDate.filterDateEnd
					}
				],
				function(result)
				{
					if (result.error()) {
						$('.content-wrap').children().remove();
						$('.date-location').children().remove();
						$('.content-wrap').append('<p class="no-info">Произошла ошибка</p>');
						window.outLoadStatus();
						return false;
					}
					else {
						if(result.more()) {
							if (result.data()) {
								preResult = preResult.concat(result.data());
							}
							if (window.stopDownloadInner()) {
								innerStopMark = false;
								return false;
							}
							result.next();
						}
						else {
							preResult = preResult.concat(result.data());
							var myRow = preResult;
							var valueResult = 0;
							var dateArray;
							var preDateArray;
							var dayPoint;
							var taskValueArray;
							for (var i in myRow) {
								dateArray = String(myRow[i].CREATED_DATE).split('-');
								dayPoint = String(dateArray[2]).split('T');
								
								valueResult = $('table#'+myRow[i].GROUP_ID+' tr.user-'+myRow[i].CREATED_BY).children('td.year'+dateArray[0]+'-month'+dateArray[1]+'-day'+dayPoint[0]).attr('data-value');
								if (valueResult === undefined) valueResult = 0;
								valueResult = parseInt(valueResult);
								valueResult++;

								$('table#'+myRow[i].GROUP_ID+' tr.user-'+myRow[i].CREATED_BY).children('td.year'+dateArray[0]+'-month'+dateArray[1]+'-day'+dayPoint[0]).children().remove();
								$('table#'+myRow[i].GROUP_ID+' tr.user-'+myRow[i].CREATED_BY).children('td.year'+dateArray[0]+'-month'+dateArray[1]+'-day'+dayPoint[0]).append('<span class="with-result">'+valueResult+'</span>');
								$('table#'+myRow[i].GROUP_ID+' tr.user-'+myRow[i].CREATED_BY).children('td.year'+dateArray[0]+'-month'+dateArray[1]+'-day'+dayPoint[0]).attr('data-value',valueResult);
								
								$('table#'+myRow[i].GROUP_ID+' tr.user-'+myRow[i].CREATED_BY).addClass('no-empty');
								$('table#'+myRow[i].GROUP_ID+' tr.user-'+myRow[i].CREATED_BY).removeClass('empty');
								$('.groups-table#groupID-'+myRow[i].GROUP_ID+' tr.user-'+myRow[i].CREATED_BY).addClass('no-empty');
								$('.groups-table#groupID-'+myRow[i].GROUP_ID+' tr.user-'+myRow[i].CREATED_BY).removeClass('empty');
							
							}
							$('.empty').remove();
							$('.report-table').fadeIn(500);
							window.frameResize();
						}
					}
				}
			);
		}
	}
	else {
		window.outLoadStatus();
		$('.date-location').children().remove();
	}
	return false;
}

window.frameResize = function() {
		
		$('.content-wrap').css('width',frameWidth+'px');
		$('.report-table').css('width',frameWidth+'px');
		$('.table-wrap').css('width',frameWidth+'px');
		
		var compensationParam = 40;
		
		var tableWindowWidth = ((Math.floor((frameWidth-200)/50))*50);
		var dayTableWidth = $('.days-table').outerWidth();
		
		$('.calendar-place').css('width',(tableWindowWidth+200)+'px');
		
		$('.table-window').css('width',tableWindowWidth+'px');
		$('.under-head-block').css('width',(tableWindowWidth+200)+'px');
		
		window.moveIniciation(dayTableWidth,(tableWindowWidth + compensationParam));
		window.scrollTableMove(dayTableWidth,tableWindowWidth);
		window.dragTableMovie(dayTableWidth,tableWindowWidth);
		
		var minHeight = ($('.content-wrap').outerHeight()) + 350;
		
		BX24.resizeWindow(frameWidth,minHeight);
		
		window.outLoadStatus();

	return false;
}

});