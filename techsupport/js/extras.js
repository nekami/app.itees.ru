function showExtras(applicationID, containerID)
{
	var ideaLink = "mailto:sup@itees.ru?subject=Idea for application ";
	var problemLink = "mailto:sup@itees.ru?subject=Error in application ";
	var auth = BX24.getAuth();
	var domain = auth["domain"];
	
	ideaLink += '"' + applicationID + '"';
	problemLink += '"' + applicationID + '"';
	
	var extrasButton = $("<div class='extras_wrap'>" +
		"<div class='feat-red extras' id='extras' style='margin:0'>" +
			"<span class='extras_arrow_bottom'></span><span class='extras_btn_text'>Больше возможностей!</span>" +
		"</div>" +
		"<ul class='extras_menu' id='extras_menu'>" +
			"<li class='extras_section1_wrap'>" +
				"<ul class='extras_section1'>" +
					"<li class='extras_menu_item js-extras_menu_item'><a href='" + ideaLink + "'>Предложите нам идею!</a></li>" +
					"<li class='extras_menu_item js-extras_menu_item'><a href='" + problemLink + "'>Сообщите о проблеме приложения!</a></li>" +
				"</ul>" +
			"</li>" +
		"</ul>" +
	"</div>");
	
	var extrasMenu = extrasButton.find("#extras_menu");
	
	if(applicationID == "itees.phone")
	{
		extrasMenu.append($("<li class='extras_section2_wrap'>" +
			"<ul class='extras_section2'>" +
				"<li class='extras_menu_more'>Попробуйте другие возможности:</li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callsdetail/'>Детализация звонков</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.salescript/'>Звоните по скриптам!</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callplan/'>Планирование звонков</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.appointments/'>Запись на прием</a></li>" +
			"</ul>" +
		"</li>"));
	}
	else if(applicationID == "itees.callsdetail")
	{
		extrasMenu.append($("<li class='extras_section2_wrap'>" +
			"<ul class='extras_section2'>" +
				"<li class='extras_menu_more'>Попробуйте другие возможности:</li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.phone/'>Отчеты по звонкам</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.salescript/'>Звоните по скриптам!</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callplan/'>Планирование звонков</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.appointments/'>Запись на прием</a></li>" +
			"</ul>" +
		"</li>"));
	}
	else if(applicationID == "itees.salescript")
	{
		extrasMenu.append($("<li class='extras_section2_wrap'>" +
			"<ul class='extras_section2'>" +
				"<li class='extras_menu_more'>Попробуйте другие возможности:</li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.phone/'>Отчеты по звонкам</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callsdetail/'>Детализация звонков</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callplan/'>Планирование звонков</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.appointments/'>Запись на прием</a></li>" +
			"</ul>" +
		"</li>"));
		
		extrasMenu.append($("<li class='extras_section3_wrap'>" +
			"<ul class='extras_section3'>" +
				"<li class='extras_menu_item js-extras_menu_item'><a href='mailto:sup@itees.ru'>Заказать скрипт продаж</a></li>" +
			"</ul>" +
		"</li>"));
	}
	else if(applicationID == "itees.callplan")
	{
		extrasMenu.append($("<li class='extras_section2_wrap'>" +
			"<ul class='extras_section2'>" +
				"<li class='extras_menu_more'>Попробуйте другие возможности:</li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.phone/'>Отчеты по звонкам</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callsdetail/'>Детализация звонков</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.salescript/'>Звоните по скриптам!</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.appointments/'>Запись на прием</a></li>" +
			"</ul>" +
		"</li>"));
	}
	else if(applicationID == "itees.employeeburden")
	{
		extrasMenu.append($("<li class='extras_section2_wrap'>" +
			"<ul class='extras_section2'>" +
				"<li class='extras_menu_more'>Попробуйте другие возможности:</li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.phone/'>Отчеты по звонкам</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callsdetail/'>Детализация звонков</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.salescript/'>Звоните по скриптам!</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callplan/'>Планирование звонков</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.appointments/'>Запись на прием</a></li>" +
			"</ul>" +
		"</li>"));
	}
	else if(applicationID == "itees.appointments")
	{
		extrasMenu.append($("<li class='extras_section2_wrap'>" +
			"<ul class='extras_section2'>" +
				"<li class='extras_menu_more'>Попробуйте другие возможности:</li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.phone/'>Отчеты по звонкам</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callsdetail/'>Детализация звонков</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.salescript/'>Звоните по скриптам!</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a target='_blank' href='https://" + domain + "/marketplace/detail/itees.callplan/'>Планирование звонков</a></li>" +
			"</ul>" +
		"</li>"));
	}
	
	$("body").on("click", function(e)
	{
		if(!e["target"]["className"].match(/extras/))
		{
			$("#extras_menu").fadeOut(200);
		}
	});
	
	$("#" + containerID).on("click", "#extras", function()
	{
		$("#extras_menu").fadeToggle(200);
	});
	
	$("#" + containerID).append(extrasButton);
}