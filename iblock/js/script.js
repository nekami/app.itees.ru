$(function(){
	
	ApplicationIblock = new Iblock("me");

	BX24.init(function(){		
		ApplicationIblock.Main();		
	});			
		
	function Iblock(user_div) 
	{
		this.crumb = new Object; // хлебные крошки
		this.test = new Object; // иерархия всех инфоблоков
		this.user_div = user_div;
		this.GroupUpdateElem = new Object; // набо элементов для обновления
	}
	
	// формирование глобального объекта по всем инфоблокам
	Iblock.prototype.Main = function()
	{
		let me = this;
		
		//контейнеры для будущих ссылок и контента вкладок		
		$('#'+me.user_div).append('<div class="container-fluid"><div class="row">'		
		+'<ul class="col-md-12 TabContainer" id="addTabLink"></ul>'			
		+'<div class="TabContent" id="addTabContent"></div>' 		
		+'</div></div><div class="PopupWindow PopupWindowWithTitlebar" id="PopupWindow"><div class="PopupWindow__Title"><span class="PopupWindow__Title__Text"></span></div><div class="PopupWindow__Content">Вы уверены, что хотите удалить?</div><span onClick="ApplicationIblock.CloseModal();" class="PopupWindow__Icon PopupWindow__Icon_Close" style="top: 10px; right: 15px;"></span><div class="PopupWindow__Buttons"><span class="PopupWindow__Button PopupWindow__Button_Accept" id="Del_Accept">Продолжить</span><span class="PopupWindow__Button PopupWindow__Button_Link PopupWindow__Button_Link_Cancel" onClick="ApplicationIblock.CloseModal();">Отменить</span></div></div><div id="overlay" class="PopupWindowOverlay"></div>');			
					
	// инфоблоки	
		return new Promise(function(resolve, reject) 
		{						
			params = {};									
								
			BX24.callMethod(
				'entity.get',
				params,
				function(result)
				{	
					resolve(result.data());
				}
			);
				
		}).then(function(iblock)
		{	
			let i = 0;
			
			// в цикле получим всю инфу о каждом ИБ
			
			$.each(iblock, function(id_ib, val_ib) 
			{						
				me.crumb[val_ib.ENTITY] = {}; // индвидуальный объект для ИБ			
				
				// все категории
				return new Promise(function(resolve, reject) 
				{							
					params = {ENTITY: val_ib.ENTITY, SORT: {'NAME': 'ASC'}};									
											
					BX24.callMethod(
						'entity.section.get',
						params,
						function(result)
						{	
							resolve(result.data());
						}
					);
							
				}).then(function(cat)
				{	
					// все элементы
					return new Promise(function(resolve, reject) 
					{				
						params = {ENTITY: val_ib.ENTITY, SORT: {DATE_ACTIVE_FROM: 'DESC', ID: 'DESC'}};									
													
						BX24.callMethod(
							'entity.item.get',
							params,
							function(result)
							{	
								resolve(result.data());
							}
						);
									
					}).then(function(elem)  //ENTITY
					{										
									
						let StructureIblock = new Object;									
									
						if (cat != undefined)
						{
							let Dir = new Object;
											
							$.each(cat, function(id_cat, val_cat) 
							{
								if (val_cat.ENTITY == val_ib.ENTITY)
								{
									let arTempCat = new Object;
													
									if (elem != undefined)
									{
										let arTempElem = new Object;
														
										$.each(elem, function(id_elem, val_elem) 
										{
											if (val_elem.SECTION == val_cat.ID)
											{
												arTempElem[val_elem.ID] = val_elem;
											}
										});
										arTempCat.Elements = arTempElem;
									}
													
									arTempCat.Category = val_cat;
													
									Dir[val_cat.ID] = arTempCat;
													
									delete arTempCat;
									delete arTempElem;
								}
							});
											
							StructureIblock.Iblock = val_ib;											
							StructureIblock.Items = Dir;											
							delete Dir;
										
							// класс активности первой вкладки
							let Active = ""; if (i == 0) Active = "TabContainer__Item_Active";
											
							// кол-во вызовов = кол-ву ИБ
							if (Object.keys(StructureIblock).length != 0) me.PrintTable(StructureIblock, Active);
										
							i++;
						}; // конец проверки существования категории
					}); // конец промиса элементов							
				});	// конец промиса категорий				
			}); // конец цикла перебора инфоблоков
		});	// конец промиса инфоблоков
	}
	
	// вывод объекта ИБ во вкладку
	Iblock.prototype.PrintTable = function(Table, Active)
	{
		let me = this;	

		// для удаления собираем
		me.test[Table.Iblock.ENTITY] = {};
		me.test[Table.Iblock.ENTITY] = Table;		
		
		// + вкладка		
		let TabLink = '<li class="TabContainer__Item '+Active+'"><a href="#'+Table.Iblock.ENTITY+'" class="TabContainer__Item__Link"><span class="TabContainer__Item__Icon"></span><span class="TabContainer__Item__Text"><span class="TabContainer__Item__EditButton"></span><span class="TabContainer__Item__Title">'+Table.Iblock.NAME+'</span><span class="TabContainer__Item__DragButton"></span></span><span class="TabContainer__Item__Counter"></span></a></li>';
		$('#addTabLink').append(TabLink);		
		
		// заранее выводим каркас таблицы, чтобы потом туда писать строки
		let Сanvas = '<div id="'+Table.Iblock.ENTITY+'" class="'+Active+'">';				
		
		Сanvas += '<div class="col-md-12 Crumbs"><span class="Crumbs__Item"><a class="Crumbs__Link" href="#"><span class="Crumbs__Link__Title" onClick="return ApplicationIblock.ShowAllDir(\''+Table.Iblock.ENTITY+'\');">Разделы</span></a></span><span data-crumb="'+Table.Iblock.ENTITY+'"></span></div>';		
		
		Сanvas += '<div class="col-md-12 ManageIblock">';		
		
		//Сanvas += '<div class="Table_Contain">';		
		
		Сanvas += '<table><thead><tr class="ManageIblock__Thead__Tr" data-thead="'+Table.Iblock.ENTITY+'">';
		
		Сanvas += '<td class="MainIblockList__Checkbox"><input type="checkbox" onclick="ApplicationIblock.CheckedAll(this, \''+Table.Iblock.ENTITY+'\');" title="Отметить все/снять отметку у всех" name="CheckedAll" id="CheckedAll" value="" /></td>';		
		
		Сanvas += '<td class="ManageIblock__Action"><a href="javascript:void(0);" title="Настроить список" class="MainIblockList__Burger"><div class="MainIblockList__Burger__Empty"></div></a></td><td>ID</td><td colspan = "2">Название</td></tr></thead><tbody data-iblock = "'+Table.Iblock.ENTITY+'"></tbody></table>';
		
		/*Сanvas += '<div class="Orientation Orientation_Left Orientation_Show" style="transform: translate3d(0px, 48px, 0px);"></div><div class="Orientation Orientation_Right Orientation_Show" style="transform: translate3d(0px, 48px, 0px);"></div>';
		
		Сanvas += '</div>';	*/	
		
		// Групповая панель редактирования
		
		Сanvas += '<div class="Toolbar"><table cellpadding="0" cellspacing="0" border="0" class=""><tbody><tr>';	
		
		Сanvas += '<td class="Toolbar__Td" style="display:none;" id="action_buttons">';
		
		Сanvas += '<input type="button" name="save"   value="Сохранить" onclick="ApplicationIblock.SaveAll(\''+Table.Iblock.ENTITY+'\');" title="Сохранить изменения">';
		
		Сanvas += '<input type="button" name="cancel" value="Отменить"  onclick="ApplicationIblock.СancelAll(\''+Table.Iblock.ENTITY+'\');" title="Отменить режим редактирования">';
		
		Сanvas += '</td>';		
		
		Сanvas += '<td class="Toolbar__Td"><input title="Применить действие для всех записей в списке" type="checkbox" name="action_all" value="Y"></td><td class="Toolbar__Td"><label title="Применить действие для всех записей в списке" for="action_all">Для всех</label></td><td class="Toolbar__Td">';
		
		Сanvas += '<a href="javascript:void(0);" onclick="ApplicationIblock.EditAll(\''+Table.Iblock.ENTITY+'\')" title="Редактировать отмеченные записи" class="Toolbar__IconEdit"></a>';
		
		Сanvas += '</td><td class="Toolbar__Td"><a href="javascript:void(0);" onclick="return ApplicationIblock.ShowPopup(\''+Table.Iblock.ENTITY+'\');" title="Удалить записи" class="Toolbar__IconDel"></a></td></tr></tbody></table></div>';		
		
		Сanvas += '</div>';
		Сanvas += '</div>';	
		
		$('#addTabContent').append(Сanvas);		
		
		// получение пользовательсикх свойств
		Promise.all([]).then(function(){
			return new Promise(function(resolve, reject) 
			{									
				BX24.callMethod(
					'entity.item.property.get',
					{ENTITY: Table.Iblock.ENTITY},
					function(result)
					{	
						resolve(result.data());
					}
				);
			});			
		}).then(function(property)
		{	
			let str = '';
				
			$.each(property, function(id, val) 
			{
				str += '<td>'+val.NAME+'</td>';
			});		

			$('[data-thead="'+Table.Iblock.ENTITY+'"]').append(str);				
		});		
		
		// масив id категорий для поиска зависимых категорий
		var arCatId = [];
		$.each(Table.Items, function(id, val) 
		{
			arCatId.push(val.Category.ID);
		});
		
		let TabContent = '';
		
		$.each(Table.Items, function(code, val_item) 
		{			
			let DataRoot = "see"; let ParentCategory = "";
			if (arCatId.indexOf(val_item.Category.SECTION) != -1) 
			{
				DataRoot = "nosee";
				ParentCategory = val_item.Category.SECTION;
			}
		
			TabContent += '<tr data-iblock="'+Table.Iblock.ENTITY+'" data-category="'+val_item.Category.ID+'" data-root="'+DataRoot+'" data-parent__category="'+ParentCategory+'" class="MainIblockList__Content" data-type="category"><td class="MainIblockList__Checkbox"><input type="checkbox" name="ID[]" id="'+val_item.Category.ID+'" value="cat" title="Отметить для редактирования"></td><td class="ManageIblock__Action"><a href="javascript:void(0);" title="Действия" class="MainIblockList__Burger"><div class="MainIblockList__Burger__Empty"></div></a></td><td>'+val_item.Category.ID+'</td>';
			
			TabContent += '<td style="width:36px;"><div class="MainIblockList__IconFolder_Small MainIblockList__IconFolder"></div></td>';
			
			TabContent += '<td><div class="MainIblockList__Td__Name">';			
			
			TabContent += '<div data-field="input" data-property="NAME"><a onClick="ApplicationIblock.ToogleDir(\''+Table.Iblock.ENTITY+'\', \''+val_item.Category.ID+'\');">'+val_item.Category.NAME+'</a></div>';
			
			TabContent += '</div></td></tr>';

			$.each(val_item.Elements, function(id_elem, val_elem) 
			{
				TabContent += '<tr data-iblock="'+Table.Iblock.ENTITY+'" data-categoryElem="'+val_item.Category.ID+'" data-elem="'+val_elem.ID+'" data-root="nosee" class="MainIblockList__Content"><td class="MainIblockList__Checkbox"><input type="checkbox" name="ID[]" id="'+val_elem.ID+'" value="elem" title="Отметить для редактирования"></td><td class="ManageIblock__Action"><a href="javascript:void(0);" title="Действия" class="MainIblockList__Burger"><div class="MainIblockList__Burger__Empty"></div></a></td><td>'+val_elem.ID+'</td>';				
				
				TabContent += '<td style="width:36px;"><div class="MainIblockList__IconItem_Small MainIblockList__IconItem MainIblockList__Icon"></div></td>';
				
				TabContent += '<td><div class="MainIblockList__Td__Name">';
				
				TabContent += '<div data-field="input" data-property="NAME"><a>'+val_elem.NAME+'</a></div>';
				
				TabContent += '</div></td>';
				
				$.each(val_elem.PROPERTY_VALUES, function(code_prop, val_prop) 
				{						
					TabContent += '<td data-field="input" data-property="'+code_prop+'">'+val_prop+'</td>';					
				});					
				
				TabContent += '</tr>';				
					
			});
			
		});			
		
		// добавление таблицы в соответствующий ей инфоблок
		$('[data-iblock = "'+Table.Iblock.ENTITY+'"]').append(TabContent); delete TabContent;

		// скрываем файлы и подкатегории
		$('[data-root="nosee"]').hide();		
		
		/*$('[data-type="category"]').click(function(){				
			me.ToogleDir($(this).attr('data-iblock'), $(this).attr('data-category'));				
		});*/	
		
		BX24.resizeWindow($(document).width(), $(document).height());
		
		
		//$('.Orientation').height($('.Table_Contain').height());
		
		
		/**ДВИЖЕНИЕ ВКЛАДОК **/
		$('.TabContainer li').click(function() {
			
			$('.TabContainer li').removeClass('TabContainer__Item_Active');
			$(this).addClass('TabContainer__Item_Active');
			
			let label = $(this).find('a').attr("href");
			
			$('.TabContent div').removeClass('TabContainer__Item_Active');
			$('.TabContent div'+label).addClass('TabContainer__Item_Active');	

			me.ShowAllDir(Table.Iblock.ENTITY);
		});
	}

// переход к элементам текущего раздела
	Iblock.prototype.ToogleDir = function(code_iblock, id_cat)
	{
		let me = this;		
	
	// видимость файловой структры (путешествие внутри дерева без крох)
		$('.MainIblockList__Content[data-iblock="'+code_iblock+'"]').hide();
	// видимые элементы
		$('[data-iblock="'+code_iblock+'"][data-categoryElem='+id_cat+']').show();
	// видимые подкатегории	
		$('[data-iblock="'+code_iblock+'"][data-parent__category="'+id_cat+'"]').show();		
			
		me.Breadcrumb(code_iblock, id_cat);
		
		// чистка от старых тотально выделенных чекбоксов
		$('#CheckedAll').prop('checked',false);			
		return false;
	}
	
// видимость всем (переход в корень)	
	Iblock.prototype.ShowAllDir = function(code_iblock)
	{		
		let me = this;		
		$('[data-root="nosee"]').hide();		
		$('[data-root="see"]').show();		
		me.crumb[code_iblock] = {};		
		$('[data-crumb="'+code_iblock+'"]').empty();

		// чистка от старых тотально выделенных чекбоксов
		$('#CheckedAll').prop('checked',false);	
		return false;
	}	
	
// хлебные крошки	
	Iblock.prototype.Breadcrumb = function(code_iblock, id_cat)
	{	
		let me = this;		
		
		return new Promise(function(resolve, reject) 
		{
			Promise.all([]).then(function(){
				return new Promise(function(resolve, reject) 
				{	
					params = {ENTITY: code_iblock, SORT: {}, FILTER: {'ID': id_cat}};					
										
					BX24.callMethod(
						'entity.section.get',
						params,
						function(result)
						{	
							resolve(result.data());
						}
					);
				});
			}).then(function(cat)
			{	
				if (cat != undefined)
				{				
					
					if (me.crumb[code_iblock][cat[0].ID] == undefined) 
					{ 
						me.crumb[code_iblock][cat[0].ID] = cat[0].NAME;	
					}
					
					let str = ""; let templateCrumb = new Object;
					
					$.each(me.crumb[code_iblock], function(id, val) 
					{	
						templateCrumb[id] = val;
					
						str += '<span data-crumbs="'+id+'" class="Crumbs__Item"><a class="Crumbs__Link" onClick="return ApplicationIblock.ToogleDir(\''+code_iblock+'\', \''+id+'\');"><span class="Crumbs__Link__Title">'+val+'</span></a></span>';

						if (id == cat[0].ID) { return false; }
					});	
					
					me.crumb[code_iblock] = templateCrumb;	
						
					$('[data-crumb="'+code_iblock+'"]').html(str);
				}
			});
		});
	}
	
	
	Iblock.prototype.SearchSubItems = function(code_iblock, parent_category)
	{
		let me = this;
		let SubItems = new Object; // для id подкатегорий (для передачи рекурсии)
		
		// удаление категорий				
		BX24.callMethod('entity.section.delete', {ENTITY: code_iblock, ID: parent_category});
		
		$('[data-iblock="'+code_iblock+'"][data-category="'+parent_category+'"]').empty();
			
		$.each(me.test[code_iblock].Items, function(alias, item) 
		{
			// подкатегорий
			if (item.Category.SECTION == parent_category) 
			{					
				BX24.callMethod('entity.section.delete', {ENTITY: code_iblock, ID: item.Category.ID});
				$('[data-iblock="'+code_iblock+'"][data-parent__category="'+parent_category+'"]').empty();
				
				SubItems[item.Category.ID] = item.Category.ID;
			}				
			
			// элементов
			$.each(item.Elements, function(id, elem) 
			{								
				if (elem.SECTION == parent_category) 
				{							
					BX24.callMethod('entity.item.delete', {
						ENTITY: code_iblock,
						ID: elem.ID
					});				

					$('[data-iblock="'+code_iblock+'"][data-categoryElem='+parent_category+']').empty();
				}	
			});
					
		});	

		// повторение экзикуции для всех подкатегорий		
		for (let i in SubItems) { me.SearchSubItems(code_iblock, i); }		
	}	
	
	
	
	Iblock.prototype.CloseModal = function()
	{		
		$('#PopupWindow')
			.animate({opacity: 0, top: '30px'}, 200,
				function(){
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
		);	
	}
	
		
	Iblock.prototype.ShowPopup = function(code_iblock)
	{
		let me = this;
		
		if ($('input[name="ID[]"]:checked').length != 0) 
		{		
			let me = this;
			
			$('#overlay').fadeIn(400,
		 	function(){
				$('#PopupWindow') 
					.css('display', 'block')
					.animate({opacity: 1, top: '10px'}, 200);
			});	
			
			$('#Del_Accept').click(function()
			{
				me.DeleteItems(code_iblock);
				me.CloseModal();
			});
		}	
				
		return false;
	}	
	
// групповое удаление	
	Iblock.prototype.DeleteItems = function(code_iblock)
	{		
		let me = this;

		$.each($('input[name="ID[]"]:checked'), function(code, val) 
		{			
			if ($(val).val() == "cat") 
			{
				me.SearchSubItems(code_iblock, $(val).attr('id'));
			}			
			
			if ($(val).val() == "elem")
			{
				BX24.callMethod('entity.item.delete', {
					ENTITY: code_iblock,
					ID: $(val).attr('id')
				});
				$(this).parent().parent().empty();
			}			
		});
		
		return false;
	}


// выделить Все
	Iblock.prototype.CheckedAll = function(e, code_iblock)
	{
		// нажатие отрабатывать для активного инфоблока и видимых категорий и элементов		
					
		$.each($('tbody[data-iblock="'+code_iblock+'"] input[name="ID[]"]').parent().parent(), function(code, val) 
		{	
			if ($(val).css('display') == 'table-row')
			{					
				if ($(e).prop("checked") == true) $(val).find('input[name="ID[]"]').prop('checked',true);
				else $(val).find('input[name="ID[]"]').prop('checked',false);					
			}
			else { $(val).find('input[name="ID[]"]').prop('checked',false); }				
		});		
		
		// остальные в рамках данного ифоблока должны отжиматься назад, если были нажаты
		// и для них сбрасывать глобальный чекбокс
		
		
		//console.log();		
	}
	
		
// Групповая кнопка "Изменить"
	Iblock.prototype.EditAll = function(code_iblock)
	{
		let me = this;
		
		$('.Toolbar__Td').hide();
		$('.Toolbar__Td#action_buttons').show();
		
		// недоступность для отметки новых элементов или категорий
		$('tbody[data-iblock="'+code_iblock+'"] input[name="ID[]"]').prop('disabled',true);
		
		// все нажатые сделать доступными для редактирования
		
		$.each($('tbody[data-iblock="'+code_iblock+'"] input[name="ID[]"]:checked'), function(code, val) 
		{	
			let tr = $(this).parent().parent().find('[data-field="input"]');
			
			if ($(this).parent().parent().css('display') == 'table-row')
			{			
				$.each(tr, function(id, content) 
				{
					let value;
					
					if ($(this).attr('data-property') == "NAME") 
						 value = $(content).find("a").html();
					else value = $(content).html();				
					
					$(content).html('<input type="text" name="'+$(this).attr('data-property')+'" value="'+value+'"/>');					
				});	
			}
		});			
	}
	
	
// сохранение изменений
	Iblock.prototype.SaveAll = function(code_iblock)
	{
		let me = this;
	
		$.each($('tbody[data-iblock="'+code_iblock+'"] input[name="ID[]"]:checked'), function(code, val) 
		{
			let tr = $(this).parent().parent().find('[data-field="input"]');			
			let id = $(val).attr("id");			
			let name = "";
			let Section = $(this).parent().parent().attr("data-categoryelem");
			let property = new Object;					
			
			// имя и пользовательские свойства
			$.each(tr, function(id, content) 
			{
				if ($(this).attr('data-property') == "NAME") name = $(content).find("input[type='text']").val();
				else property[$(this).attr('data-property')] = $(content).find("input[type='text']").val();
			});

			if ($(val).val() == "cat") 
			{
				BX24.callMethod('entity.section.update', {
					ENTITY: code_iblock, 
					ID: id, 
					NAME: name
				});
			}			
			
			if ($(val).val() == "elem")
			{						
				BX24.callMethod('entity.item.update', {
					ENTITY: code_iblock,
					ID: id,
					DATE_ACTIVE_FROM: new Date(),				
					NAME: name,
					PROPERTY_VALUES: property,
					SECTION: Section
				});			
			}			
		});	

		me.СancelAll(code_iblock);		
	}
	
	
// отмена режима редактирования без сохранения внесенных изменений
	Iblock.prototype.СancelAll = function(code_iblock)
	{			
		$('.Toolbar__Td').show();
		$('.Toolbar__Td#action_buttons').hide();
		
		// вернуть чекбоксам активность
		$('tbody[data-iblock="'+code_iblock+'"] input[name="ID[]"]').prop('disabled',false);
		
		$.each($('tbody[data-iblock="'+code_iblock+'"] input[name="ID[]"]:checked'), function(code, val) 
		{	
			let tr = $(this).parent().parent().find('[data-field="input"]');
			
			let id_cat = $(val).attr("id");	

			if ($(val).val() == "cat") 
			{
				$.each(tr, function(id, content) 
				{
					$(content).html('<a onclick="ApplicationIblock.ToogleDir(\''+code_iblock+'\', \''+id_cat+'\');">'+$(content).find("input[type='text']").val()+'</a>');		
				});						
			}			
			
			if ($(val).val() == "elem")
			{						
				$.each(tr, function(id, content) 
				{
					if ($(this).attr('data-property') == "NAME")
						$(content).html('<a>'+$(content).find("input[type='text']").val()+'</a>');	
					else
						$(content).html($(content).find("input[type='text']").val());					
				});	
			}
			
		});	
	}
	
});

				
				// создание инфоблока
				/*BX24.callMethod('entity.add', {'ENTITY': 'ambulance', 'NAME': 'Скорая помощь', 'ACCESS': {'AU':'X'}}, function(result)
				{	
					console.log(result.error());
				});
				
				BX24.callMethod('entity.add', {'ENTITY': 'mvd', 'NAME': 'МВД', 'ACCESS': {'AU':'X'}}, function(result)
				{	
					console.log(result.error());
				});
				
				BX24.callMethod('entity.add', {'ENTITY': 'emercom', 'NAME': 'МЧС', 'ACCESS': {'AU':'X'}}, function(result)
				{	
					console.log(result.error());
				});*/
				
				/*
				// получение инфоблока
				BX24.callMethod('entity.get', {}, function(result)
				{	
					console.log(result.data());
				});*/
				
				
				// создание категорий
				/*BX24.callMethod('entity.section.add', {ENTITY: 'ambulance', 'NAME': 'Скорая на базе УАЗ'});
				
				BX24.callMethod('entity.section.add', {ENTITY: 'ambulance', 'NAME': 'Скорая на базе Fiat Ducato'});
				
				BX24.callMethod('entity.section.add', {ENTITY: 'mvd', 'NAME': 'Кинологическая служба'});
				
				BX24.callMethod('entity.section.add', {ENTITY: 'emercom', 'NAME': 'Пожарно-техническая лаборатория'});
				
				BX24.callMethod('entity.section.add', {ENTITY: 'emercom', 'NAME': 'Аварийно-спасательный'});*/
				
				
				//BX24.callMethod('entity.section.add', {ENTITY: 'ambulance', 'NAME': 'Тестовая подкатегория', 'SECTION': 3144});
				
				
				/*
				// получение категорий
				BX24.callMethod('entity.section.get', {ENTITY: 'ambulance', SORT: {'NAME': 'ASC'}}, function(result)
				{	
					console.log(result.data());
				});
				*/
				
				// ! позже создание пользовательских свойств у элемента 
				
			//// создание элементов для каждой из категорий
				/*BX24.callMethod('entity.item.add', {
					ENTITY: 'ambulance',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Санитарный УАЗ',
					ACTIVE: 'Y',					
					SECTION: 3144
				});
				
				BX24.callMethod('entity.item.add', {
					ENTITY: 'ambulance',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'УАЗ Скорая помощь, класс B',
					ACTIVE: 'Y',					
					SECTION: 3144
				});*/
				
			/////
			/*
				BX24.callMethod('entity.item.add', {
					ENTITY: 'ambulance',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Скорая Форд Транзит, класс А',
					ACTIVE: 'Y',					
					SECTION: 3154
				});
				
				BX24.callMethod('entity.item.add', {
					ENTITY: 'ambulance',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Скорая Форд Транзит, класс B',
					ACTIVE: 'Y',					
					SECTION: 3154
				});
				
				BX24.callMethod('entity.item.add', {
					ENTITY: 'ambulance',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Реанимация Форд Транзит',
					ACTIVE: 'Y',					
					SECTION: 3154
				});*/
				
			/////
			/*
				BX24.callMethod('entity.item.add', {
					ENTITY: 'ambulance',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Скорая помощь Fiat, класс А',
					ACTIVE: 'Y',					
					SECTION: 3146
				});
			
				BX24.callMethod('entity.item.add', {
					ENTITY: 'ambulance',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Скорая помощь Fiat, класс B',
					ACTIVE: 'Y',					
					SECTION: 3146
				});
				
				BX24.callMethod('entity.item.add', {
					ENTITY: 'ambulance',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Реанимация Fiat Ducato',
					ACTIVE: 'Y',					
					SECTION: 3146
				});*/
				
				
				
				/*BX24.callMethod('entity.item.add', {
					ENTITY: 'mvd',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Кинелогическая служба на базе ГАЗель',
					ACTIVE: 'Y',					
					SECTION: 3148
				});*/			
				
				
				BX24.callMethod('entity.item.add', {
					ENTITY: 'emercom',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Тестовый товар 1',
					ACTIVE: 'Y',					
					SECTION: 3150
				});
				
				BX24.callMethod('entity.item.add', {
					ENTITY: 'emercom',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Тестовый товар 2',
					ACTIVE: 'Y',					
					SECTION: 3150
				});				
				
				BX24.callMethod('entity.item.add', {
					ENTITY: 'emercom',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Тестовый товар 3',
					ACTIVE: 'Y',					
					SECTION: 3152
				});
				
				BX24.callMethod('entity.item.add', {
					ENTITY: 'emercom',
					DATE_ACTIVE_FROM: new Date(),
					DETAIL_PICTURE: '',
					NAME: 'Тестовый товар 4',
					ACTIVE: 'Y',					
					SECTION: 3152
				});