<!DOCTYPE html>
<html>
	<head>
		<title>Управление инфоблоками</title>	
		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet" />		
		<link href="css/style.css?<?=rand();?>" rel="stylesheet" />	
		<script src="js/jquery-3.1.0.min.js"></script>			
		<script src="js/b24_rest_api.js"></script>		
		<script src="js/script.js?<?=rand();?>"></script>		
	</head>
	<body>	
		<div id="me"></div>					
	</body>	
</html>