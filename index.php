<!DOCTYPE html>
<html>
	<head>
		<title>Управление инфоблоками</title>	
		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">		
		<script src="js/jquery-3.1.0.min.js"></script>			
		<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>		
	</head>
	<body>
	
<!--
		<div id="components-demo">
		  <button-counter></button-counter>
		  <button-counter></button-counter>
		  <button-counter></button-counter>
		</div>
	
		<script>
			// Определяем новый компонент, названный button-counter
			
			// название компонента - это название будущего тега
			
			Vue.component('button-counter', {
			  data: function () {
				return {
				  count: 0
				}
			  },
			  template: '<button v-on:click="count++">Счётчик кликов — {{ count }}</button>'
			});
			
			// в вызове сообщаем где
			new Vue({ el: '#components-demo' });
		</script>

-->
				
		<div id="components">
		  <button-counter></button-counter>
		  
		  <blog-post title="My journey with Vue"></blog-post>
		  <blog-post title="Blogging with Vue"></blog-post>
		  <blog-post title="Why Vue is so fun"></blog-post>
		  
		  <!--<blog-post
			  v-for="post in posts"
			  v-bind:key="post.id"
			  v-bind:title="post.title"
		  ></blog-post>
		  -->
		  
		  <!-- с помощью v-bind можно устанавливать атрибуты тэга -->		  
		  <ul>
				<li v-for="post in posts" v-bind:key="post.id" v-bind:title="post.title">{{ post.title }}</li>
		  </ul>
		  
		  <blog-post
			  v-for="post in posts"
			  v-bind:key="post.id"
			  v-bind:post="post"
		  ></blog-post>
		  
		</div>
		
		<script>			
			Vue.component('button-counter', {
			  data: function () {
				return {
				  count: 0
				}
			  },
			  template: '<button v-on:click="count++">Счётчик кликов — {{ count }}</button>'
			});			
						
			Vue.component('blog-post', {
			  props: ['title'],
			  template: '<h3>{{ title }}</h3>'
			});
			
			Vue.component('blog-post', {
			  props: ['post'],
			  template: '<div class="blog-post">'
							+'<h3>{{ post.title }}</h3>'
							+'<div v-html="post.content"></div>'
						+'</div>'
			});			
			
			new Vue({
			  el: '#components',
			  data: {
				posts: [
				  { id: 1, title: 'My journey with Vue' },
				  { id: 2, title: 'Blogging with Vue' },
				  { id: 3, title: 'Why Vue is so fun' }
				]
			  }
			});			
		</script>
		
<!-- 		
		
		<div id="app">
			<h1>{{ message }}</h1>
			<input type="text" v-model="message">
			<pre>{{ $data | json }}</pre>
			
			<ul>
				<li v-for="bear in bears">{{ bear }}</li>
			</ul>
		</div>
		
		<script>
			data = {
				message: "Цвет настроенья синий",
				bears: ['гризли', 'белый', 'панда', 'губач']
			};
		
			new Vue({				
				el: "#app",
				data: data
			});				
		</script>-->
		
	</body>	
</html>