var ownCompanies = [];
var ownRequisites = [];
var company_entity_type_id = 4;

function __b24cpc_getRequisites(companyIDs)
{
	return new Promise(function(resolve, reject) 
	{
		if(!companyIDs.length)
		{
			resolve();
			return;
		}
		
		var own_requisites = [];
		
		BX24.callMethod("crm.requisite.list",
		{
			filter: {"ENTITY_TYPE_ID": company_entity_type_id, "ENTITY_ID": companyIDs},
			select: [ "ID", "NAME", "*"]
		},
		function(result)
		{ 
			if(result.error())
			{			
				console.error(result.error());
				resolve();
			}			
			else
			{
				if(result.more())
				{
					if(result.data())
					{
						own_requisites = own_requisites.concat(result.data());
					}

					result.next();
				}
				else
				{
					if(result.data())
					{
						own_requisites = own_requisites.concat(result.data());
					}
						
					ownRequisites = own_requisites;
						
					resolve();
				}
			}
		});
	});
}

function __b24cpc_getCompanies()
{
	return new Promise(function(resolve, reject) 
	{
		var own_companies = [];
		
		BX24.callMethod("crm.company.list", {
			filter: {"IS_MY_COMPANY": "Y"}, 				
			select: ["ID", "TITLE", "PHONE", "EMAIL"] 			
		}, 
		function(result)
		{
			if(result.error())
			{			
				console.error(result.error());
				resolve([]);
			}			
			else
			{
				if(result.more())
				{
					if(result.data())
					{
						own_companies = own_companies.concat(result.data());
					}

					result.next();
				}
				else
				{
					if(result.data())
					{
						own_companies = own_companies.concat(result.data());
					}
					
					ownCompanies = own_companies;
					
					var companyIDs = [];
					
					for(var index in own_companies)
					{
						companyIDs.push(own_companies[index]["ID"]);
					}
					
					resolve(companyIDs);
				}
			}
		});
	});
}

function b24cpc_getCount(obj)
{
	var counter = 0;
	for(var prop in obj)
	{
		counter ++;
	}
	
	return counter;
}

// Получаем информацию о приложении
function b24cpc_getAppInfo(df)
{
	try{
		BX24.callMethod("app.info", {}, function(res)
		{
			if(res.error())
			{
				console.log('ошибка запроса: ' + res.error());
				df.resolve(false);
			}
			else
			{
				var appInfo = res.data();
			
				df.resolve(appInfo);
			}
		});
	}
	catch(e)
	{
		df.resolve(false);
	}
}

// Получаем информацию о компании клиента
function b24cpc_getOwnCompaniesInfo(df)
{
	try{
		__b24cpc_getCompanies()
		.then(__b24cpc_getRequisites)
		.then(function()
		{
			var obRequisites = {};
			
			$.each(ownRequisites, function(i, req)
			{
				$.each(req, function(j, field)
				{
					if(!field)
					{
						delete req[j];
					}
				});
				
				if(!obRequisites[req["ENTITY_ID"]])
				{
					obRequisites[req["ENTITY_ID"]] = [];
				}
				
				obRequisites[req["ENTITY_ID"]].push(req);
			});
			
			$.each(ownCompanies, function(i, company)
			{
				if(obRequisites[company["ID"]])
				{
					company["REQUISITES"] = obRequisites[company["ID"]];
				}
				
				delete company["ID"];
			});
			
			df.resolve(ownCompanies);
		});
	}
	catch(e)
	{
		df.resolve(false);
	}
}

// Получаем количество пользователей портала
function b24cpc_getUserCount(df)
{
	try{
		BX24.callMethod('user.get', {}, function(res)
		{ 
			if(res.error())
			{
				console.log('ошибка запроса: ' + res.error());
				df.resolve(false);
			}
			else
			{
				var count = res.total();
			
				df.resolve(count);
			}
		});
	}
	catch(e)
	{
		df.resolve(false);
	}
}

// Получаем информацию о текущем пользователе
function b24cpc_getCurrentUserInfo(df)
{
	try{
		BX24.callMethod('user.current', 
		{}, 
		function(res)
		{
			if(res.error())
			{
				console.log('ошибка запроса: ' + res.error());
				df.resolve(false);
			}
			else
			{
				var curUserInfo = res.data();
				
				df.resolve(curUserInfo);
			}
		});
	}
	catch(e)
	{
		df.resolve(false);
	}
}

function b24cpc_sendAppInfo(appInfo, curUserInfo, userCount, ownCompaniesInfo, action, isUpdate)
{
	return new Promise(function(resolve, reject)
	{
		var url = "https://cp.itees.ru/dev/b24cpclients/app_handler.php";
		var data = {};
		
		if(userCount)
		{
			data.user_count = userCount;
		}
	
		if(curUserInfo)
		{
			var userFields = [
				"NAME",
				"SECOND_NAME",
				"LAST_NAME",
				"EMAIL",
				"PERSONAL_MOBILE",
				"PERSONAL_PHONE",
				"PERSONAL_BIRTHDAY",
				"WORK_POSITION",
				"WORK_PHONE",
				"UF_FACEBOOK",
				"UF_LINKEDIN",
				"UF_SKYPE",
				"UF_TWITTER",
			];
			var contactInfo = {};
			
			$.each(userFields, function(i, field)
			{
				if(curUserInfo[field])
				{
					contactInfo[field] = curUserInfo[field];
				}
			});
			
			if(b24cpc_getCount(contactInfo))
			{
				data.contact_info = contactInfo;
			}
		}
	
		if(appInfo)
		{
			data.app_info = appInfo;
		}
		
		if(ownCompaniesInfo)
		{
			data.company_info = ownCompaniesInfo;
		}
		
		if(b24cpc_getCount(data))
		{
			var curDate = (new Date()).valueOf();
			var auth = BX24.getAuth();
			
			if(!auth || !auth.domain)
			{
				resolve();
			}
			
			data.date = curDate;
			data.action = action;
			data.domain = auth.domain;
			data.is_update = isUpdate?1:0;
			
			var ajaxData = {
				url: url,
				type: "POST",
				dataType: "json",
				data: data,
				complete: function(res)
				{
					resolve();
				}
			}
			
			$.ajax(ajaxData);
		}
		else
		{
			resolve();
		}
	});
}

// Событие установки модуля
function b24cpc_sendAppInfo__install(df, isUpdate)
{
	var d_userCount = $.Deferred();
	var d_appInfo = $.Deferred();
	var d_curUserInfo = $.Deferred();
	var d_ownCompaniesInfo = $.Deferred();

	$.when(
		d_appInfo,
		d_curUserInfo,
		d_userCount,
		d_ownCompaniesInfo,
	)
	.done(function (
		appInfo, 
		curUserInfo, 
		userCount,
		ownCompaniesInfo,
		)
	{
		b24cpc_sendAppInfo(appInfo, curUserInfo, userCount, ownCompaniesInfo, "install", isUpdate)
		.then(function(){
			df.resolve();
		});
	});

	b24cpc_getUserCount(d_userCount);
	b24cpc_getAppInfo(d_appInfo);
	b24cpc_getCurrentUserInfo(d_curUserInfo);
	b24cpc_getOwnCompaniesInfo(d_ownCompaniesInfo);
}

// Событие сброса триала
function b24cpc_sendAppInfo__trialReset()
{
	var d_appInfo = $.Deferred();
	
	$.when(d_appInfo)
	.done(function (appInfo) {		
		var app_status = appInfo["STATUS"];
		var cur_trial_days_left = appInfo["DAYS"];
		
		if(app_status == "T") //TRIAL
		{
			var prev_trial_days_left = BX24.appOption.get("trial_days_left");
		
			if(typeof prev_trial_days_left != 'undefined' && prev_trial_days_left < cur_trial_days_left)
			{
				var d_userCount = $.Deferred();
				var d_curUserInfo = $.Deferred();
				var d_ownCompaniesInfo = $.Deferred();

				$.when(
					d_curUserInfo, 
					d_userCount,
					d_ownCompaniesInfo,
				)
				.done(function (
					curUserInfo, 
					userCount, 
					ownCompaniesInfo
					) 
				{
					b24cpc_sendAppInfo(appInfo, curUserInfo, userCount, ownCompaniesInfo, "trial_reset", false);
				});

				b24cpc_getUserCount(d_userCount);
				b24cpc_getCurrentUserInfo(d_curUserInfo);
				b24cpc_getOwnCompaniesInfo(d_ownCompaniesInfo);
			}
		}
		
		BX24.appOption.set("trial_days_left", cur_trial_days_left);
	});
	
	b24cpc_getAppInfo(d_appInfo);
}