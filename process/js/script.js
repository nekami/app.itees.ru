$(function(){	
	
	/******************ОБРАБОТКА КЛИКОВ ДЛЯ ВСЕХ УСТРОЙСТВ******************/	
	
	var clickEventType = ((document.ontouchstart!==null)?'click':'touchstart');

	$('body').on(clickEventType, '#ShowPanel', function(e){
		e.preventDefault();
		BusinessProcess.ShowPanel();	
	});
		
	$('body').on(clickEventType, '#CloseModalIcon', function(e){
		BusinessProcess.CloseModal();	
	});
	
	$('body').on(clickEventType, '#CloseModalOk', function(e){
		BusinessProcess.CloseModal();	
	});
	
	$('body').on(clickEventType, '#open_line_btn', function(e){
		BX24.openApplication({action: 'popup', page: 'open_line'});	
	});
	
	/****************************************************/		
	
	BusinessProcess = new Process("ListProcess", "SetProperty", "SwitchBox", "message");
	
	BX24.init(function()
	{		
		// размеры окна
		BX24.resizeWindow($(window).width(), screen.height);
		
		// логика процессов	
		BusinessProcess.Main();		
		
		BusinessProcess.Select2("InicialSelect2Date");
	});	
		
	function Process(ListProcess, SetProperty, idOption, idMessage) 
	{
		this.ListProcess = ListProcess; // селект для списка процессов
		this.SetProperty = SetProperty; // блок для вывода свойств в виде элементов формы		
		this.idOption = idOption; // id для вывода процессов в настройках
		this.idMessage = idMessage; // id для вывода сообщения о публикации процесса		
		this.obProcess = new Object; // активные процессы
		this.obAllProcess = new Object; // все процессы
		this.AllProperty = new Object; // свойства для всех процессов
		this.Configuration = new Object; // конфигурация процессов		
		this.GlobalConf = new Object; // опции в настройках "выбрать все", "убрать все"
	}
	
	Process.prototype.Main = function()
	{		
		let me = this;		
			
		// получение процессов без свойств			
		return new Promise(function(resolve, reject) 
		{
			params = {'IBLOCK_TYPE_ID': 'bitrix_processes'};

			BX24.callMethod(
				'lists.get',
				params,
				function(result)
				{					
					resolve(result.total());
				}
			);				
				
		}).then(function(total)
		{			
			me.getAllPage(params, total, "lists.get");			
		});		
		
	}
	
/* формирование объекта процессов */	
	
	Process.prototype.FormationObjectProcess = function(processes)
	{
		let me = this;			
	
	/******************************************************************/			
		// формирование глобального массива процессов
		$.each(processes, function(id, item) 
		{
			let template_processes = new Object;					
			template_processes.NAME = item.NAME;				
			me.obProcess[item.ID] = template_processes;									
		});
				
		me.obAllProcess = me.obProcess; // кол-во me.obProcess может сокращаться в зависмости от активных		
		
		var batchs = {};
		
		$.each(me.obAllProcess, function(id, item) 
		{
			params = {
				'IBLOCK_TYPE_ID': 'bitrix_processes',
				'IBLOCK_ID': id
			};			

			// формирование batch-объекта			
			batchs[id] = {
				'method': "lists.field.get", 
				'params': Object.assign({'key': id}, params)
			};	
			
		});	

		me.SetProperties(batchs);

/******************************************************************/
		me.PrintContent();

		// очистка прелоадера	
		$('#loading_ind').remove();			
	}
	
// 	формирование объекта свойств процессов	
	Process.prototype.SetProperties = function(batchs)
	{	
		let me = this;
		
		var promises = [];
		
		var pr = new Promise((resolve, reject) => {	
					
			setTimeout(
				function() {
					BX24.callBatch(batchs, function (result) 
					{						
						BX24.refreshAuth();
						resolve(result);
					});
				}
			);					
					
		});
		
		promises.push(pr);
		
		Promise.all(promises).then(result => {
			let resBatch, data;
			resBatch = {}; data = {};			
			
			result.forEach(function(value) {
				$.extend(resBatch, value);
			});

			for(let id in resBatch) 
			{
				let res;
				res = resBatch[id];				
				data[id] = res.data();			
			}						
		
			me.AllProperty = data;			
		});		
	}	

// вывод контента	
// и передача id активного селекта до нажатия кнопки Выбрать

	Process.prototype.PrintContent = function(IdSelected = "000")
	{
		let me = this;	

		// достаем конфигурацию		
		me.СheckConfiguration(); // процессы в настройках
			
		me.GetGlobalConf(); // конфигурация "Выбрать все", "Убрать все"
		
		/**********СЕЛЕКТ И ПОЛЯ ФОРМЫ*********/
		
		let srt = '<table class="TableProcess"><tr><td><label class="TableProcess__TD">Процесс:</label><span class="TableProcess__Select ParamsSelect2Date"><select class="InicialSelect2Process" id="ListProcess"><option value="000">--Не выбрано--</option>';	
		let Acvtive = ''; // отслеживает появление активного значения селекта из текущего набора процессов		
		
		
		$.each(me.obProcess, function(id, item) 
		{			
			if (((me.Configuration == undefined) || $.isEmptyObject(me.Configuration)) && (me.GlobalConf["not"] == false))
			{
				srt += '<option value="'+id+'">'+item.NAME+'</option>';					
			}
			else if (me.Configuration[id] == true)
			{					
				if (IdSelected == id) 
				{ 				
					Acvtive = "selected"; 
					srt += '<option '+Acvtive+' value="'+id+'">'+item.NAME+'</option>';
				}
				else { srt += '<option value="'+id+'">'+item.NAME+'</option>'; 	}
			}			
		});	
			
		
		srt += '</select></span></td></tr></table>';

		// если нет активного, убираем форму
		if (Acvtive != "selected") $('#'+me.SetProperty).empty();	

		// вывод списка процессов
		$('#mainContent').html(srt);

					
						
		// считывание выбранного				
		$('#'+me.ListProcess).on('change', function() {	

			if ( $('#'+me.ListProcess).val() != 000 ) {	
				// запрос его набора полей					
				me.GetProperty($(this).val());
			} else { $('#'+me.SetProperty).empty(); }
		});

		/**********НАСТРОЙКИ*********/			

		// установка селекта с датой соотвественно настройкам		
		let ConfDateFormat = me.ConfDateFormat(); let titleDate;

		switch(ConfDateFormat) 
		{
			case 'dd.mm.yyyy': titleDate = "ДД.ММ.ГГГГ"; break;			
			case 'mm/dd/yyyy': titleDate = "ММ/ДД/ГГГГ"; break;
		}	
		
		if ($("#pril_format_date").find('[value = "'+ConfDateFormat+'"]').length != 0)
		{ 
			//$("#pril_format_date").find('[value = "'+ConfDateFormat+'"]')[0].selected = true;
			$('#select2-pril_format_date-container').attr("title", ConfDateFormat);				
			$('#select2-pril_format_date-container').html(titleDate);			
		}	
		
		me.Select2("InicialSelect2Process");
	   		
	}
	
	
	Process.prototype.GetProperty = function(id_process)
	{
		let me = this;
		
		let str = '<table class="TableProcess">';				
				
		$.each(me.AllProperty[id_process], function(id, item) 
		{	
			if ((item.TYPE == "NAME") || (item.TYPE == "S") || (item.TYPE == "N") || (item.TYPE == "N:Sequence") || (item.TYPE == "SORT") || (item.TYPE == "L") || (item.TYPE == "S:Date") || (item.TYPE == "S:DateTime") || (item.TYPE == "S:HTML") || (item.TYPE == "PREVIEW_TEXT") || (item.TYPE == "DETAIL_TEXT"))
			{	
				// обязательность поля
				let required = ''; let requiredClass = '';
				if (item.IS_REQUIRED == "Y") 
				{
					requiredClass = '<span class="TableProcess__Required">*</span>';
					required = "required";
				}			
						
				// видимость поля
				let show = '';
				if (item.SETTINGS.SHOW_ADD_FORM == "N") show = 'style="display: none;"';

				str += '<tr '+show+'><td><label class="TableProcess__TD">'+item.NAME+':'+requiredClass+'</label>';
						
				/***типы полей***/					
						
				// название процесса
				if (item.TYPE == "NAME")
				{						
					str += '<input data-send="'+id_process+'" type="text" value="'+me.obProcess[id_process].NAME+'" name="'+item.FIELD_ID+'"/>';
				}

				if (item.SETTINGS.SHOW_ADD_FORM != "N")
				{			
					// числа
					if ((item.TYPE == "N:Sequence") || (item.TYPE == "N"))
					{						
						str += '<input '+required+' data-send="'+id_process+'" type="text" name="'+item.FIELD_ID+'" data-format="numeric"/>';					
					}
					
					// строка
					if ((item.TYPE == "SORT") || (item.TYPE == "S"))
					{
						str += '<input '+required+' data-send="'+id_process+'" type="text" name="'+item.FIELD_ID+'"/>';
					}					
							
					// список
					if (item.TYPE == "L")
					{
						str += ' <span class="TableProcess__Select ParamsSelect2Date"><select class="InicialSelect2" '+required+' data-send="'+id_process+'" name="'+item.FIELD_ID+'">';
								
						$.each(item.DISPLAY_VALUES_FORM, function(id_option, val_option) 
						{
							str += '<option value="'+id_option+'">'+val_option+'</option>';
						});
								
						str += '</select></span>';						
					}					
							
					// дата
					if ((item.TYPE == "S:Date") || (item.TYPE == "S:DateTime"))   
					{
						str += '<input '+required+' readonly="true" data-send="'+id_process+'" name="'+item.FIELD_ID+'" type="text" class="bx-lists-input-calendar datepicker-here" data-position="bottom center" />';
					}
							
					// текстовое полей
					if ((item.TYPE == "S:HTML") || (item.TYPE == "PREVIEW_TEXT") || (item.TYPE == "DETAIL_TEXT"))
					{	
						str += '<textarea '+required+' data-send="'+id_process+'" id="editor'+item.ID+'" name="'+item.FIELD_ID+'"></textarea>';
					}					
				}
				/****************************/
						
				str += '</td></tr>';
			}				
					
		});

		str += '</table>';					

		// добавление html
		$('#'+me.SetProperty).html(str);	
		
		me.Select2("InicialSelect2");		
		
		// подключение скриптов после формирования DOM
		me.IncludeScript('datepicker/js/datepicker.min.js');		

		// если там поля, стоит ли выводить кнопку?
		let FindMe = $('#'+me.SetProperty).find('[data-send="'+id_process+'"]').length;				
		if (FindMe != 0) 
		{
			// код кнопки
			$('#'+me.SetProperty).append('<div class="ButtonsSet"><button style="float: right;" onClick="return BusinessProcess.StartProcess(\''+id_process+'\')" class="ButtonsSet__Button ButtonsSet__Button_Primary">Запуск процесса</button></div>');
		}

		// текущий формат даты
		let conf_date_format = me.ConfDateFormat();			
			
		// определение полей с типом даты			
		$('.bx-lists-input-calendar').datepicker({
			dateFormat: conf_date_format,
			autoClose: true
		});		
		
		// интерактивная проверка полей даты на заполненность
		$('.bx-lists-input-calendar').focusout(function() {			
			if (($(this).attr("required") == "required") && ($(this).val().length > 0 ) && ($(this).val().length < 2 )) { 		
				$(this).parent().addClass("Error");							
			} else {
				$(this).parent().removeClass("Error");
			}		
		});	
		
		// интерактивная проверка текстовых полей формы на заполненность
		$(document).on("input",function(ev){		
			if (($(ev.target).attr("required") == "required") && ($(ev.target).val().length == 0)) { 		
				$(ev.target).parent().addClass("Error");							
			} else {				
				if ($(ev.target).parent().hasClass('Error')) { $(ev.target).parent().removeClass("Error"); }
			}
		});	
		
		// меняет МАСКУ числовым полям
		$('[data-format="numeric"]').inputmask({mask:"9{0,}", max:'infinite'});	
		
		// размеры окна
		BX24.resizeWindow($(window).width(), $(document).height());
	}
		
/**************************************************/	

// проверка конфигурации процессов
	Process.prototype.СheckConfiguration = function()
	{
		let me = this;
		let Configuration = BX24.userOption.get("CHECKBOX");		
		if ((Configuration != undefined) && (Configuration != 'undefined')) { me.Configuration = JSON.parse(Configuration); }
	}	

// проверка конфигурации даты	
	Process.prototype.ConfDateFormat = function()
	{		
		let conf_date_format = BX24.userOption.get("DATE_FORMAT");
		
		if ((conf_date_format != undefined) && (conf_date_format != 'undefined')) { conf_date_format = JSON.parse(conf_date_format); }
		else { conf_date_format = "dd.mm.yyyy" }
		
		return conf_date_format;	
	}

// проверка конфигурации глобальных настроек
	Process.prototype.GetGlobalConf = function()
	{	
		let me = this;
	
		let GlobalConf = BX24.userOption.get("GLOBAL_CHECKBOX");		
		
		if ((GlobalConf != undefined) && (GlobalConf != 'undefined')) 
		{ 
			me.GlobalConf = JSON.parse(GlobalConf);			
		}
		else 
		{ 
			me.GlobalConf["all"] = true; me.GlobalConf["not"] = false; 			
		}		
	}			

/****************модальное окно*******************/
	
	
	Process.prototype.CloseModal = function()
	{		
		$('#PopupWindow')
			.animate({opacity: 0, top: '30px'}, 200,
				function(){
					$('#BP')[0].reset();
					$(this).css('display', 'none');
					$('#overlay').fadeOut(400);
				}
		);	
	}
	
		
	Process.prototype.ShowPopup = function(code_iblock)
	{			
		let popupTop = parseInt($(window).height() / 2) - 200;
		$('#overlay').fadeIn(400);			
		$('#PopupWindow').css('display', 'block').animate({opacity: 1, top: popupTop + 'px'}, 200);			
		return false;
	}		
	

/******************запуск процесса*****************/	
	
	
	Process.prototype.StartProcess = function(id_process)
	{	
		let me = this;			
				
		let element_code = 'process_'+id_process+'_'+Math.floor(Date.now());					
					
		let FindMe = $('#'+me.SetProperty).find('[data-send="'+id_process+'"]');
					
		let fields = new Object;	

		let canLaunch = true;
					
		$.each(FindMe, function(code, value) 
		{				
			let alias = $(value).attr('name');			
			fields[alias] = $(value).val();
			
			if (($(value).attr('required') == "required") && ($(value).val() == ""))
			{ 		
				$(value).parent().addClass("Error");	
				canLaunch = false;			
			}
		});	

		if(canLaunch) {
			var params = {
				'IBLOCK_TYPE_ID': 'bitrix_processes',
				'IBLOCK_ID': id_process,
				'ELEMENT_CODE': element_code,						
				'FIELDS': fields
			};				
						
			BX24.callMethod(
				'lists.element.add',
				params,
				function(result)
				{
					let message = '';
				
					if (result.data() != undefined) {
						message = 'Процесс запущен';					
					}
					else if(result.error()) {
						message = 'Не удалось запустить процесс!';
						console.error(result.error());
					}

					$("#"+me.idMessage).html(message);

					me.ShowPopup();
				}
			);	
		}					
		
		return false;
	}
	

	
	/*************скрытие панели************/	
	
	Process.prototype.ClosePanel = function()
	{
		let me = this;		
		$('.SidePanel').animate({ right: '-'+($('.SidePanel').width()-38)+'px' });
		$('#overlay').fadeOut(400);
	}
	
/***********выдвижная панель***********/	
		
	Process.prototype.ShowPanel = function()
	{			
		let me = this;		
		
		/************************************************************/		
		
		if ( me.GlobalConf["all"] == true)	{
			$("#all").prop("checked", true); $("#not").prop("checked", false);			
		}
		
		if ( me.GlobalConf["not"] == true) {
			$("#not").prop("checked", true); $("#all").prop("checked", false);
		}	
		
		/************************************************************/
		
		// вывод чекбоксов		
		
		let str = '<div class="col-lg-12 col-md-12 col-sm-12 SwitchBox">';
		
		$.each(me.obAllProcess, function(id, item) 
		{	
			let checked = '';
			if (($.isEmptyObject(me.Configuration) || (me.Configuration == undefined) || (me.Configuration[id] == true)) 
				&& (me.GlobalConf["not"] == false)) checked = "checked";
		
			str += '<div class="SwitchBox__Block"><span class="SwitchBox__Content"><span class="SwitchBox__Left"><input '+checked+' type="checkbox" id="'+id+'" class="SwitchBox__Checkbox"></span><span class="SwitchBox__Right"><span class="SwitchBox__Title">'+item.NAME+'</span></span></span></div>';
		});
		
		str += '</div>';
				
		str += '<div class="col-lg-12 col-md-12 col-sm-12 PopupWindow__Buttons"><span class="PopupWindow__Button PopupWindow__Button_Accept" onClick="BusinessProcess.Save()">Сохранить</span><span class="PopupWindow__Button PopupWindow__Button_Link PopupWindow__Button_Link_Cancel" onclick="return BusinessProcess.ClosePanel();">Отменить</span></div>';				
		
		$('#'+me.idOption).html(str);		
		
		
		/*******************ОРГАНИЗАЦИОННЫЕ МОМЕНТЫ******************/
		
		
		// анимация выдвижной панели в зависимости от ее текущего положения
		// также регулирование высоты
		
		
		
		if ($('.SidePanel').css('right').replace('px', '') < 0) 
		{	
			$('#overlay').fadeIn(400);				
			$('.SidePanel').animate({ right: "0%" });

			//$('.SidePanel').css({'right' : "0%"});

			// console.log($('.SidePanel').css('right'));
		}
		else { 
		     me.ClosePanel(); 
		}
		
		// получить текущие размеры окна
		
		OnlineSize = BX24.getScrollSize();		
		
		if (OnlineSize.scrollHeight < ($('#SwitchBox').outerHeight(true) + $('#GlobalSettings').outerHeight(true)))
			BX24.resizeWindow($(window).width(), ($('#SwitchBox').outerHeight(true) + $('#GlobalSettings').outerHeight(true)));			
			
		// обработка нажатия на ...		
		$('.SwitchBox__Block').click(function() 
		{	//  блок с процессом
			me.Сhecked($(this).find('input[type="checkbox"]'));
		});
		
		$('.SwitchBox__Block input[type="checkbox"]').click(function() 
		{	//  чекбокс с процессом
			me.Сhecked($(this));
		});	
		
		return false;
	}	
	
// нажатие на кнопку "Сохранить" в выдвижной панели
	
	Process.prototype.Save = function()
	{
		let me = this;
		
		/*********КОНФИГУРАЦИЯ ПАРАМЕТРОВ**********/
			
		let Configuration = new Object; // сбор конфигурации ПАРАМЕТРОВ на сохранение				
		let FindProcess = $('#SwitchBox').find('input[type="checkbox"]:checked');			
		$.each(FindProcess, function(alias, elem) 
		{			
			Configuration[$(elem).attr("id")] = $(elem).prop("checked");				
		});					
		Configuration = JSON.stringify(Configuration);				
		BX24.userOption.set("CHECKBOX", Configuration);
		//BX24.userOption.set("CHECKBOX");
		delete Configuration;
			
		/*********СБОР КОНФИГУРАЦИИ ГЛОБАЛЬНЫХ НАСТРОЕК**********/ 
			
		let GlobalConf = new Object;
		let FindAction = $('#GlobalSettings').find('input[type="checkbox"]');	
		$.each(FindAction, function(alias, elem) 
		{			
			GlobalConf[$(elem).attr("id")] = $(elem).prop("checked");				
		});					
		GlobalConf = JSON.stringify(GlobalConf);				
		BX24.userOption.set("GLOBAL_CHECKBOX", GlobalConf);
		//BX24.userOption.set("GLOBAL_CHECKBOX");		
		delete GlobalConf;
			
		/*********ЗАПИСЬ ФОРМАТА ДАТЫ**********/
		if ( $("#pril_format_date").val() !=0 ) {
			conf_format_date = JSON.stringify($("#pril_format_date").val());				
			BX24.userOption.set("DATE_FORMAT", conf_format_date);
			//BX24.userOption.set("DATE_FORMAT");
			delete conf_format_date;
		}
		
		// текущий формат даты
		let conf_date_format = me.ConfDateFormat();	
			
		if ($('.bx-lists-input-calendar').length != 0)
		{							
			$('.bx-lists-input-calendar').datepicker({
				dateFormat: conf_date_format,
				autoClose: true
			});
		}		

		// меняем список в селекте согласно конфигурации			
		me.PrintContent($('#'+me.ListProcess).val());			
					
		// убираем выдвижную панель с экрана
		me.ClosePanel();			
	}
	
	
// нажатичек чекбокса	
	Process.prototype.Сhecked = function(e)
	{	
		let me = this;		
		
		if ( $(e).prop("checked") == false )
		{ $(e).prop('checked',true); }
		else { $(e).prop('checked',false);}	
		
		if (
				(($(e).attr("id") == "all") && ($(e).prop("checked") == true))
				||
				(($(e).attr("id") == "not") && ($(e).prop("checked") == false))
		   ) 
		{			
			$('#'+me.idOption+' input[type="checkbox"]').prop('checked',true);
			$("#GlobalSettings input[type='checkbox']#not").prop('checked',false);
			$("#GlobalSettings input[type='checkbox']#all").prop('checked',true);
		} 
		else if (   
					(($(e).attr("id") == "not") && ($(e).prop("checked") == true))  
					||
					(($(e).attr("id") == "all") && ($(e).prop("checked") == false))
				) 
		{			
			$('#'+me.idOption+' input[type="checkbox"]').prop('checked',false);
			$("#GlobalSettings input[type='checkbox']#not").prop('checked',true);
			$("#GlobalSettings input[type='checkbox']#all").prop('checked',false);
		}
		
		// если активны все чекбоксы, установить активным "Выбрать все"		
		// если неактивны все чекбоксы, установить активным "Убрать все"
		
			CountChechboxFalse = 0; // количество чекбоксов с false			
			$.each($('#'+me.idOption+' input[type="checkbox"]'), function(id, item) 
			{			
				if ($(item).prop("checked") == false) { ++CountChechboxFalse; }		
			});	
			
			// выбрать все
			if (CountChechboxFalse != 0) { $("#GlobalSettings input[type='checkbox']#all").prop('checked',false); }
			else { $("#GlobalSettings input[type='checkbox']#all").prop('checked',true); }
			
			// убрать все
			if (CountChechboxFalse == $('#'+me.idOption+' input[type="checkbox"]').length) 
			{ 
				$("#GlobalSettings input[type='checkbox']#not").prop('checked',true); 
			}	
			else if ((CountChechboxFalse < $('#'+me.idOption+' input[type="checkbox"]').length) && (CountChechboxFalse > 0)) 
			{ 
				$("#GlobalSettings input[type='checkbox']#not").prop('checked',false); 
			}
		
	}
	
// подключение скрипта
	Process.prototype.IncludeScript = function(url)
	{	
		let script = document.createElement('script');
		script.src = url;
		document.getElementsByTagName('head')[0].appendChild(script);
	}


/*************БАТЧ-ЗАПРОСЫ НА СПИСОК ВСЕХ ПРОЦЕССОВ************/
	
	
	// получение объектов со всех страниц сразу
	Process.prototype.getAllPage = function(params, total, method)
	{
		let me = this;
		
		let count = 0, b_count = 0, limit = 40, items_per_page = 50, batchs = {};
		let pages = Math.ceil(total/items_per_page);
								
		for(let index = 0; index < pages; index ++)
		{
			if (count >= limit) {
				count = 0;
				b_count++;
			}

			if(!batchs[b_count]) {
				batchs[b_count] = {};
			}
									
			batchs[b_count][index] = {
				'method': method, 
				'params': Object.assign({'start': items_per_page * index}, params)
			};

			count++;
		}	

		if(Object.keys(batchs).length) 
		{																		
			me.bacthHandler(
				batchs,
				function(id, error) {
					console.error("ERROR: " + error);
				},
				function(id, data) {},
				function(items) {
					me.FormationObjectProcess(items);
					//resolve();
				}
			);
		}
	}
	
	
	Process.prototype.bacthHandler = function(batchs, funcError, funcSuccess, funcEnd) 
	{
		let me = this;
		let ob, promises;
		ob = this; promises = [];
		
		promises = me.getBatchPromises(promises, batchs);
		
		Promise.all(promises).then(result => {
			let resBatch, data;
			resBatch = {}; data = {};
			
			result.forEach(function(value) {
				$.extend(resBatch, value);
			});

			for(let id in resBatch) {
				let res;
				res = resBatch[id];
				
				if(res.error()) {
					funcError(id, res.error(), res);
				}
				else {
					data[id] = res.data();
					funcSuccess(id, data[id], res);
				}
			}
			
			// преобразование массива в удобный формат
			
			let newData = new Object;

				$.each(data, function(id, mass) {
				$.each(mass, function(code, elem) 
				{				
					newData[elem.ID] = elem;
				});
			});										
			
			funcEnd(newData);			
		});
	}
	

//
	Process.prototype.getBatchPromises = function(promises, batchs)
	{		
		let ob;
		ob = this;

		function delayExec(num, resolve) {
			setTimeout(
				function() {
					BX24.callBatch(batchs[num], function (result) 
					{						
						BX24.refreshAuth();
						resolve(result);
					});
				},
				(Math.floor(parseInt(num) / 1)) * 7500
			);
		}				
			
		if(Object.keys(batchs).length) 
		{
			for (let n in batchs) {
				let pr = new Promise((resolve, reject) => {						
					delayExec(n, resolve);
				});
				
				promises.push(pr);
			}			
		}				
		
		return promises;
	}
	
	Process.prototype.Select2 = function(NameClass)
	{			
		if (NameClass == "InicialSelect2Date")
		{
			$('.'+NameClass).select2({minimumResultsForSearch: Infinity, width : "100%"}); // селект даты

			$(".SwitchBox__Block_Setting .select2-container").css("height", "27px");
			$(".SwitchBox__Block_Setting .select2-container").css("line-height", "27px");
			$(".SwitchBox__Block_Setting .select2-selection").css("height", "27px");
			$(".SwitchBox__Block_Setting .select2-selection").css("line-height", "27px");
			$(".SwitchBox__Block_Setting .select2-selection__rendered").css("line-height", "27px");
			$(".SwitchBox__Block_Setting .select2-selection__arrow").css("height", "27px");
		}
		else
		{
			let width;	
			if ($(window).width() <= 998) { width = "100%"; } else { width = "75%"; }
			
			$('.'+NameClass).select2({minimumResultsForSearch: Infinity, width : width}); // селекты контетной части
			
			$(".ParamsSelect2Date .select2-container").css("height", "38px");
			$(".ParamsSelect2Date .select2-container").css("line-height", "38px");
			$(".ParamsSelect2Date .select2-selection").css("height", "38px");
			$(".ParamsSelect2Date .select2-selection").css("line-height", "38px");	
			$(".ParamsSelect2Date .select2-selection__rendered").css("line-height", "38px");
			$(".ParamsSelect2Date .select2-selection__arrow").css("height", "38px");			
		}		
		
		$(".select2-container").css("vertical-align", "middle");
		$(".select2-container").css("border-radius", "2px");
		$(".select2-container").css("border-color", "#a1a6ac");		
		$(".select2-selection").css("vertical-align", "middle");
		$(".select2-selection").css("border-radius", "2px");
		$(".select2-selection__arrow").css("background", "transparent url('img/arrow.png') no-repeat 50% 50%");
		$(".select2-selection__arrow").css("width", "36px");
		$(".select2-selection__arrow").children().remove();		
	}	
	
});