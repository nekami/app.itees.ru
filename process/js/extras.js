function showExtras(applicationID, containerID)
{
	var b24_apps = {
		"itees.phone": "Отчеты по звонкам",		
		"itees.callsdetail": "Детализация звонков",		
		"itees.salescript": "Звоните по скриптам!",		
		"itees.callplan": "Планирование звонков",
		"itees.appointments": "Запись на прием",
		"itees.crmreport": "Сводный отчет CRM",
		"itees.processes": "Процессы в телефоне"
	};	
	var auth = BX24.getAuth();
	
	var domain = auth["domain"];
	
	var extrasButton = $("<div class='extras_wrap'>" +
		"<div class='feat-red extras' id='extras' style='margin:0'>" +
			"<span class='extras_arrow_bottom'></span><span class='extras_btn_text'>Больше возможностей!</span>" +
		"</div>" +
		"<ul class='extras_menu' id='extras_menu'></ul>" +
	"</div>");
	
	var extrasMenu = extrasButton.find("#extras_menu");
	
	if(applicationID && $.inArray(applicationID, Object.keys(b24_apps)))
	{
		var ideaLink = "mailto:sup@itees.ru?subject=Idea for application ";
		var problemLink = "mailto:sup@itees.ru?subject=Error in application ";
		
		ideaLink += '"' + applicationID + '"';
		problemLink += '"' + applicationID + '"';
		
		extrasMenu.append($("<li class='extras_section1_wrap'>" +
			"<ul class='extras_section1'>" +
				"<li class='extras_menu_item js-extras_menu_item'><a href='" + ideaLink + "'>Предложите нам идею!</a></li>" +
				"<li class='extras_menu_item js-extras_menu_item'><a href='" + problemLink + "'>Сообщите о проблеме приложения!</a></li>" +
			"</ul>" +
		"</li>"));
	}
	
	extrasMenu.append($("<li class='extras_section2_wrap'>" +
		"<ul class='extras_section2'>" +
			"<li class='extras_menu_more'>Попробуйте другие возможности:</li>" + "</ul>" + "</li>"));
		
	var appList = "<li class='extras_section2_wrap'>" +
		"<ul class='extras_section2'>";
		for(var app in b24_apps)
		{
			appList += "<li class='extras_menu_item js-extras_menu_item' data-app='" + app + "'><a target='_blank' href='https://" + domain + "/marketplace/detail/" + app + "/'>" + b24_apps[app] + "</a></li>";
		}
		appList += "</ul>" +
	"</li>";
	
	extrasMenu.append(appList);
	
	if(applicationID && $.inArray(applicationID, Object.keys(b24_apps)))
	{
		extrasMenu.find(".js-extras_menu_item[data-app='" + applicationID + "']").remove();
	}
	
	$("body").on("click", function(e)
	{
		if(!e["target"]["className"].match(/extras/))
		{
			$("#extras_menu").fadeOut(200);
		}
	});
	
	$("#" + containerID).on("click", "#extras", function()
	{
		$("#extras_menu").fadeToggle(200);
	});
	
	$("#" + containerID).append(extrasButton);
}