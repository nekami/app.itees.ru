<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Запуск бизнес-процессов</title>			
		<link href="css/bootstrap.min.css?<?=rand();?>" rel="stylesheet" />
		<link href="css/select2.min.css?<?=rand();?>" rel="stylesheet" />		
		<link href="css/style.css?<?=rand();?>" rel="stylesheet" />	
		<script src="js/jquery-3.1.0.min.js?<?=rand();?>"></script>			
	</head>
	
	<body class='app_wrap transparent' id="app_wrap" onload="$('body').height(screen.height); LoadAnimation();">
	
		<div class="container-fluid">
			<form id="BP">
		
			<!--для корректной работы скрипта даты-->	
			<input type="hidden" class="bx-lists-input-calendar datepicker-here" data-position="bottom center" />
			
			
			<div class="row">
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Settings">
				 
					 <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12" id="mainContent"></div>			 
					 
					 <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">					 
						 <div class="TopMenu" id="top_menu_wrap">						
							<!--кнопка фейсбука-->
								<a href="https://www.facebook.com/iteessolutions/" target="_blank" class="TopMenu__Facebook"></a>
							<!--техническая поддержка-->
								<div class="TopMenu__Support" id="open_line_btn" title="Техподдержка"></div>
						 </div>					 
					 </div>				 
					
				</div>
			</div>
			
			
			<div class="row">		
				 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					  <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12" id="SetProperty"></div>                        
				</div>
			</div>	
			
			
			<div class="SidePanel hidden-xs">
				<button id="ShowPanel" class="Settings__Button Settings__Button_Icon_Setting"></button>
				
				<div class="SidePanel__Content">
				
					<div id="GlobalSettings" class="col-lg-12 col-md-12 col-sm-12">					
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="SwitchBox__Block SwitchBox__Block_Setting">
								<span class="SwitchBox__Content SwitchBox__Content_Setting">
									<span class="SwitchBox__Left">
										<input type="checkbox" name="all" id="all" value="" class="SwitchBox__Checkbox">
									</span><span class="SwitchBox__Right"><span class="SwitchBox__Title">Выбрать все</span></span>
								</span>
							</div>
							
							<div class="SwitchBox__Block SwitchBox__Block_Setting">
								<span class="SwitchBox__Content SwitchBox__Content_Setting">
									<span class="SwitchBox__Left">
										<input type="checkbox" name="not" id="not" value="" class="SwitchBox__Checkbox">
									</span><span class="SwitchBox__Right"><span class="SwitchBox__Title">Убрать все</span></span>
								</span>
							</div>	
							
							<div class="SwitchBox__Block SwitchBox__Block_Setting">
								<span class="TableProcess__Select TableProcess__Select_Setting" style="width: 100%;">
									<select class="InicialSelect2Date" id="pril_format_date" style="height: 27px">
										<option value="">--формат даты--</option> 
										<option value="dd.mm.yyyy">ДД.ММ.ГГГГ</option>
										<option value="mm/dd/yyyy">ММ/ДД/ГГГГ</option>										
									</select>
								</span>
							</div>
						</div>
					</div>	
					<div id="SwitchBox" class="col-lg-12 col-md-12 col-sm-12"></div>					
				</div>
				
			</div>
			</form>
		</div>
		
		
		<div class="PopupWindow PopupWindowWithTitlebar col-lg-offset-4 col-md-offset-3 col-xs-offset-1 col-lg-4 col-md-6 col-xs-10" id="PopupWindow">
			<div class="PopupWindow__Title"><span class="PopupWindow__Title__Text"></span></div>
			
			<div class="PopupWindow__Content" id="message"></div>
			
			<span id="CloseModalIcon" class="PopupWindow__Icon PopupWindow__Icon_Close" style="top: 10px; right: 15px;"></span>
			<div class="PopupWindow__Buttons">				
				<span class="PopupWindow__Button PopupWindow__Button_Accept" id="CloseModalOk">OK</span>
			</div>
		</div>
		
		
		<!--подложка одна для всех-->
		<div id="overlay" class="PopupWindowOverlay"></div>
		
		
				
		<script src="js/b24_rest_api.js?<?=rand();?>"></script>
		<!--календарь-->
		<link href="datepicker/css/datepicker.css?<?=rand();?>" rel="stylesheet" type="text/css">
		<script src="datepicker/js/datepicker.min.js"></script>	
		<!--маска ввода полей-->
		<script src="js/jquery.inputmask.bundle.min.js"></script>
		<!--<script src="js/inputmask.binding.min.js"></script>-->
		<!--кнопка "Больше возможностей"-->
		<link href="css/extras.css?<?=rand();?>" rel="stylesheet" />
		<script src="js/extras.js?<?=rand();?>"></script>		
		<!--плагин селекта-->			
		<script src="js/select2.full.min.js?<?=rand();?>"></script>			
		<!--логика работы процессов-->
		<script src="js/script.js?<?=rand();?>"></script>		

		<script>
			<!--вычисления, не относящиеся к логике процессов-->
					
			
			// техническая поддержка
			BX24.init(function()
			{	
				var placementInfo = BX24.placement.info();
				var placementOptions = placementInfo['options'];
				var appAction = placementOptions['action'];
				var appPage = placementOptions['page'];
				var appWrap = $('#app_wrap');

				if(appAction == 'popup') {
					var urls = {
						open_line: 'https://cp.itees.ru/online/salescriptpro', //открытая линия
					}

					var frameWrap = $('<div id="frame_wrap"></div>');
					appWrap.removeClass('transparent');
					appWrap.html(frameWrap);
					appWrap.addClass('no_font_size');

					$(document).ready(function() {
						if(urls[appPage]) {
							frameWrap.append('<iframe src="' + urls[appPage] + '" class="transparent" frameborder="0" onload="resizeIframe(this); $(this).removeClass(\'transparent\');">');
						}
					});

					return;
				}

				appWrap.removeClass('transparent');

				// кнопка возможностей
				showExtras("itees.processes", "top_menu_wrap");
				
				////////////////////////////////////////////////////////////////////////
		
				var appDependencies = [
					"js/b24cpc_defines.js",
				];
				var d_arr = [];
				
				for(var file in appDependencies)
				{
					d_arr.push($.Deferred());
				}
				
				$.when.apply($, d_arr).done(function ()
				{
					b24cpc_sendAppInfo__trialReset();
				});
				
				$.each(appDependencies, function(i, file) 
				{
					$.getScript(file, function()
					{
						d_arr[i].resolve();
					});
				});				
				
				////////////////////////////////////////////////////////////////////////
			});	
					

			// установка размеров фрейма
			function resizeIframe(obj) {
				obj.style.height = window.innerHeight + 'px';
				obj.style.width = '100%';
			}

			// прелоадер			
			
			function LoadAnimation()
			{
				str = '<div id="loading_ind" class="LoadingAnimation"><div class="LoadingAnimation__Container"><svg class="LoadingAnimation__Circular" viewBox="25 25 50 50"><circle class="LoadingAnimation__Track" cx="50" cy="50" r="20" fill="none" stroke-miterlimit="10"></circle></svg></div></div> ';
				
				$('body').append(str);
				
				animateRotateLoad(360, 'loading_ind');	
				
				
				//let online = event.target;
				
				//console.log($(online).find("select"));
			}
			
			function animateRotateLoad(d, idLoad) {
				$('#'+idLoad+' .LoadingAnimation__Circular').animate({deg: d}, {
					step: function(now, fx){
						$('#'+idLoad+' .LoadingAnimation__Circular').css({
							transform: "rotate(" + now + "deg)"
						});
					},
					duration: 1000,
					complete : function() {
						$('#'+idLoad+' .LoadingAnimation__Circular').stop();
						animateRotateLoad(d+360, idLoad);
					}
				});
			}
					
		</script>		
		
	</body>	
</html>