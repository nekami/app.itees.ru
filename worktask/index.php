<?
 function generateString($length = 8){
    $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
    $numChars = strlen($chars);
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= substr($chars, rand(1, $numChars) - 1, 1);
    }
    return $string;
}

$str = generateString();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">

		<title>Табелирование рабочего дня</title>

		<!--JS SDK REST-->
		<script src="//api.bitrix24.com/api/v1/?t=<?=$str?>"></script>
		<!-- Vue.js -->
		<script src="//cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <!-- components.js -->
        <script src="js/components.js?t=<?=$str?>"></script>
        <!-- jQuery -->
        <!--<script src="https://code.jquery.com/jquery-3.3.1.js?t=<?=$str?>" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>-->
		<script src="js/jquery-3.1.0.min.js?t=<?=$str?>"></script>
        <!-- underscore.js -->
        <script type="text/javascript" src="js/underscore.js?t=<?=$str?>"></script>

        <!-- Классы -->
        <script type="text/javascript" src="classes/Repository.js?t=<?=$str?>"></script>
        <script type="text/javascript" src="classes/Application.js?t=<?=$str?>"></script>
        <script type="text/javascript" src="classes/Employee.js?t=<?=$str?>"></script>
        <script type="text/javascript" src="classes/TaskService.js?t=<?=$str?>"></script>
        <script type="text/javascript" src="classes/Release.js?t=<?=$str?>"></script>
        <script type="text/javascript" src="classes/B24app.js?t=<?=$str?>"></script>
        <script type="text/javascript" src="classes/JsonService.js?t=<?=$str?>"></script>

        <!-- reset.css -->
        <link rel='stylesheet' href="css/reset.css"/>
		<!-- Bootstrap CSS -->
		<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"/> -->
        <!-- components.css -->
        <link rel='stylesheet' href="css/components.css?t=<?=$str?>"/>

        <!-- Пользовательские стили -->
        <link rel="stylesheet" href="css/style.css?t=<?=$str?>">
    </head>
	<body>
        <div class="Row" id="app">	
		
            <div id="taskView" class="Row taskView">
			
                <div class="Row">
                    <div class="Row taskView__Title">{{task['TITLE']}}</div>
                    
					<div class="Row taskView__BlockSelect">
					
						<div class="taskView__BlockSelect__Col">
							<label>Постановщик</label>							
							<span class="taskView__SelectContainer">						
								<select id="created_by">
									<option v-for="(user, id) in users" :value="id">{{obEmployee.getFullName(id)}}</option>
								</select>
							</span>						
						</div>
						
						<div class="taskView__BlockSelect__Col">
							<label>Ответственный</label>														
							<span class="taskView__SelectContainer">						
								<select id="responsible_id">
									<option v-for="(user, id) in users" :value="id">{{obEmployee.getFullName(id)}}</option>
								</select>
							</span>						
						</div>
						
						<div class="taskView__BlockSelect__Col">
							<label>Соисполнители</label>							
							<span class="taskView__SelectContainer">						
								<select id="accomplices">
									<option v-for="(user, id) in users" :value="id">{{obEmployee.getFullName(id)}}</option>
								</select>
							</span>
						</div>
						
						<div class="taskView__BlockSelect__Col">
							<label>Наблюдатели</label>
							<span class="taskView__SelectContainer">						
								<select id="auditors">
									<option v-for="(user, id) in users" :value="id">{{obEmployee.getFullName(id)}}</option>
								</select>
							</span>	
						</div>
						
						<div class="taskView__BlockSelect__Col">
							<label>Приложение</label>							
							<span class="taskView__SelectContainer">						
								<select id="group_id">
									<option v-for="(name, code) in b24apps" :value="code">{{name}}</option>
								</select>
							</span>
						</div>
						
					</div>
					
                </div>				
				
                <div class="Row taskView__Bottom">
                    
					<div v-for="(item, id) in checklist" class="Row">
                        <div class="taskView__CheskList">
							<div class="taskView__CheskList__Title">{{item['TITLE']}}</div>
							<div class="taskView__CheskList__Content">
							
								<div class="taskView__Bottom__Col" @click="obTaskService.addSubtask(task['ID'], item);">
									<span class="taskView__Bottom__Button">
										Сделать подзадачей
									</span>
								</div>
								
								<div class="taskView__Bottom__Col" @click="currentRelease.add(item)">								
									<span class="taskView__Bottom__Button">
										В текущий релиз
									</span>							
								</div>
								
								<div class="taskView__Bottom__Col" @click="currentRelease.addBug(item)">
									<span class="taskView__Bottom__Button">
										В текущий релиз (баги)
									</span>	
								</div>
								
								<div class="taskView__Bottom__Col" @click="nextRelease.add(item)">
									<span class="taskView__Bottom__Button">
										В следующий релиз
									</span>							
								</div>
								
								<div class="taskView__Bottom__Col" @click="nextRelease.addBug(item)">
									<span class="taskView__Bottom__Button">
										В следующий релиз (баги)
									</span>	
								</div>
							
							</div>
						</div>
                    </div>
					
                </div>				
            </div>			
        </div>
	</body>
</html>
<script type="text/javascript">
    var vm = false
		
	$(function() {		
        BX24.init(function(){
            var taskId = 1118;
            var options = {
                presumablyPages: 5, //редполагаемое количество страниц
                itemsPerPage: 50,   //количество элементов на странице
            }

			var obRepository = new Repository();
            var obApp = new Application(obRepository);   //класс для работы с приложением
            var obEmployee = new Employee(obRepository, options);
			
			//console.log(obRepository, options);
			
            var obTaskService = new TaskService(obRepository, options);
			
			//console.log(obTaskService.getChecklist(taskId));

            var dUsers = $.Deferred()
            var dTask = $.Deferred()
            var dChecklist = $.Deferred()

            //(1)необходимые данные подгружаются в (2)
            $.when(dUsers, dTask).done(() => {
				
				//console.log(obTaskService.getChecklist(taskId));				
				
                vm = new Vue({
                    data() {
                        return {
                            obApp: obApp,
                            obEmployee: obEmployee,
                            obTaskService: obTaskService,
                            users: obEmployee.getAll(),
                            task: obTaskService.get(taskId),
                            checklist: obTaskService.getChecklist(taskId),
                            currentRelease: false,
                            nextRelease: false,
                            b24appSelected: 'itees.salescriptpro',
                            b24apps: {
                                'itees.salescriptpro': 'SaleScript PRO',
                                'itees.appointmentspro': 'Запись на прием Pro',
                                'itees.phone': 'Отчеты по звонкам',
                                'itees.pomidor': 'Помидор',
                            },
                            b24app: false
                        };
                    },
                    computed: {},
                    methods: {
                        loadB24app(appCode, callback) {
                            let obB24app = new B24app(obRepository, obTaskService, appCode)
                            obB24app.load(callback);
                        },
                        app24Change(el) {
                            let code = 'test';
                            this.loadB24app(code, function (res) {
                                //заполняем this.b24app this.currentRelease, this.nextRelease
                            })
                        }
                    },
					created() {
						//<-----------------
						//Заполнить this.currentRelease, this.nextRelease

                        this.loadB24app(this.b24appSelected, function (res) {
                            //заполняем this.b24app this.currentRelease, this.nextRelease
                        })
                    },
                    mounted() {
                        //убираем крутилку, показываем интерфейс
                        $('#taskView').addClass('visible')
                        $('#loading').removeClass('visible')
                    }
                })

                vm.$mount('#app')

                BX24.fitWindow()
            })

            //(2)подгружаем необходимые данные. Эти данные используем в (1)
            obEmployee.loadAll(dUsers);
            obTaskService.load(dTask, taskId);
            obTaskService.loadChecklist(dChecklist, taskId);
        })
    })
</script>