var iteesComponents = {
    'itees-select': {
        props: ['options', 'optionsinput', 'value'],
		data: function () {
			return {
				isOpen: false,
				indexSelected: null, 
				isSelected: false,
				valueLocal: this.value,
				styleSelect: {
					'z-index' : 2
				},
				styleInput: {
					'z-index' : 1,
					'width' : '60px'
				}
			}
		},
		methods: {
			inputFocus: function (e) {
				if(this.isSelected){
					this.isSelected = false;
				}
				else if(!this.isOpen) {
					this.selectOpen();
				}
			},
			inputBlur: function (e) {
				if(!this.isSelected) {
					this.selectClose();
				}
			},
			inputKeyup : function (e) {
				var
				elInput = this.$refs['input'],
				elSelect = this.$refs['select'];
				
				switch(e.keyCode) {
					//enter, esc
					case 13: case 27:
						elInput.blur();
					break;
					
					//up, down
					case 38: case 40:
						var
						index = Object.keys(this.options).indexOf(this.valueLocal);
						flUp = e.keyCode === 38,
						lastIndex = Object.keys(this.options).length - 1,
						firstIndex = 0;
						
						if(index == -1) {
							index = flUp ? lastIndex : firstIndex;
						}
						else if(flUp) {
							index > 0 ? index-- : index = lastIndex;
						}
						else {
							index < lastIndex ? index++ : index = firstIndex
						}
						
						this.valueLocal = Object.keys(this.options)[index];
						this.$emit('change', this.valueLocal);
					break;
				};
				
				return false;
			},
			inputKeydown : function (e) {
				this.keyCode = e.keyCode;
			},
			selectMousedown: function(e) {
				this.isSelected = true;
			},
			selectChange: function (e) {
				this.isSelected = true;
				this.valueLocal = e.target.dataset.value;
				this.$emit('change', this.valueLocal);
			},
			selectOpen: function () {
				var
				elInput = this.$refs['input'],
				elSelect = this.$refs['select'],
				elSelectContainer = this.$refs['select_container'];
				
				if(device.mobile()) {
					elSelectContainer.style.top = '50%';
					elSelect.style.marginTop = '-50%';
					elSelectContainer.style.left = '50%';
					elSelect.style.left = '-50%';
				}
				else {
					elSelectContainer.style.top = 5 + elInput.getBoundingClientRect().top + elInput.clientHeight + 'px';
					elSelectContainer.style.left = elInput.getBoundingClientRect().left + 'px';
				}
				
				this.isOpen = true;
			},
			selectClose: function () {
				var
				elInput = this.$refs['input'],
				elSelect = this.$refs['select'];
				this.isOpen = false;
			}
		},
		template: '<label>' +
			'<span class="input input_type_select" :style="styleInput">\
				<input ref="input" readonly="readonly"\
				@keydown="inputKeydown($event)"\
				@keyup="inputKeyup($event)"\
				@focus="inputFocus($event)"\
				@blur="inputBlur($event)"\
				:value="optionsinput[valueLocal]"\
				class="input__select"\ type="text">\
			</span>' +
			
			'<div ref="select_container"\
			:style="styleSelect"\
			:class="[\'select\', \'select_absolute\', {\'select_hidden\' : !isOpen}]"><ul ref="select"\ class="select__container">' +
				'<li class="select__option" v-for="(val, code) in options" :data-value="code"\
				:class="{\'select__option\' : true, \'select__option_active\' : code == valueLocal}"\
				@mousedown="selectMousedown($event);"\
				@click="selectChange($event); selectClose();">\
					{{val}}\
				</li>' +
			'</ul></div>' +
		'</label>',
    }
}