<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <title>Табелирование рабочего дня</title>

    <!--JS SDK REST-->
    <script src="//api.bitrix24.com/api/v1/?t=<?=$str?>"></script>
    <!-- underscore.js -->
    <script type="text/javascript" src="js/underscore.js?t=<?=$str?>"></script>

    <!-- Классы -->
    <script type="text/javascript" src="classes/Repository.js?t=<?=$str?>"></script>
    <script type="text/javascript" src="classes/Application.js?t=<?=$str?>"></script>

    <!-- reset.css -->
    <link rel='stylesheet' href="css/reset.css"/>
</head>
<body>
<h1 class="install_info">Приложение устанавливается, подождите!<h1>
</body>
</html>
<script type="text/javascript">
    BX24.init(function() {
        var repository = new Repository()
        var app = new Application(repository)
        
        app.install();
    });
</script>