// класс для работы с приложением
class Application {
    constructor(repository) {
        this.repository = repository               //взаимодействие с БД
        this.placements = [                       //места встройки
            {
                'PLACEMENT': 'TASK_VIEW_MODE',
                'HANDLER': 'https://ol.ngknn.app.itees.ru/worktask/index.php',
                'TITLE': 'Работа с задачами',
                'DESCRIPTION': 'Работа с задачами',
            },
        ]
    }

    /**
     * Установка обработчиков мест встраивания.
     * @param repository
     * @param properties
     * @returns {Promise<any>}
     */
    installPlacements(repository, placements) {
		
		let that = this;		
		
        return new Promise((resolve) => {	

//<--------------------- данные для запроса находятся в this.placements
			let filter = {}; filter = {'PLACEMENT': 'TASK_VIEW_MODE', 'HANDLER': 'https://ol.ngknn.app.itees.ru/worktask/index.php', 'TITLE': 'Работа с задачами', 'DESCRIPTION': 'Работа с задачами'};		

//<--------------------- переписать через repository.batchHandler			
			that.GeneralbatchHandler(
				'Y',
				'PLACEMENT', 
				'placement.bind', 
				filter, 
				function(items) {

					console.log(items);
				
					resolve();
				}
			);
			
        });	
    }

    /**
     * Установка приложения
     */
    install() {
				
        this.installPlacements(this.repository, this.placements)
    		.then(BX24.installFinish);
	}
	
	
//<--------------------- убрать	
	GeneralbatchHandler(no_circle, name_batch, method, filter, callback)
	{
		Promise.all([]).then(() => {
            return new Promise((resolve) => {
				
                var batch = {};
				
				let finish = this.options.presumablyPages;				
				if (no_circle == "Y") finish = 1;

                for(var index = 0; index < finish; index ++) {
					
					filter['start'] = index * this.options.itemsPerPage;
					
                    batch[name_batch+'-'+ index] = {'method': method, 'params': filter};
                }	

                var batchs = {0: batch};
				var that = this

                getCallCardDataBase(batchs);

                function getCallCardDataBase(batchs) {
                    that.repository.batchHandler(
                        batchs,
                        function(id, error) {
                            console.error("ERROR: " + error);
                        },
                        function(id, data) {},
                        function(items) {
                            var totals = {};
                            var callCardData = items[0];

                            if(callCardData) {
								
                                $.each(callCardData, function(code, value) {
                                    if(value.error()) {
                                        console.log('Ошибка запроса: ' + value.error());
                                        return;
                                    } else {
                                        var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                        switch(batchCode[1])
                                        {
                                            case name_batch:
											
												callback(value.data());
												
												var total = value.answer.total;
												
												var presumablCount = that.options.presumablyPages * that.options.itemsPerPage

                                                if(total > presumablCount) {
                                                    totals[name_batch] = total;
                                                }

                                                break;
                                        }
                                    }
                                });

                                resolve(totals);
                            } else {
                                getCallCardDataBase(batchs);
                            }
                        }
                    );
                }
            });
        }).then((totals) => {			
			
            return new Promise((resolve, reject) => {
                if(Object.keys(totals).length) {
                    var count = 0, b_count = 0, limit = 40, batchs = {};

                    for(var code in totals) {
                        var pages = Math.ceil(totals[code]/this.options.itemsPerPage);

                        switch(code) {
                            case name_batch:
                                for(var index = this.options.presumablyPages; index < pages; index ++)
                                {
                                    if (count >= limit) {
                                        count = 0;
                                        b_count++;
                                    }

                                    if(!batchs[b_count]) {
                                        batchs[b_count] = {};
                                    }
									
									filter['start'] = index * this.options.itemsPerPage;

                                    batchs[b_count][name_batch+'-'+index] = {'method': method, 'params': filter};

                                    count++;
                                }
                                break;
                        }
                    }

                    if(Object.keys(batchs).length){
						var that = this
                        getCallCardDataTails(batchs);

                        function getCallCardDataTails(batchs)
                        {
                            that.repository.batchHandler(
                                batchs,
                                function(id, error) {
                                    console.error("ERROR: " + error);
                                },
                                function(id, data) {},
                                function(items) {
                                    $.each(items, function(i, callCardData) {
                                        $.each(callCardData, function(code, value) {
                                            if(value.error()) {
                                                alert('Ошибка запроса: ' + value.error());
                                                return
                                            } else {
                                                var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                                switch(batchCode[1]) {
                                                    case name_batch:                                                    
														
														callback(value.data());												

                                                    break;
                                                }
                                            }
                                        });

                                        delete batchs[i];
                                    });

                                    if(Object.keys(batchs).length) {
                                        getCallCardDataTails(batchs);
                                    } else {
                                        resolve();
                                    }
                                }
                            );
                        }
                    }
                } else {
                    resolve();
                }
            })
        })		
	}
	
	
}