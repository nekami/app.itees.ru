// класс для работы с сотрудниками
class Employee {
    constructor(repository, options) {
        this.currentId = 0        
        this.repository = repository;
        this.options = options;
		this.employees = {};
    }

    /**
     * Зажгружает информацию о пользователях
     * @param df
     */

    loadAll(df) {
        Promise.all([]).then(() => {
            return new Promise((resolve) => {
                var batch = {};

                for(var index = 0; index < this.options.presumablyPages; index ++) {
                    batch['USERS_GET-' + index] = {'method': 'user.get', 'params': {'start': index * this.options.itemsPerPage, "FILTER": {'ACTIVE': true}}};
                }

                var batchs = {0: batch};
				var that = this

                getCallCardDataBase(batchs);

                function getCallCardDataBase(batchs) {
                    that.repository.batchHandler(
                        batchs,
                        function(id, error) {
                            console.error("ERROR: " + error);
                        },
                        function(id, data) {},
                        function(items) {
                            var totals = {};
                            var callCardData = items[0];

                            if(callCardData) {
                                $.each(callCardData, function(code, value) {
                                    if(value.error()) {
                                        alert('Ошибка запроса: ' + value.error());
                                        return;
                                    } else {
                                        var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                        switch(batchCode[1])
                                        {
                                            case 'USERS_GET':
                                                var users = value.data();
                                                var total = value.answer.total;

                                                for(var user in users)
                                                {
                                                    that.employees[users[user]["ID"]] = users[user];
                                                }
												
												var presumablCount = that.options.presumablyPages * that.options.itemsPerPage

                                                if(total > presumablCount) {
                                                    totals['USERS_GET'] = total;
                                                }

                                                break;
                                        }
                                    }
                                });

                                resolve(totals);
                            } else {
                                getCallCardDataBase(batchs);
                            }
                        }
                    );
                }
            });
        }).then((totals) => {
            return new Promise((resolve, reject) => {
                if(Object.keys(totals).length) {
                    var count = 0, b_count = 0, limit = 40, batchs = {};

                    for(var code in totals) {
                        var pages = Math.ceil(totals[code]/this.options.itemsPerPage);

                        switch(code) {
                            case 'USERS_GET':
                                for(var index = this.options.presumablyPages; index < pages; index ++)
                                {
                                    if (count >= limit) {
                                        count = 0;
                                        b_count++;
                                    }

                                    if(!batchs[b_count]) {
                                        batchs[b_count] = {};
                                    }

                                    batchs[b_count]['USERS_GET-' + index] = {'method': 'user.get', 'params': {'start': index * this.options.itemsPerPage, "FILTER": {'ACTIVE': true}}};

                                    count++;
                                }
                                break;
                        }
                    }

                    if(Object.keys(batchs).length){
						var that = this
                        getCallCardDataTails(batchs);

                        function getCallCardDataTails(batchs)
                        {
                            that.repository.batchHandler(
                                batchs,
                                function(id, error) {
                                    console.error("ERROR: " + error);
                                },
                                function(id, data) {},
                                function(items) {
                                    $.each(items, function(i, callCardData) {										
										
                                        $.each(callCardData, function(code, value) {
                                            if(value.error()) {
                                                alert('Ошибка запроса: ' + value.error());
                                                return
                                            } else {
                                                var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                                switch(batchCode[1]) {
                                                    case 'USERS_GET':
                                                        var users = value.data();

                                                        for(var user in users) {
                                                            that.employees[users[user]["ID"]] = users[user];
                                                        }

                                                        break;
                                                }
                                            }
                                        });

                                        delete batchs[i];
                                    });

                                    if(Object.keys(batchs).length) {
                                        getCallCardDataTails(batchs);
                                    } else {
                                        resolve();
                                    }
                                }
                            );
                        }
                    }
                } else {
                    resolve();
                }
            })
        }).then(function(){
            df.resolve();
        });
    }	

    /**
     * Зажгружает ID текущего пользователя
     * @param df
     */
    loadCurrent(df) {
        Promise.all([]).then(() => {
            //<----------------- Реализовать через repository.batchHandler, user.current

            let currentId = 15

            this.currentId = currentId
        }).then(function(){
            df.resolve();
        });
    }

    /**
     * Возвращает сотрудников
     * @returns {*}
     */
    get(userId) {
        return (userId && this.employees[userId]) ? this.employees[userId] : false
    }

    /**
     * Возвращает сотрудников
     * @returns {*}
     */
    getAll() {
        return this.employees
    }

    /**
     * Возвращает ID текущего пользователя
     * @returns {boolean}
     */
    getCurrentEmployeeId() {
        return this.currentId ? this.currentId : false
    }

    /**
     * Возвращает ФИО сотрудника
     * @param employeeId
     * @returns {*}
     */
    getFullName(userId) {		
		
        if(!this.employees[userId]) {
            return false
        }

        let arName = []

        if(this.employees[userId]['LAST_NAME']) {
            arName.push(this.employees[userId]['LAST_NAME'])
        }

        if(this.employees[userId]['NAME']) {
            arName.push(this.employees[userId]['NAME'])
        }

        if(this.employees[userId]['SECOND_NAME']) {
            arName.push(this.employees[userId]['SECOND_NAME'])
        }

        return arName.join(' ')
    }
}