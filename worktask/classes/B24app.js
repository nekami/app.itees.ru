// класс для работы с компанией
class B24app {
    constructor(repository, taskService, appCode) {
        this.taskService = taskService;
        this.repository = repository;
        this.appCode = appCode;
        this.appID = 0;
        this.ideasId = 0;
        this.bugtrackerId = 0;
        this.releaseData = {};
    }

    /**
     * Получение релиза по ID
     * @param df
     */
    getRelease(id) {
        //<----------------- Реализовать
		
		// возвращает объекты релиза
    }

    /**
     * Получение следующего релиза
     * @param df
     */
    getLastRelease() {
        //<----------------- Реализовать
		
		// возвращает объекты релиза
    }

    /**
     * Получение текущего релиза
     * @param df
     */
    getCurRelease() {
        //<----------------- Реализовать
		
		// возвращает объекты релиза
    }

    /**
     * Загрузка приложения
     * @param df
     */
    load(callback) {
        //<----------------- Реализовать через repository.batchHandler, this.taskService.getList(filter, callback). Заполнить поля
    }
}