// сервис для работы с JSON
class JsonService {
    /**
	 * Безопасное декодирование JSON-строки
     * @param stringData
     * @param defaultValue
     * @param error
     * @returns {any}
     */
    parse(stringData, defaultValue, error) {
		var data = false;
		try {
			data = JSON.parse(stringData);
		} catch(e) {
			console.error('JSON parse error' + (error ? '(' + error + ')' : '') + ': ' + e.message);
			console.error('error detail: ' + e.name  + ':');
			console.error(e.stack);
			data = defaultValue;
		}
		
		return data;
	}
}