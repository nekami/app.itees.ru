// класс для работы с компанией
class Company {
    constructor(repository, options) {
        this.employees = {};
        this.departments = {};
        this.repository = repository;
        this.options = options;
    }

    /**
     * Загрузка пользователей
     * @param df
     */
    loadUsers(df) {
        Promise.all([]).then(() => {
            return new Promise((resolve) => {
                var batch = {};

                for(var index = 0; index < this.options.presumablyPages; index ++) {
                    batch['USERS_GET-' + index] = {'method': 'user.get', 'params': {'start': index * this.options.itemsPerPage, "FILTER": {'ACTIVE': true}}};
                }

                var batchs = {0: batch};
				var that = this

                getCallCardDataBase(batchs);

                function getCallCardDataBase(batchs) {
                    that.repository.batchHandler(
                        batchs,
                        function(id, error) {
                            console.error("ERROR: " + error);
                        },
                        function(id, data) {},
                        function(items) {
                            var totals = {};
                            var callCardData = items[0];

                            if(callCardData) {
                                $.each(callCardData, function(code, value) {
                                    if(value.error()) {
                                        alert('Ошибка запроса: ' + value.error());
                                        return;
                                    } else {
                                        var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                        switch(batchCode[1])
                                        {
                                            case 'USERS_GET':
                                                var users = value.data();
                                                var total = value.answer.total;

                                                for(var user in users)
                                                {
                                                    that.employees[users[user]["ID"]] = users[user];
                                                }
												
												var presumablCount = that.options.presumablyPages * that.options.itemsPerPage

                                                if(total > presumablCount) {
                                                    totals['USERS_GET'] = total;
                                                }

                                                break;
                                        }
                                    }
                                });

                                resolve(totals);
                            } else {
                                getCallCardDataBase(batchs);
                            }
                        }
                    );
                }
            });
        }).then((totals) => {
            return new Promise((resolve, reject) => {
                if(Object.keys(totals).length) {
                    var count = 0, b_count = 0, limit = 40, batchs = {};

                    for(var code in totals) {
                        var pages = Math.ceil(totals[code]/this.options.itemsPerPage);

                        switch(code) {
                            case 'USERS_GET':
                                for(var index = this.options.presumablyPages; index < pages; index ++)
                                {
                                    if (count >= limit) {
                                        count = 0;
                                        b_count++;
                                    }

                                    if(!batchs[b_count]) {
                                        batchs[b_count] = {};
                                    }

                                    batchs[b_count]['USERS_GET-' + index] = {'method': 'user.get', 'params': {'start': index * this.options.itemsPerPage, "FILTER": {'ACTIVE': true}}};

                                    count++;
                                }
                                break;
                        }
                    }

                    if(Object.keys(batchs).length){
						var that = this
                        getCallCardDataTails(batchs);

                        function getCallCardDataTails(batchs)
                        {
                            that.repository.batchHandler(
                                batchs,
                                function(id, error) {
                                    console.error("ERROR: " + error);
                                },
                                function(id, data) {},
                                function(items) {
                                    $.each(items, function(i, callCardData) {
                                        $.each(callCardData, function(code, value) {
                                            if(value.error()) {
                                                alert('Ошибка запроса: ' + value.error());
                                                return
                                            } else {
                                                var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                                switch(batchCode[1]) {
                                                    case 'USERS_GET':
                                                        var users = value.data();

                                                        for(var user in users) {
                                                            that.employees[users[user]["ID"]] = users[user];
                                                        }

                                                        break;
                                                }
                                            }
                                        });

                                        delete batchs[i];
                                    });

                                    if(Object.keys(batchs).length) {
                                        getCallCardDataTails(batchs);
                                    } else {
                                        resolve();
                                    }
                                }
                            );
                        }
                    }
                } else {
                    resolve();
                }
            })
        }).then(function(){
            df.resolve();
        });
    }

    /**
     * Загрузка подразделений
     * @param df
     */
    loadDepartments(df) {
        Promise.all([]).then(() => {
            return new Promise((resolve, reject) => {
                var batch = {};

                for(var index = 0; index < this.options.presumablyPages; index ++) {
                    batch['DEPARTMENTS_GET-' + index] = {'method': 'department.get', 'params': {'start': index * this.options.itemsPerPage}};
                }

                var batchs = {0: batch};
				var that = this

                getCallCardDataBase(batchs);

                function getCallCardDataBase(batchs) {
                    that.repository.batchHandler(
                        batchs,
                        function(id, error) {
                            console.error("ERROR: " + error);
                        },
                        function(id, data) {},
                        function(items) {
                            var totals = {};
                            var callCardData = items[0];

                            if(callCardData) {
                                $.each(callCardData, function(code, value) {
                                    if(value.error()) {
                                        alert('Ошибка запроса: ' + value.error());
                                        return;
                                    } else {
                                        var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                        switch(batchCode[1])
                                        {
                                            case 'DEPARTMENTS_GET':
                                                var departments = value.data();
                                                var total = value.answer.total;

                                                for(var user in departments)
                                                {
                                                    that.departments[departments[user]["ID"]] = departments[user];
                                                }
												
												var presumablCount = that.options.presumablyPages * that.options.itemsPerPage

                                                if(total > presumablCount) {
                                                    totals['DEPARTMENTS_GET'] = total;
                                                }

                                                break;
                                        }
                                    }
                                });

                                resolve(totals);
                            } else {
                                getCallCardDataBase(batchs);
                            }
                        }
                    );
                }
            });
        }).then((totals) => {
            return new Promise((resolve, reject) => {
                if(Object.keys(totals).length) {
                    var count = 0, b_count = 0, limit = 40, batchs = {};

                    for(var code in totals) {
                        var pages = Math.ceil(totals[code]/this.options.itemsPerPage);

                        switch(code) {
                            case 'DEPARTMENTS_GET':
                                for(var index = this.options.presumablyPages; index < pages; index ++)
                                {
                                    if (count >= limit) {
                                        count = 0;
                                        b_count++;
                                    }

                                    if(!batchs[b_count]) {
                                        batchs[b_count] = {};
                                    }

                                    batchs[b_count]['DEPARTMENTS_GET-' + index] = {'method': 'user.get', 'params': {'start': index * this.options.itemsPerPage, "FILTER": {'ACTIVE': true}}};

                                    count++;
                                }
                                break;
                        }
                    }

                    if(Object.keys(batchs).length){
						var that = this
                        getCallCardDataTails(batchs);

                        function getCallCardDataTails(batchs)
                        {
                            that.repository.batchHandler(
                                batchs,
                                function(id, error) {
                                    console.error("ERROR: " + error);
                                },
                                function(id, data) {},
                                function(items) {
                                    $.each(items, function(i, callCardData) {
                                        $.each(callCardData, function(code, value) {
                                            if(value.error()) {
                                                alert('Ошибка запроса: ' + value.error());
                                                return
                                            } else {
                                                var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                                switch(batchCode[1]) {
                                                    case 'DEPARTMENTS_GET':
                                                        var departments = value.data();

                                                        for(var user in departments) {
                                                            that.departments[departments[user]["ID"]] = departments[user];
                                                        }

                                                        break;
                                                }
                                            }
                                        });

                                        delete batchs[i];
                                    });

                                    if(Object.keys(batchs).length) {
                                        getCallCardDataTails(batchs);
                                    } else {
                                        resolve();
                                    }
                                }
                            );
                        }
                    }
                } else {
                    resolve();
                }
            })
        }).then(function(){
            df.resolve();
        });
    }

    /**
     * Возвращает ФИО сотрудника
     * @param employeeId
     * @returns {*}
     */
    getEmployeeFullName(employeeId) {
        if(!this.employees[employeeId]) {
            return false
        }

        let arName = []

        if(this.employees[employeeId]['LAST_NAME']) {
            arName.push(this.employees[employeeId]['LAST_NAME'])
        }

        if(this.employees[employeeId]['NAME']) {
            arName.push(this.employees[employeeId]['NAME'])
        }

        if(this.employees[employeeId]['SECOND_NAME']) {
            arName.push(this.employees[employeeId]['SECOND_NAME'])
        }

        return arName.join(' ')
    }

    /**
     * Возвращает должность сотрудника
     * @param employeeId
     * @returns {*}
     */
    getEmployeeWorkPosition(employeeId) {
        if(!this.employees[employeeId]) {
            return false
        }
        
        return this.employees[employeeId]['WORK_POSITION']
    }

    /**
     * Возвращает сотрудников
     * @returns {*}
     */
    getEmployees() {
        return this.employees
    }

    /**
     * Возвращает подразщделения
     * @returns {*}
     */
    getDepartments() {
        return this.departments
    }
}