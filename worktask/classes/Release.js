// класс для работы с компанией
class Release {
    constructor(repository, taskService, data) {
        this.taskService = taskService;
        this.repository = repository;
        this.taskId = data['TASK_ID'];
        this.errorTaskId = data['ERROR_TASK_ID'];
        this.version = data['VERSION'];
        this.mpVersion = data['MP_VERSION'];
    }

    /**
     * Добавление элемента в чек-лист
     * @param df
     */
    addItem(item) {
        //<----------------- Реализовать через repository.batchHandler, task.checklistitem.add для this.taskId
    }
	
	/**
     * Добавление элемента в чек-лист баг-трекера
     * @param df
     */
    addBug(item) {
        //<----------------- Реализовать через repository.batchHandler, task.checklistitem.add для this.errorTaskId
    }
}