// класс для для взаимодействия с БД
class Repository {
    /**
     * Выполнение batch-запросов
     * @param batchs
     * @param funcError - вызывается при ошибке
     * @param funcSuccess - вызывается при успехе
     * @param funcEnd - вызывается после выполнения всех batch-запросов
     */
    batchHandler(batchs, funcError, funcSuccess, funcEnd)
    {
        var ob, promises, additionalSecs;
        ob = this; promises = [], additionalSecs = 0;

        if(Object.keys(batchs).length == 1) {
            additionalSecs = 1;
        }

        promises = this._getBatchPromises(promises, batchs, additionalSecs);

        Promise.all(promises).then(result => {
            var resBatch, data;
            resBatch = {}; data = {};

            for(var i in result) {
                var value = result[i];

                var b_id = value.index;
                var b_res = value.result;

                if(b_res.status == 500)
                {
                    funcError(0, 500, false);
                    continue;
                }

                data[b_id] = {};

                for(var id in b_res) {
                    var res;
                    res = b_res[id];

                    if(res.error()) {
                        funcError(id, res.error(), res);
                    }
                    else {
                        funcSuccess(id, res.data(), res);
                    }

                    data[b_id][id] = res;
                }
            }

            funcEnd(data);
        });
    }

    /**
     * Формирует массив с промисами по batch-запросам
     * @param promises
     * @param batchs
     * @param secs
     * @returns {*}
     * @private
     */
    _getBatchPromises(promises, batchs, secs) {
        var ob;
        ob = this;

        function delayExec(num, resolve) {
            setTimeout(
                function() {
                    try{
                        BX24.callBatch(batchs[num], function (result) {
                            resolve({'index': num, 'result': result});
                        });
                    } catch(err) {
                        resolve(false);
                    }
                },
                (parseInt(num) + secs) * 1000
            );
        }

        if(Object.keys(batchs).length) {
            for (let num in batchs) {
                var pr = new Promise((resolve, reject) => {
                    setTimeout(
                        function() {
                            try{
                                BX24.callBatch(batchs[num], function (result) {
                                    resolve({'index': num, 'result': result});
                                });
                            } catch(err) {
                                resolve(false);
                            }
                        },
                        (parseInt(num) + secs) * 1000
                    );
                });
                promises.push(pr);
            }
        }

        return promises;
    }
}