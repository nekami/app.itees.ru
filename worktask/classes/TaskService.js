// класс для работы с компанией
class TaskService {
    constructor(repository, options) {
        this.tasks = {};		
        this.checklist = {};
        this.repository = repository;
        this.options = options;	
    }

    /**
     * Загрузка задачи
     * @param df
     */
	 
	load(df, taskId) {
		
		let that = this;
		
			let batch = {}; batch['TASK'] = {'method': 'task.item.getdata', 'params': {"TASKID": taskId}};

			let batchs = {0: batch};			
		
			that.repository.batchHandler(
                batchs,
                function(id, error) {
					console.error("ERROR: " + error);
                },
                function(id, data) {},
                function(items) {
					
					console.log(items);
                           

					
					//resolve(items);
                }
            );
		
		
		

		
		let filter = {}; filter = {"TASKID": taskId};	
		
		return new Promise(function(resolve, reject) 
		{	
//<--------------------- переписать через repository.batchHandler
			that.GeneralbatchHandler(
				'Y',
				'TASK', 
				'task.item.getdata', 
				filter, 
				function(items) { 				
					that.tasks[taskId] = items;
					resolve(); 				
				}
			);
			
		}).then(function(){
            df.resolve();
        });	
    }
	
/******************************/

    /**
     * Получение задачи
     * @param df
     */
    get(taskId) {		
        return (taskId && this.tasks[taskId]) ? this.tasks[taskId] : false
    }

    /**
     * Загрузка чек-листа
     * @param df
     */		
    loadChecklist(df, taskId) {
		
		var that = this;

		let filter = {}; filter = {"TASKID": taskId};
		
		return new Promise(function(resolve, reject) 
		{	
		//<--------------------- переписать через repository.batchHandler
			that.GeneralbatchHandler(
				'N',
				'CHECKLIST', 
				'task.checklistitem.getlist', 
				filter, 
				function(items) {

					var external_data = {};

                    for(var id in items)
                    {												
                        external_data[items[id].ID] = items[id];
                    }
				
					that.checklist[taskId] = external_data;  
					resolve(); 
				}
			);
			
		}).then(function()
		{			
			df.resolve();
		});			
    }		
		
/******************************/	

    /**
     * Получение чек-листа
     * @param df
     */
    getChecklist(taskId) {		
		
        return (taskId && this.checklist[taskId]) ? this.checklist[taskId] : false;
    }
	
	/**
     * Получение чек-листа
     * @param df
     */
	//getList(filter, callback) {
		
	getList(taskId, callback) {		
		
		var that = this;		
		
		let filter = {}; filter = {
			"ORDER": {"ID": "desc"}, 
			"FILTER": {"PARENT_ID": taskId}, 		
			"PARAMS": {"NAV_PARAMS": {nPageSize : 50, iNumPage  : 1}}, 		
			"SELECT": ["TITLE"]
		};
		
		return new Promise(function(resolve, reject) 
		{	
		//<--------------------- переписать через repository.batchHandler
			that.GeneralbatchHandler(
				'N',
				'SUBTASK', 
				'task.item.list', 
				filter, 
				function(items) {

					var external_data = {};

                    for(var id in items) {						
                        external_data[items[id].ID] = items[id];
                    }	
					
					callback(external_data); 
					resolve(); 
				}
			);
			
		});
	}
	
	
	// теги
	
	Tags(taskId) {
		
		let that = this;
		let tags = [];		
		$.each(that.tasks[taskId].TAGS, function(code, value) {			
			if ((value == "app24") || (value.indexOf("app24code_") !== -1) || (value.indexOf("app24vmp_") !== -1) || (value.indexOf("app24v_") !== -1)) {
				tags.push(value);
			}
			if (value == "app24bt") { tags.push("app24bug"); }			
			if (value == "app24vbt") { tags.push("app24vbug"); }			
			if (value == "app24release") { tags.push("app24feature"); }		
		});
		return tags;
	}
	
	
	//подготовка дополнительного номера подзадачи
	
	GetTitleSubtask(subtask, tags, taskId, title) {

		let that = this;		
			
		let ArrSubnumber__Bug = [], ArrSubnumber = [];	
		
		var NumberTask = that.tasks[taskId].TITLE.match(/\d+/)[0]; // номер задачи
			
		$.each(subtask, function(code, value) 
		{	
			var string = value.TITLE.split('.');
			var string2 = string.slice(1).join('.');
				
			var string3 = string2.match(/^[0-9]+/);
				
			if ((string3 != null) && (string3 != undefined)) {
				
				if (($.inArray("app24bug", tags) != -1) && string3['0'] >= 100) {
						
					ArrSubnumber.push(string3[0]);
				}
				else if (($.inArray("app24bug", tags) == -1) && string3['0'] < 100)
				{
					ArrSubnumber.push(string3[0]);
				}
			}
		});			
			
		// "хвост"
		let Subnumber = Math.max.apply(Math, ArrSubnumber) + 1;
			
		if (Subnumber.toString().length == 1) { Subnumber = "0"+Subnumber; }	
			
		let SubTitle = NumberTask+'.'+Subnumber+' '+title.TITLE; // заголовок новой позадачи

		return SubTitle;
	}
	
		
	/**
     * Добавить подзадачу
     * @param df
     */
	addSubtask(taskId, title) {
		
		let that = this;		

		return new Promise(function(resolve, reject) 
		{						
			// все подзадачи
			that.getList(
				taskId, 
				function(items) { resolve(items); }
			);
				
		}).then(function(items)
		{	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////					
							
			var tags = []; tags = that.Tags(taskId); // подготовка тегов		
		
			var SubTitle = that.GetTitleSubtask(items, tags, taskId, title); // название подзадачи	
			 					
			// добавление подзадачи		
					
			let filter = {}; filter = {"TASKDATA": {
				"TITLE": SubTitle, 
				RESPONSIBLE_ID: $('#responsible_id').val(), // идентификатор отвественного
				CREATED_BY: $('#created_by').val(), // идентификатор постановщика
				ACCOMPLICES: [$('#accomplices').val()], // идентификатор соисполнителя 
				GROUP_ID: that.tasks[taskId].GROUP_ID, // группа
				AUDITORS: [$('#auditors').val()], // идентификатор наблюдателя
				PARENT_ID: taskId, 
				TAGS: tags
			}};
			
			return new Promise(function(resolve, reject) 
			{
//<--------------------- переписать через repository.batchHandler
				that.GeneralbatchHandler(
					'Y',
					'ONESUBTASK', 
					'task.item.add', 
					filter, 
					function(items) {					
						if (items != undefined) alert("Задача \""+SubTitle+"\" успешно создана!");
						resolve();
					}
				);
			});
			
			resolve();
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		});
		
	}
	
	//<--------------------- убрать
	GeneralbatchHandler(no_circle, name_batch, method, filter, callback)
	{
		Promise.all([]).then(() => {
            return new Promise((resolve) => {
				
                var batch = {};
				
				let finish = this.options.presumablyPages;				
				if (no_circle == "Y") finish = 1;

                for(var index = 0; index < finish; index ++) {
					
					filter['start'] = index * this.options.itemsPerPage;
					
                    batch[name_batch+'-'+ index] = {'method': method, 'params': filter};
                }					

                var batchs = {0: batch};
				var that = this

                getCallCardDataBase(batchs);

                function getCallCardDataBase(batchs) {
                    that.repository.batchHandler(
                        batchs,
                        function(id, error) {
                            console.error("ERROR: " + error);
                        },
                        function(id, data) {},
                        function(items) {
                            var totals = {};
                            var callCardData = items[0];

                            if(callCardData) {
								
                                $.each(callCardData, function(code, value) {
                                    if(value.error()) {
                                        console.log('Ошибка запроса: ' + value.error());
                                        return;
                                    } else {
                                        var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                        switch(batchCode[1])
                                        {
                                            case name_batch:
											
												callback(value.data());
												
												var total = value.answer.total;
												
												var presumablCount = that.options.presumablyPages * that.options.itemsPerPage

                                                if(total > presumablCount) {
                                                    totals[name_batch] = total;
                                                }

                                                break;
                                        }
                                    }
                                });

                                resolve(totals);
                            } else {
                                getCallCardDataBase(batchs);
                            }
                        }
                    );
                }
            });
        }).then((totals) => {			
			
            return new Promise((resolve, reject) => {
                if(Object.keys(totals).length) {
                    var count = 0, b_count = 0, limit = 40, batchs = {};

                    for(var code in totals) {
                        var pages = Math.ceil(totals[code]/this.options.itemsPerPage);

                        switch(code) {
                            case name_batch:
                                for(var index = this.options.presumablyPages; index < pages; index ++)
                                {
                                    if (count >= limit) {
                                        count = 0;
                                        b_count++;
                                    }

                                    if(!batchs[b_count]) {
                                        batchs[b_count] = {};
                                    }
									
									filter['start'] = index * this.options.itemsPerPage;

                                    batchs[b_count][name_batch+'-'+index] = {'method': method, 'params': filter};

                                    count++;
                                }
                                break;
                        }
                    }

                    if(Object.keys(batchs).length){
						var that = this
                        getCallCardDataTails(batchs);

                        function getCallCardDataTails(batchs)
                        {
                            that.repository.batchHandler(
                                batchs,
                                function(id, error) {
                                    console.error("ERROR: " + error);
                                },
                                function(id, data) {},
                                function(items) {
                                    $.each(items, function(i, callCardData) {
                                        $.each(callCardData, function(code, value) {
                                            if(value.error()) {
                                                alert('Ошибка запроса: ' + value.error());
                                                return
                                            } else {
                                                var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                                switch(batchCode[1]) {
                                                    case name_batch:                                                    
														
														callback(value.data());												

                                                    break;
                                                }
                                            }
                                        });

                                        delete batchs[i];
                                    });

                                    if(Object.keys(batchs).length) {
                                        getCallCardDataTails(batchs);
                                    } else {
                                        resolve();
                                    }
                                }
                            );
                        }
                    }
                } else {
                    resolve();
                }
            })
        })		
	}
	
}