// класс для работы с табелями
class Vacation {
    constructor(repository, options, jsonService) {
        this.repository = repository
        this.options = options
        this.jsonService = jsonService
        this.vacations = {}
        this.vacationsSrc = {}
    }

    /**
     * Загрузка табелей
     * @param df
     * @param filter
     */
    load(df, filter) {
        this.vacations = {}

        Promise.all([]).then(() => {
            return new Promise((resolve, reject) => {
                var batch = {};

                for(var index = 0; index < this.options.presumablyPages; index ++) {
                    batch['DATA_GET-' + index] = {'method': 'entity.item.get', 'params': {'start': index * this.options.itemsPerPage, "ENTITY": 'dssl_tt_data', "FILTER": filter}};
                }

                var batchs = {0: batch};
                var that = this

                getCallCardDataBase(batchs);

                function getCallCardDataBase(batchs) {
                    that.repository.batchHandler(
                        batchs,
                        function(id, error) {
                            console.error("ERROR: " + error);
                        },
                        function(id, data) {},
                        function(items) {
                            var totals = {};
                            var callCardData = items[0];

                            if(callCardData) {
                                $.each(callCardData, function(code, value) {
                                    if(value.error()) {
                                        alert('Ошибка запроса: ' + value.error());
                                        return;
                                    } else {
                                        var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                        switch(batchCode[1])
                                        {
                                            case 'DATA_GET':
                                                var vacations = value.data();
                                                var total = value.answer.total;

                                                for(var vacation in vacations)
                                                {
													const employeeID = vacations[vacation]['PROPERTY_VALUES']['employeeID']
													const periodMark = vacations[vacation]['PROPERTY_VALUES']['periodMark']
													
													if(!that.vacations[periodMark]) {
														that.vacations[periodMark] = {}
													}

                                                    if(!that.vacationsSrc[periodMark]) {
                                                        that.vacationsSrc[periodMark] = {}
                                                    }
													
                                                    that.vacations[periodMark][employeeID] = that.jsonService.parse(vacations[vacation]['DETAIL_TEXT'], false)
                                                    that.vacationsSrc[periodMark][employeeID] = vacations[vacation]
                                                }

                                                var presumablCount = that.options.presumablyPages * that.options.itemsPerPage

                                                if(total > presumablCount) {
                                                    totals['DATA_GET'] = total;
                                                }

                                                break;
                                        }
                                    }
                                });

                                resolve(totals);
                            } else {
                                getCallCardDataBase(batchs);
                            }
                        }
                    );
                }
            });
        }).then(function(totals) {
            return new Promise((resolve, reject) => {
                if(Object.keys(totals).length) {
                    var count = 0, b_count = 0, limit = 40, batchs = {};

                    for(var code in totals) {
                        var pages = Math.ceil(totals[code]/itemsPerPage);

                        switch(code) {
                            case 'DATA_GET':
                                for(var index = this.options.presumablyPages; index < pages; index ++)
                                {
                                    if (count >= limit) {
                                        count = 0;
                                        b_count++;
                                    }

                                    if(!batchs[b_count]) {
                                        batchs[b_count] = {};
                                    }

                                    batchs[b_count]['DATA_GET-' + index] = {'method': 'entity.item.get', 'params': {'start': index * this.options.itemsPerPage, "FILTER": filter}};

                                    count++;
                                }
                                break;
                        }
                    }

                    if(Object.keys(batchs).length){
                        var that = this
                        getCallCardDataTails(batchs);

                        function getCallCardDataTails(batchs)
                        {
                            that.repository.batchHandler(
                                batchs,
                                function(id, error) {
                                    console.error("ERROR: " + error);
                                },
                                function(id, data) {},
                                function(items) {
                                    $.each(items, function(i, callCardData) {
                                        $.each(callCardData, function(code, value) {
                                            if(value.error()) {
                                                alert('Ошибка запроса: ' + value.error());
                                                return
                                            } else {
                                                var batchCode = code.match(/([a-zA-Z_]+)[-]?([0-9]*)/);

                                                switch(batchCode[1]) {
                                                    case 'DATA_GET':
                                                        var vacations = value.data();

                                                        for(var vacation in vacations) {
															const employeeID = vacations[vacation]['PROPERTIES']['employeeID']
															const periodMark = vacations[vacation]['PROPERTIES']['periodMark']
															
															if(!that.vacations[periodMark]) {
																that.vacations[periodMark] = {}
															}
															
															that.vacations[periodMark][employeeID] = that.jsonService.parse(vacations[vacation]['DETAIL_TEXT'], false)
															that.vacationsSrc[periodMark][employeeID] = vacations[vacation]
                                                        }

                                                        break;
                                                }
                                            }
                                        });

                                        delete batchs[i];
                                    });

                                    if(Object.keys(batchs).length) {
                                        getCallCardDataTails(batchs);
                                    } else {
                                        resolve();
                                    }
                                }
                            );
                        }
                    }
                } else {
                    resolve();
                }
            })
        }).then(function(){
            df.resolve();
        });
    }

    /**
     * Сохранение табелей. Пока не используется
     */
    save() {

    }

    /**
     * Получение табелей
     * @param curPeriodMark
     * @returns {*}
     */
    get(curPeriodMark) {
        if(this.vacations) {
            return curPeriodMark ? this.vacations[curPeriodMark] : this.vacations
        } else {
            return false
        }
    }

    /**
     * Получение елементов инфоблока с табелями
     * @param curPeriodMark
     * @returns {*}
     */
	getSrc(curPeriodMark) {
        if(this.vacationsSrc) {
            return curPeriodMark ? this.vacationsSrc[curPeriodMark] : this.vacationsSrc
        } else {
            return false
        }
    }

    /**
     * Выставление табелей. Пока не используется
     * @param vacations
     */
    set(vacations) {
        this.vacations = vacations
    }
}